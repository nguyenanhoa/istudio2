require("dotenv").config();
const withCSS = require('@zeit/next-css')


module.exports = withCSS(
  {
      cssModules: true,
      assetPrefix:`${process.env.URL_PREFIX}/`,
      env: {
          BASE_URL: process.env.BASE_URL,
          BASE_ASSET_URL: process.env.BASE_ASSET_URL,
          API_KEY: process.env.API_KEY,
          URL_PREFIX:process.env.URL_PREFIX            
      },      
  }
)