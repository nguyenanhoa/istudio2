# Honda LMS Admin Panel

## Process Flow
*Headless CMS concept*

![image](./misc/honda-lms-admin-flow.png)

## How To Develop:
* Run `npm install` to install the package dependencies
* Make a copy from `env-template` to `.env` and set the variables accordingly
* Run `npm run dev` to start a local server (Default port: `3000`)
* Get a valid user account from API PIC to login

## How to Deploy:
* Run `npm run build` to build the project
* Make a copy from `env-template` to `.env` and set the variables accordingly
* Run `NODE_ENV=production node server.js` to start a server in production mode