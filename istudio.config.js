
const config = {
    // Push notification config
    push: {
        // Category types (label value pair)
        modules: {
            "My Category 1": "value1",
            "My Category 2": "value2",
            "My Category 3": "value3"
        }
    }
}
export default config