

require("dotenv").config();

const express = require("express");
const next = require("next");
const dev = process.env.NODE_ENV !== "production";
const app = next({ dir: "./src", dev });
const handle = app.getRequestHandler();
const cookieParser = require("cookie-parser");
const axios = require("axios");

/* function authenticate(req, res) {
    const token = req.cookies.token
    var requestHandled = false
    if (token) {
        requestHandled = false
    }
    else {
        requestHandled = true
    }
    return requestHandled;
} */

app
    .prepare()
    .then(() => {
        const server = express();
        server.use(express.urlencoded({
            extended: true
        }))
        server.use(cookieParser());
        // Routing table for url rewriting
        var routes = [
            { url: "/", page:"/notification/draft" },
            { url: "/login", page: "/auth/login" },
            { url: "/forgot_password", page: "/auth/forgot_password"},
            { url: "/forgot_password_success", page: "/auth/forgot_password_success"},
            { url: "/reset_password", page: "/auth/reset_password"},

            { url: "/draft", page: "/notification/draft"},            
            { url: "/scheduled", page: "/notification/scheduled"},
            { url: "/sent", page: "/notification/sent"},
            { url: "/notification_log", page: "/notification/notification_log"},
            { url: "/notification_analytics", page: "/notification/notification_analytics"},
            

            { url: "/setting/general", page: "/setting/general"},
            { url: "/setting/database", page: "/setting/database"},
            { url: "/setting/token", page: "/setting/token"},
            { url: "/setting/notification", page: "/setting/notification"},
            { url: "/setting/smtp", page: "/setting/smtp"},
            { url: "/setting/fileserver", page: "/setting/fileserver"},
            { url: "/setting/smsserver", page: "/setting/smsserver"},
            { url: "/setting/paymentgateway", page: "/setting/paymentgateway"},
            
            { url: "/setting/users", page: "/setting/users"},
            { url: "/setting/users/create", page: "/setting/create-user-page"},
            { url: "/setting/users/edit/:user_id", page: "/setting/edit-user-page" },

        ];

        var whitelist = [
            ""
        ]
        routes.forEach(route => {
            server.get(route.url, async (req, res) => {
                // bypass if the url is in the whitelist
                if (whitelist.indexOf(route.url) > -1) {
                    const combined = { ...req.query, ...req.params };
                    app.render(req, res, route.page, combined);
                    return
                }

                /* var requestHandled = authenticate(req, res);
                if (!requestHandled) { */
                    const combined = { ...req.query, ...req.params };
                    app.render(req, res, route.page, combined);
               /*  }
                else {
                    const combined = { ...req.query, ...req.params };
                    app.render(req, res, "/prebooking", combined);
                } */


            });
        });
        

        server.get("*", async (req, res) => {
            // bypass these kinds of url
            if (
                req.url.startsWith("/_next") ||
                req.url.endsWith(".css") ||
                req.url.endsWith(".js") ||
                req.url.endsWith(".ico") ||
                req.url.endsWith(".jpg") ||
                req.url.endsWith(".png") ||
                req.url.endsWith(".html") ||
                req.url.endsWith(".json")
            ) {
                return handle(req, res);
            }

            // bypass if the url is in the whitelist
            if (whitelist.indexOf(req.path) > -1) {
                return handle(req, res)
            }

            return handle(req, res);
        });
       
        // Listen to port
        let port = process.env.PORT || 3000;
        server.listen(port, err => {
            if (err) throw err;
            console.log(`> Ready on http://localhost:${port}`);
        });
    })
    .catch(ex => {
        console.error(ex.stack);
        process.exit(1);
    });
