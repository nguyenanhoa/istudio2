function confirmDelete() {
	return confirm("Do you want to delete the selected item?");
}

function confirmRemove() {
	return confirm("Do you want to remove the selected item?");
}

function confirmAction() {
	return confirm("Do you want to perform the action now?");
}

function confirmSignOut() {	
	return confirm("Do you want to sign out now?");
}

function showDetailPanel(){
    document.getElementById("draft-action-details-action").style.display ="block";
}

function removeAllPanel(){
    document.getElementById("draft-action-details-action").style.display ="none";
}

$(window).on('load', function() {
    if($(window).width() > 991) {
        $('#nav-right').addClass('justify-content-end');
        $('#nav-right').removeClass('justify-content-between');
        $('#all-forms-action').addClass('justify-content-end');
    }else{
        $('#nav-right').addClass('justify-content-between');
        $('#nav-right').removeClass('justify-content-end');
        $('#all-forms-action').removeClass('justify-content-end');
    }

    if($(window).width() > 1200) {
        $('.all-forms-action').addClass('justify-content-end');
    }else{
        $('.all-forms-action').removeClass('justify-content-end');
    }

})

$(window).on('resize', function() {
    if($(window).width() > 991) {
        $('#nav-right').addClass('justify-content-end');
        $('#nav-right').removeClass('justify-content-between');
    }else{
        $('#nav-right').addClass('justify-content-between');
        $('#nav-right').removeClass('justify-content-end');
    }

    if($(window).width() > 1200) {
        $('.all-forms-action').addClass('justify-content-end');
    }else{
        $('.all-forms-action').removeClass('justify-content-end');
    }
})