import MyService from "../../services/my-service";
import { Snack } from '../../components/snack';
import Setting_Layout from '../../components/setting_layout';
import Madatory_Slider from '../../components/madatory_slider';
import { withAuth } from '../../components/auth-component';
import { withRouter } from "next/router";
import update from 'immutability-helper';
import IStudioService from '../../services/istudio-service';
import { ACCESS_SETTING_GENERAL_SMTP, ACCESS_SETTING } from '../../services/access-service';

class Smtp extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            zeroState: true,
            switchTheme: this.props.router.query.mode == "dark" ? "dark_theme" : "white_theme",
            data: {},
            loading: true
        }
    }

    componentDidMount() {
        IStudioService.getSettings('smtp').then(res => {
            console.log("SMTP", res.data.data)
            this.setState({
                data: res.data.data,
                loading: false
            })
        })
    }

    updateFormData(key, value) {
        let data = update(this.state.data, {
            [key]: {
                $set: value
            }
        })
        this.setState({ data })
    }

    updateSettings() {
        IStudioService.saveSettings(this.state.data).then(res => {
            this.setState({
                success: true
            })
        })
    }
    render() {
        return (
            <Setting_Layout title="General" mode={this.state.switchTheme}>
                <Madatory_Slider madatory="smtp" mode={this.state.switchTheme} />
                {/* drafts panel */}
                {/* right panel */}
                {
                    this.state.loading ?
                        <div class="spinner-border text-primary center_spinner" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        :
                        <div className="col-8 p-0 fade-in" style={{ height: '100%' }}>
                            <div className="col border-bottom p-0 d-flex align-items-center  noti-draft-right-panel-top-height theme-title-color">
                                <div className="col row p-0 m-0">
                                    <div className="col-xl-4 " style={{ marginTop: '5px', marginBottom: '-5px' }}>
                                        <div className="font-size-21 ml-1 font-medium theme-font-color">Mail Server Configuration</div>
                                    </div>
                                    <div className="col-xl-8 row m-0 d-flex justify-content-end">
                                        <button className="green-font font-size-14 mr-3" style={{ background: 'transparent', border: '0px' }}>Success @ 30-Dec-20, 05:17 PM</button>
                                        <button className="draft-detail-btn theme-font-color mr-3" style={{ width: '180px' }}>Test Connection</button>
                                        <button className="draft-proceed-btn btn-primary">Update</button>
                                    </div>
                                </div>
                            </div>
                            <div className="col p-0 m-0 row  noti-draft-right-panel-btm-height">
                                {/* draft details panel */}
                                <div className="col d-flex flex-column justify-content-start py-4 panel overflow-auto" style={{ height: '99%' }}>
                                    <div className="font-size-18 pb-2 font-medium theme-font-color">Connect to an Existing Mail Server</div>
                                    <div className="row form-group pt-1 d-flex align-items-center">
                                        <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">Type</label>
                                        <div className="col-sm-7">
                                            <select className="form-control draft-input-height" style={{ margin: '0px' }}>
                                                <option>TLS</option>
                                                <option>SSL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="row form-group pt-1 d-flex align-items-center">
                                        <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">Server Host</label>
                                        <div className="col-sm-4">                                            
                                            <input
                                                onChange={(e) => {
                                                    this.updateFormData('smtp.host', e.target.value)
                                                }}
                                                type="text"
                                                className="form-control draft-input-height"
                                                placeholder="mail.inglab.com.my"
                                                value={this.state.data['smtp.host']} />
                                        </div>
                                        <div className="col-sm-1">
                                            <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">Port</label>
                                        </div>
                                        <div className="col-sm-2">
                                            <input
                                                onChange={(e) => {
                                                    this.updateFormData('smtp.port', e.target.value)
                                                }}
                                                type="text"
                                                className="form-control draft-input-height"
                                                placeholder="25"
                                                value={this.state.data['smtp.port']} />
                                        </div>
                                    </div>
                                    <div className="row form-group pt-1 d-flex align-items-center">
                                        <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">Sender Name</label>
                                        <div className="col-sm-7">
                                            <input type="text" className="form-control draft-input-height" placeholder="Adminstrator" />
                                        </div>
                                    </div>
                                    <div className="row form-group pt-1 d-flex align-items-center">
                                        <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">Sender Email</label>
                                        <div className="col-sm-7">
                                            <input type="text" className="form-control draft-input-height" placeholder="no-reply@inglab.my" />
                                        </div>
                                    </div>
                                    <div className="row form-group pt-1 d-flex align-items-center">
                                        <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">Password</label>
                                        <div className="col-sm-7">
                                            <input type="password" className="form-control draft-input-height" placeholder="******" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                }
            </Setting_Layout>
        )
    }
}

export default withAuth(withRouter(Smtp))