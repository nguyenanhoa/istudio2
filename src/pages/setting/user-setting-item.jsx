/**
 * @author Guster
 * @email guster@inglab.com.my
 * @create date 2020-04-16 15:32:25
 * @modify date 2020-04-16 15:32:25
 * @desc [description]
 */

import React, { Component } from 'react'
import Utils from '../../services/utils'
import InfoTooltip from '../../components/info-tooltip'

export default class UserSettingItem extends Component {
        constructor(props) {
        super(props)
    }

    render() {
        let uuid = Utils.uuid()
        return (
            <div className='p-3 mb-1' style={{ border: '1px solid #ddd', borderRadius: '10px' }}>
                <div className='d-flex align-items-center'>
                    <div className="font-size-16 font-medium black-font">{this.props.title}</div>
                    {this.props.id ?
                        <InfoTooltip id={`tooltip-${this.props.id}`} tooltip={this.props.tooltip} />
                        : null
                    }
                    <div className='flex-grow-1' />
                    <label
                        htmlFor={uuid}
                        className=' mt-1'>
                        <input
                            id={uuid}
                            type='checkbox'
                            data-toggle="toggle" data-size="sm"
                            checked={this.props.checked || false}
                            onChange={e => {
                                if (this.props.onChange) {
                                    this.props.onChange(e.target.checked)
                                }
                            }} />
                        <span className="slider round"></span>
                    </label>
                </div>
            </div>
        )
    }
}
