import MyService from "../../services/my-service";
import { Snack } from '../../components/snack';
import Setting_Layout from '../../components/setting_layout';
import Madatory_Slider from '../../components/madatory_slider';
import { withAuth } from '../../components/auth-component';
import { withRouter } from "next/router";
import update from 'immutability-helper';
import IStudioService from '../../services/istudio-service';
import { ACCESS_SETTING_GENERAL_PUSH } from '../../services/access-service';
class Notification extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            zeroState: true,
            switchTheme: this.props.router.query.mode == "dark" ? "dark_theme" : "white_theme",
            data: {},
            loading: true
        }
    }

    componentDidMount() {
        IStudioService.getSettings('push').then(res => {
            this.setState({
                data: res.data.data,
                loading: false
            })
        })
    }

    updateFormData(key, value) {
        let data = update(this.state.data, {
            [key]: {
                $set: value
            }
        })
        this.setState({ data })
    }

    updateSettings() {
        console.log(this.state.data)
        IStudioService.saveSettings(this.state.data).then(res => {
            this.setState({
                success: true
            })
        })
    }

    render() {
        return (
            <Setting_Layout title="General" mode={this.state.switchTheme}>
                <Madatory_Slider madatory="notification" mode={this.state.switchTheme} />
                {/* drafts panel */}
                {/* right panel */}
                {
                    this.state.loading ?
                        <div class="spinner-border text-primary center_spinner" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        :
                        <div className="col-8 p-0 fade-in" style={{ height: '100%' }}>
                            <div className="col border-bottom p-0 d-flex align-items-center  noti-draft-right-panel-top-height theme-title-color">
                                <div className="col row p-0 m-0">
                                    <div className="col-xl-4 " style={{ marginTop: '5px', marginBottom: '-5px' }}>
                                        <div className="font-size-21 ml-1 font-medium theme-font-color">Push Notification Setting</div>
                                    </div>
                                    <div className="col-xl-8 row m-0 d-flex justify-content-end">
                                        <button className="draft-proceed-btn btn-primary">Update</button>
                                    </div>
                                </div>
                            </div>
                            <div className="col p-0 m-0 row  noti-draft-right-panel-btm-height">
                                {/* draft details panel */}
                                <div className="col d-flex flex-column justify-content-start py-4 panel overflow-auto" style={{ height: '99%' }}>
                                    <div className="font-size-18 pb-2 font-medium theme-font-color">Connect to a Push Notification Gateway</div>
                                    <div className="row form-group pt-1 d-flex align-items-center">
                                        <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">Service Provider</label>
                                        <div className="col-sm-7">
                                            {/* <select className="form-control draft-input-height" style={{ margin: '0px' }}>
                                        <option>Firebase</option>
                                        <option selected>One Signal</option>
                                    </select> */}
                                            <select
                                                className="form-control draft-input-height"
                                                value={this.state.data['push.provider']}
                                                onChange={(e) => { this.updateFormData('push.provider', e.target.value) }}
                                                style={{ margin: '0px' }}
                                            >
                                                <option value="one-signal">One Signal</option>
                                                <option value="airship">Firebase</option>
                                                <option value="fcm">Urban Airship</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="row form-group pt-1 d-flex align-items-center">
                                        <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">API Key</label>
                                        <div className="col-sm-7">
                                            <input
                                                onChange={(e) => { this.updateFormData('push.api_key', e.target.value) }}
                                                type="text"
                                                className="form-control draft-input-height"
                                                placeholder="3f2de2ed2q3x34qasv4q3w3dww"
                                                value={this.state.data['push.api_key']} />
                                        </div>
                                    </div>
                                    <div className="row form-group pt-1 d-flex align-items-center">
                                        <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">API ID</label>
                                        <div className="col-sm-7">
                                            {/*  <input type="text" className="form-control draft-input-height" placeholder="3f2de2ed2q3x34qasv4q3w3dww" /> */}
                                            {this.state.data['push.provider'] == 'one-signal' ?
                                                <input
                                                    onChange={(e) => { this.updateFormData('push.os.app_id', e.target.value) }}
                                                    type="text"
                                                    placeholder="3f2de2ed2q3x34qasv4q3w3dww"
                                                    className="form-control draft-input-height"
                                                    value={this.state.data['push.os.app_id']} />
                                                : null}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                }
            </Setting_Layout>
        )
    }
}

export default withAuth(withRouter(Notification))