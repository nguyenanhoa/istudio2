/**
 * @author Guster
 * @email guster@inglab.com.my
 * @create date 2020-04-16 15:58:09
 * @modify date 2020-04-16 15:58:09
 * @desc [description]
 */

import React, { Component } from 'react'
import { Row, Col, Card } from 'reactstrap'
import Utils from '../../services/utils'
import InfoTooltip from '../../components/info-tooltip'

export default class UserSettingSubgroup extends Component {
    render() {
        let lv1id = Utils.uuid()
        return (
            <div className={`mt-3 ${this.props.className}`}>
                <div className='d-flex'>
                    <div className='mr-2 ml-2 flex-grow-1 font-size-18 font-medium black-font'><b>{this.props.title}</b></div>
                    {this.props.id ?
                        <InfoTooltip id={`tooltip-${this.props.id}`} tooltip={this.props.tooltip} />
                        : null
                    }

                    {(this.props.checkable == null || this.props.checkable) ?
                        <label htmlFor={lv1id} className='mr-3' style={{ marginTop: '3px' }}>
                            <input
                                id={lv1id}
                                type='checkbox'
                                data-toggle="toggle" data-size="sm"
                                checked={this.props.checked || false}
                                onChange={e => {
                                    if (this.props.onChange) {
                                        this.props.onChange(e.target.checked)
                                    }
                                }} />
                            <span className="slider round"></span>
                        </label>
                        : <div className='m-3' />}
                </div>

                {this.props.children}
            </div>
        )
    }
}
