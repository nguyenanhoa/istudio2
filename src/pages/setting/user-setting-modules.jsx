/**
 * @author Guster
 * @email guster@inglab.com.my
 * @create date 2020-04-20 15:01:49
 * @modify date 2020-04-20 15:01:49
 * @desc [description]
 */

import React, { Component } from 'react'
import update from 'immutability-helper';
import { Row, Col, Card, Form, Button } from 'reactstrap'
import UserSettingItem from './user-setting-item';
import UserSettingGroup from './user-setting-group';
import UserSettingSubgroup from './user-setting-subgroup';
import * as mtx from '../../services/access-service';
import * as _ from 'lodash';

export default class UserSettingModules extends Component {

    constructor(props) {
        super(props)
        this.state = {
            matrix: props.matrix || {},
        }
    }

    componentWillReceiveProps(props) {
        this.setState({
            matrix: props.matrix
        })
    }

    updateMatrix(key, value) {
        let matrix = update(this.state.matrix, {
            [key]: {
                $set: value
            }
        })
        this.setState({ matrix })

        this.props.onMatrixChanged && this.props.onMatrixChanged(matrix)
    }

    updateMatrixMultiple(keys, value) {
        let map = {}
        keys.forEach(e => map[e] = {
            $set: value
        })
        let matrix = update(this.state.matrix, map)
        this.setState({ matrix })

        this.props.onMatrixChanged && this.props.onMatrixChanged(matrix)
    }

    accessible(key) {
        return this.state.matrix[key] == true
    }

    accessibleMultiple(keys) {
        let result = true
        keys.forEach(e => {
            result = result && this.accessible(e)
        })
        return result
    }

    render() {
        return (
            // <Col sm={7} className="main-content">
            <Col className={`main-content ${this.props.className}`} style={this.props.style}>
                {/* <div className="card-body"> */}
                <div className="">
                    <UserSettingGroup
                        title='Push Notification'
                        id='push'
                        tooltip='Push Notification'
                        // className='mt-5'
                        checkable={true}
                        checked={this.accessibleMultiple([
                            mtx.ACCESS_PUSH_DRAFT,
                            mtx.ACCESS_PUSH_DRAFT_CREATE,
                            mtx.ACCESS_PUSH_DRAFT_SEND,
                            mtx.ACCESS_PUSH_SCHEDULED,
                            mtx.ACCESS_PUSH_SENT
                        ])}
                        onChange={e => {
                            this.updateMatrixMultiple([
                                mtx.ACCESS_PUSH_DRAFT,
                                mtx.ACCESS_PUSH_DRAFT_CREATE,
                                mtx.ACCESS_PUSH_DRAFT_SEND,
                                mtx.ACCESS_PUSH_SCHEDULED,
                                mtx.ACCESS_PUSH_SENT,
                            ], e)
                        }}>
                        <UserSettingSubgroup
                            title='Draft'
                            checkable={false}>
                            <Row>
                                <Col>
                                    <UserSettingItem
                                        title='View'
                                        id='push-view'
                                        tooltip="Allow to view only draft messages"
                                        checked={this.accessibleMultiple([
                                            mtx.ACCESS_PUSH_DRAFT,
                                        ])}
                                        onChange={e => {
                                            this.updateMatrixMultiple([
                                                mtx.ACCESS_PUSH_DRAFT,
                                            ], e)
                                        }} />
                                </Col>
                                <Col>
                                    <UserSettingItem
                                        title='Create'
                                        id='push-create'
                                        tooltip="Allow to create, edit & delete draft messages"
                                        checked={this.accessible(mtx.ACCESS_PUSH_DRAFT_CREATE)}
                                        onChange={e => {
                                            this.updateMatrixMultiple([
                                                mtx.ACCESS_PUSH_DRAFT_CREATE,
                                            ], e)
                                        }} />
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={6}>
                                    <UserSettingItem
                                        title='Send'
                                        id='push-send'
                                        tooltip='Allow to send push messages'
                                        checked={true}
                                        checked={this.accessible(mtx.ACCESS_PUSH_DRAFT_SEND)}
                                        onChange={e => {
                                            this.updateMatrixMultiple([
                                                mtx.ACCESS_PUSH_DRAFT_SEND
                                            ], e)
                                        }} />
                                </Col>
                            </Row>
                        </UserSettingSubgroup>
                        <hr />
                        <UserSettingSubgroup
                            title='Scheduled'
                            checked={this.accessible(mtx.ACCESS_PUSH_SCHEDULED)}
                            onChange={e => {
                                this.updateMatrix(mtx.ACCESS_PUSH_SCHEDULED, e)
                            }} />
                        <hr />
                        <UserSettingSubgroup
                            title='Sent'
                            checked={this.accessible(mtx.ACCESS_PUSH_SENT)}
                            onChange={e => {
                                this.updateMatrix(mtx.ACCESS_PUSH_SENT, e)
                            }} />
                    </UserSettingGroup>

                    {/* activity logs */}
                    <UserSettingGroup
                        title='Activity Logs'
                        id='actlogs'
                        tooltip='Activity Logs'
                        className='mt-5'
                        checkable={true}
                        checked={this.accessibleMultiple([
                            mtx.ACCESS_ACTLOG_SYSTEM,
                            mtx.ACCESS_ACTLOG_USER,
                            mtx.ACCESS_ACTLOG_ADMIN,
                        ])}
                        onChange={e => {
                            this.updateMatrixMultiple([
                                mtx.ACCESS_ACTLOG_SYSTEM,
                                mtx.ACCESS_ACTLOG_USER,
                                mtx.ACCESS_ACTLOG_ADMIN,
                            ], e)
                        }}>
                        <UserSettingSubgroup
                            title='System'
                            checked={this.accessible(mtx.ACCESS_ACTLOG_SYSTEM)}
                            onChange={e => {
                                this.updateMatrix(mtx.ACCESS_ACTLOG_SYSTEM, e)
                            }} />
                        <hr />
                        <UserSettingSubgroup
                            title='User Logs'
                            checked={this.accessible(mtx.ACCESS_ACTLOG_USER)}
                            onChange={e => {
                                this.updateMatrix(mtx.ACCESS_ACTLOG_USER, e)
                            }} />
                        <hr />
                        <UserSettingSubgroup
                            title='Admin Logs'
                            checked={this.accessible(mtx.ACCESS_ACTLOG_ADMIN)}
                            onChange={e => {
                                this.updateMatrix(mtx.ACCESS_ACTLOG_ADMIN, e)
                            }} />
                    </UserSettingGroup>

                    {/* business */}
                    <UserSettingGroup
                        title='Business'
                        id='business'
                        tooltip='Business'
                        className='mt-5'
                        checkable={true}
                        checked={this.accessible(mtx.ACCESS_BUSINESS)}
                        onChange={e => {
                            this.updateMatrix(mtx.ACCESS_BUSINESS, e)
                        }}>
                    </UserSettingGroup>

                    {/* Setting */}
                    <UserSettingGroup
                        title='Setting'
                        id='setting'
                        tooltip='Setting'
                        className='mt-5'
                        checkable={true}
                        checked={this.accessibleMultiple([
                            mtx.ACCESS_SETTING,
                            mtx.ACCESS_SETTING_BUSINESS,
                            mtx.ACCESS_SETTING_GENERAL,
                            mtx.ACCESS_SETTING_GENERAL_PUSH,
                            mtx.ACCESS_SETTING_GENERAL_SMTP,
                            mtx.ACCESS_SETTING_GENERAL_TNC,
                            mtx.ACCESS_SETTING_SECURITY,
                            mtx.ACCESS_SETTING_SECURITY_COMM,
                            mtx.ACCESS_SETTING_SECURITY_DEVICE,
                            mtx.ACCESS_SETTING_SECURITY_LOGIN,
                            mtx.ACCESS_SETTING_SECURITY_PASSWORD,
                            mtx.ACCESS_SETTING_USERS,
                        ])}
                        onChange={e => {
                            this.updateMatrixMultiple([
                                mtx.ACCESS_SETTING,
                                mtx.ACCESS_SETTING_BUSINESS,
                                mtx.ACCESS_SETTING_GENERAL,
                                mtx.ACCESS_SETTING_GENERAL_PUSH,
                                mtx.ACCESS_SETTING_GENERAL_SMTP,
                                mtx.ACCESS_SETTING_GENERAL_TNC,
                                mtx.ACCESS_SETTING_SECURITY,
                                mtx.ACCESS_SETTING_SECURITY_COMM,
                                mtx.ACCESS_SETTING_SECURITY_DEVICE,
                                mtx.ACCESS_SETTING_SECURITY_LOGIN,
                                mtx.ACCESS_SETTING_SECURITY_PASSWORD,
                                mtx.ACCESS_SETTING_USERS,
                            ], e)
                        }}>
                    </UserSettingGroup>

                    {/* reports */}
                    {/* <UserSettingGroup
                        title='Reports'
                        id='report'
                        tooltip='Reports'
                        className='mt-5'
                        checkable={true}
                        checked={this.accessibleMultiple([
                            mtx.ACCESS_REPORT_USER,
                            mtx.ACCESS_REPORT_ANDROID,
                            mtx.ACCESS_REPORT_IOS,
                            mtx.ACCESS_REPORT_NOTIFICATION,
                        ])}
                        onChange={e => {
                            this.updateMatrixMultiple([
                                mtx.ACCESS_REPORT_USER,
                                mtx.ACCESS_REPORT_ANDROID,
                                mtx.ACCESS_REPORT_IOS,
                                mtx.ACCESS_REPORT_NOTIFICATION,
                            ], e)
                        }}> */}
                    {/* <UserSettingSubgroup
                            title='User'
                            checked={this.accessible(mtx.ACCESS_REPORT_USER)}
                            onChange={e => {
                                this.updateMatrix(mtx.ACCESS_REPORT_USER, e)
                            }} />
                        <hr />
                        <UserSettingSubgroup
                            title='Android'
                            checked={this.accessible(mtx.ACCESS_REPORT_ANDROID)}
                            onChange={e => {
                                this.updateMatrix(mtx.ACCESS_REPORT_ANDROID, e)
                            }} />
                        <hr />
                        <UserSettingSubgroup
                            title='iOS'
                            checked={this.accessible(mtx.ACCESS_REPORT_IOS)}
                            onChange={e => {
                                this.updateMatrix(mtx.ACCESS_REPORT_IOS, e)
                            }} />
                        <hr />
                        <UserSettingSubgroup
                            title='Push Notification'
                            checked={this.accessible(mtx.ACCESS_REPORT_NOTIFICATION)}
                            checkable={true}
                            onChange={e => {
                                this.updateMatrix(mtx.ACCESS_REPORT_NOTIFICATION, e)
                            }} />
                    </UserSettingGroup> */}

                </div>


                <style jsx>{`
                    .title {
                        font-family: SF-UI-Display-Heavy;
                    }
                    .subtitle {
                        font-family: SF-UI-Display-Heavy;
                        font-size: 21px;
                    }
                `}</style>
            </Col>
        )
    }
}
