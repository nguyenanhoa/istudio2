import MyService from "../../services/my-service";
import { Snack } from '../../components/snack';
import Setting_Layout from '../../components/setting_layout';
import Madatory_Slider from '../../components/madatory_slider';
import { withAuth } from '../../components/auth-component';
import FormField from '../../components/form-field';
import { withRouter } from "next/router";
import IStudioService from "../../services/istudio-service";
import update from 'immutability-helper';
import * as _ from 'lodash';
import * as mtx from '../../services/access-service';
import { ACCESS_PUSH_DRAFT, AccessService, ACCESS_PUSH_DRAFT_SEND } from '../../services/access-service';
import UserSettingModules from './user-setting-modules';
import Select from 'react-select';
import AppButton from '../../components/app-button';
import { Form } from 'reactstrap'

class CreateUserPage extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            error: null,
            success: null,
            zeroState: true,
            switchTheme: this.props.router.query.mode == "dark" ? "dark_theme" : "white_theme",
            matrix: {},
            loading: false,
            success: null,
            userId: props.router.query.user_id,
            creatingUser: true,
            superior1: null,
            superior2: null,
            createUser: {
                username: null,
                email: null,
                displayName: null,
                includes: [],
                excludes: [],
                superiors: []
            }
        }
    }

    componentDidMount() {
        IStudioService.getSuperiors().then(res => {
            this.setState({
                superiors: res.data.map(o => {
                    return {
                        label: o.displayName,
                        value: o.userId
                    }
                })
            })
        })
    }

    validateSuperiors() {
        if (this.state.superior1 != null
            && this.state.superior2 != null
            && this.state.superior1 == this.state.superior2) {
            this.setState({
                formError: '2 Superiors cannot be the same person'
            })
            return false
        }
        return true
    }

    submit(e) {
        e.preventDefault()

        let includes = Object.keys(this.state.matrix)
            .filter(key => this.state.matrix[key] == true)
        let excludes = Object.keys(this.state.matrix)
            .filter(key => this.state.matrix[key] == false)

        includes.push(mtx.ACCESS_HOME)

        this.state.createUser.includes = includes
        this.state.createUser.excludes = excludes

        if (!this.validateSuperiors()) return

        this.state.createUser.superiors = []
        if (this.state.superior1 != null) {
            this.state.createUser.superiors.push(this.state.superior1)
        }

        if (this.state.superior2 != null) {
            this.state.createUser.superiors.push(this.state.superior2)
        }

        this.setState({ loading: true })

        IStudioService.createUser(this.state.createUser).then(res => {
            this.setState({
                loading: false,
                success: true
            })

            window.location.assign(`${process.env.URL_PREFIX}/settings/team`)

        }).catch(e => {
            this.setState({
                success: false,
                loading: false,
                error: e.response ? e.response.data.message : 'Service Unavailable, please try again later.'
            })
        })
    }
    directUrl(url) {

        let switchTheme = window.localStorage.getItem("switchTheme")
        if (switchTheme != null) {
            this.setState({
                switchTheme: switchTheme
            })
            if (switchTheme == "white_theme") {
                window.location.href = `${process.env.URL_PREFIX}${url}?mode=white`
            }
            else {
                window.location.href = `${process.env.URL_PREFIX}${url}?mode=dark`
            }
        }
    }
    render() {
        return (
            <Setting_Layout title="Users" mode={this.state.switchTheme}>
                <div class="col-3 pl-2 pr-0 fade-in draft_container">
                    <div class="mt-1 d-flex justify-content-between pt-0" style={{ height: "10%" }}>
                        <div onClick={() => this.directUrl("/setting/users")}>
                            <p class="py-0 m-0 font-size-21 font-medium black-font theme-font-color">
                                <span ><a className="font-size-21 font-medium theme-font-color"><img class="mr-3" src={`${process.env.URL_PREFIX}/static/img/Arrow - Back.png`} alt="" />Create New User</a></span>
                            </p>
                        </div>
                    </div>
                    <Form
                        onSubmit={(e) => {
                            this.submit(e)
                        }}>
                        <div className="font-size-18 pt-1 font-medium black-font theme-font-color">Step 1 - Account Information</div>
                        <div className="font-size-16 pt-1 font-medium black-font theme-font-color">User's Account details</div>
                        <hr />
                        <div className="row form-group pt-1 d-flex align-items-center">
                            <FormField
                                title='Display Name'
                                required={true}
                                placeholder='John Doe'
                                value={this.state.createUser.displayName || ''}
                                onChange={e => {
                                    let createUser = update(this.state.createUser, {
                                        displayName: {
                                            $set: e.target.value
                                        }
                                    })
                                    this.setState({ createUser })
                                }}
                            />
                        </div>
                        <div className="row form-group pt-1 d-flex align-items-center">
                            <FormField
                                title='Username'
                                required={true}
                                placeholder='johndoe'
                                value={this.state.createUser.username || ''}
                                onChange={e => {
                                    let createUser = update(this.state.createUser, {
                                        username: {
                                            $set: e.target.value
                                        }
                                    })
                                    this.setState({ createUser })
                                }}
                            />
                        </div>
                        <div className="row form-group pt-1 d-flex align-items-center">
                            <FormField
                                title='Email'
                                type='email'
                                required={true}
                                value={this.state.createUser.email || ''}
                                placeholder='abc@gmail.com'
                                onChange={e => {
                                    let createUser = update(this.state.createUser, {
                                        email: {
                                            $set: e.target.value
                                        }
                                    })
                                    this.setState({ createUser })
                                }}
                            />
                        </div>
                        <div className="row form-group pt-1 d-flex align-items-center">
                            <label htmlFor="name" className="col-sm-12 col-form-label grey-font font-size-14">Superior 1</label>
                            <div className="col-sm-12">
                                <Select
                                    options={this.state.superiors}
                                    className="draft-input-height"
                                    onChange={item => {
                                        this.setState({
                                            superior1: item.value
                                        })
                                    }}
                                />
                            </div>
                        </div>

                        <div className="row form-group pt-1 d-flex align-items-center">
                            <label htmlFor="name" className="col-sm-12 col-form-label grey-font font-size-14">Superior 2</label>
                            <div className="col-sm-12">
                                <Select
                                    className="draft-input-height"
                                    options={this.state.superiors}
                                    onChange={item => {
                                        this.setState({
                                            superior2: item.value
                                        })
                                    }}
                                />
                            </div>
                        </div>
                        {/* <div className="row m-0 d-flex justify-content-center">
                        <button onClick={(e) => this.submit(e)} className="draft-proceed-btn btn-primary" style={{width:"100%"}}>Continue</button>
                    </div> */}
                        <AppButton
                            loading={this.state.loading}
                            disabled={this.state.loading}>
                            CONTINUE
                    </AppButton>
                    </Form>

                </div>
                {/* drafts panel */}
                {/* right panel */}
                <div className="col-7 p-0 fade-in" style={{ height: '100%' }}>
                    <div className="ml-5 mr-5">
                        <div className="font-size-18 font-medium black-font theme-font-color" style={{ marginTop: "68px" }}>Step 2 - Module Access</div>
                        <div className="font-size-16 font-medium black-font theme-font-color">Select which modules and actions this user can access.</div>
                        <hr style={{ marginTop: "20px" }} />
                        <div style={{ overflow: 'auto', maxHeight: '600px' }}>
                            <UserSettingModules
                                matrix={this.state.matrix}
                                onMatrixChanged={matrix => {
                                    this.setState({ matrix, dataChanged: true })
                                }} />
                        </div>
                    </div>
                </div>
                {this.state.error ?
                    <Snack
                        color='#d74338'
                        text={this.state.error}
                        vertical='bottom'
                        horizontal='left'
                        open={this.state.error != null}
                        onClose={() => {
                            this.setState({ error: null })
                        }}
                        onActionButtonClick={() => {
                            this.setState({ error: null })
                        }}
                    />
                    : null
                }
                {this.state.success ?
                    <Snack
                        color='green'
                        text={this.state.success}
                        vertical='bottom'
                        horizontal='left'
                        open={this.state.success != null}
                        onClose={() => {
                            this.setState({ success: null })
                        }}
                        onActionButtonClick={() => {
                            this.setState({ success: null })
                        }}
                    />
                    : null
                }
            </Setting_Layout>
        )
    }
}

export default withAuth(withRouter(CreateUserPage))