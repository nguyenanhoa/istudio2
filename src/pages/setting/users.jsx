import MyService from "../../services/my-service";
import { Snack } from '../../components/snack';
import Setting_Layout from '../../components/setting_layout';
import Madatory_Slider from '../../components/madatory_slider';
import { withAuth } from '../../components/auth-component';
import { withRouter } from "next/router";
import IStudioService from "../../services/istudio-service";
import update from 'immutability-helper';
import * as _ from 'lodash';
import * as mtx from '../../services/access-service';
import { ACCESS_PUSH_DRAFT, AccessService, ACCESS_PUSH_DRAFT_SEND } from '../../services/access-service';
import UserSettingModules from './user-setting-modules';

class Users extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            error: null,
            success: null,
            zeroState: true,
            switchTheme: this.props.router.query.mode == "dark" ? "dark_theme" : "white_theme",
            users: [],
            matrix: {},
            selectedUserIndex: -1,
            selectedUser: null,
            dataChanged: false,
            loading: false,
            creatingUser: true,
            createUser: {
                username: null,
                email: null,
                displayName: null
            }
            
        }
    }

    componentDidMount() {
        IStudioService.getAdminUsers().then(res => {
            console.log("Users Data", res.data)
            //auto select first item
            let users=res.data
            if(users.length>0)
            {
                this.onUserSelected(users[0],0)
            }            
            //
            this.setState({
                users: res.data
            })
        })
    }

    onUserSelected(user, index) {
        this.setState({
            selectedUserIndex: index,
            selectedUser: user,
            creatingUser: false
        })

        /* IStudioService.getMyUserMatrix(user.userId).then(res => {
            let map = {}
            res.data.data.forEach(item => {
                map[item] = true
            })
            this.setState({
                matrix: map
            })
        }) */
    }

    save() {
        let includes = Object.keys(this.state.matrix)
            .filter(key => this.state.matrix[key] == true)
        let excludes = Object.keys(this.state.matrix)
            .filter(key => this.state.matrix[key] == false)

        includes.push(mtx.ACCESS_HOME)

        this.setState({ loading: true })

        IStudioService.updateMyUserMatrix(this.state.selectedUser.userId, includes, excludes).then(res => {
            setTimeout(() => {
                this.setState({
                    success: true,
                    dataChanged: false,
                    loading: false
                })
            }, 700);
        }).catch(e => {
            this.setState({
                success: false,
                loading: false,
                error: e.response ? e.response.data.message : 'Service Unavailable, please try again later.'
            })
        })
    }
    directUrl(url) {

        let switchTheme = window.localStorage.getItem("switchTheme")
        if (switchTheme != null) {
            this.setState({
                switchTheme: switchTheme
            })
            if (switchTheme == "white_theme") {
                window.location.href = `${process.env.URL_PREFIX}${url}?mode=white`
            }
            else {
                window.location.href = `${process.env.URL_PREFIX}${url}?mode=dark`
            }
        }
    }
    
    render() {
        return (
            <Setting_Layout title="Users" mode={this.state.switchTheme}>
                <div class="col-3  pl-2 pr-0 fade-in draft_container">
                    <div class="mt-3 d-flex justify-content-between pt-0" style={{ height: "10%" }}>
                        <div>
                            <p class="py-0 m-0 pl-3 font-size-21 font-medium black-font theme-font-color">Users</p>
                        </div>
                        <div>
                            <button className="draft-proceed-btn btn-primary mr-3" onClick={() => this.directUrl("/setting/users/create")}>Add User</button>
                        </div>
                    </div>

                    <div className="m-0 p-0 pr-1 draft-panel overflow-auto" style={{ height: '90%' }}>
                        <div style={{ padding: '0px 3px 0px 3px' }}>
                            {
                                this.state.users.map((user, index) => {
                                    return (                                       
                                        <div 
                                            key={user.userId} 
                                            className={this.state.selectedUserIndex == index ? "col draft-list-item py-3 pl-4 mb-3 draft-list-item_active" : "col draft-list-item py-3 pl-4 mb-3"}
                                            onClick={() => {
                                            this.onUserSelected(user, index)
                                        }}
                                        >
                                            <div className="pt-1 pb-2 font-size-14 black-font font-semi theme-font-color">{user.displayName}</div>
                                            <div className="pb-1 font-size-14 font-medium">{user.email}</div>
                                        </div>
                                    );
                                })
                            }
                        </div>
                    </div>
                </div>
                {/* drafts panel */}
                {/* right panel */}
                <div className="col-7 p-0 fade-in" style={{ height: '100%' }}>
                    <div className="col  border-bottom p-0 d-flex align-items-center  noti-draft-right-panel-top-height theme-title-color">
                        <div className="col row p-0 m-0">
                            <div className="col-xl-6 " style={{ marginTop: '5px', marginBottom: '-5px' }}>
                                <div className="font-size-21 ml-4 font-medium theme-font-color">
                                    {this.state.selectedUser && this.state.selectedUser.displayName}
                                </div>
                            </div>
                            <div className="col-xl-6 row m-0 d-flex justify-content-end">                            
                                {
                                    this.state.selectedUser ?
                                    <>
                                        <button className="draft-proceed-btn btn-primary mr-2" 
                                        onClick={() => this.directUrl(`/setting/users/edit/${this.state.selectedUser.userId}`)}
                                        >Edit</button>                               
                                        <button className="draft-proceed-btn btn-primary" onClick={(e)=>this.save()}>Save</button>  
                                    </>
                                    :
                                    null
                                }                                                              
                            </div>
                        </div>
                    </div>
                    <div className="col p-0 m-0 ml-3 row  noti-draft-right-panel-btm-height">
                        {/* draft details panel */}
                        <div className="col d-flex flex-column justify-content-start py-4 panel overflow-auto" style={{ height: '99%' }}>                            
                            <UserSettingModules
                            className="theme-background"
                            matrix={this.state.matrix}
                            onMatrixChanged={matrix => {
                                this.setState({ matrix, dataChanged: true })
                            }} />
                        </div>
                    </div>

                </div>
                {this.state.error ?
                    <Snack
                        color='#d74338'
                        text={this.state.error}
                        vertical='bottom'
                        horizontal='left'
                        open={this.state.error != null}
                        onClose={() => {
                            this.setState({ error: null })
                        }}
                        onActionButtonClick={() => {
                            this.setState({ error: null })
                        }}
                    />
                    : null
                }
                {this.state.success ?
                    <Snack
                        color='green'
                        text={this.state.success}
                        vertical='bottom'
                        horizontal='left'
                        open={this.state.success != null}
                        onClose={() => {
                            this.setState({ success: null })
                        }}
                        onActionButtonClick={() => {
                            this.setState({ success: null })
                        }}
                    />
                    : null
                }
            </Setting_Layout>
        )
    }
}

export default withAuth(withRouter(Users))