import MyService from "../../services/my-service";
import { Snack } from '../../components/snack';
import Setting_Layout from '../../components/setting_layout';
import Madatory_Slider from '../../components/madatory_slider';
import { withAuth } from '../../components/auth-component';
import FormField from '../../components/form-field';
import FormFieldPassword from '../../components/form-field-password';
import { withRouter } from "next/router";
import IStudioService from "../../services/istudio-service";
import update from 'immutability-helper';
import * as _ from 'lodash';
import * as mtx from '../../services/access-service';
import { ACCESS_PUSH_DRAFT, AccessService, ACCESS_PUSH_DRAFT_SEND } from '../../services/access-service';
import UserSettingModules from './user-setting-modules';
import Select from 'react-select';
import AppButton from '../../components/app-button';
import { Row, Col, Card, Form, Button, Alert } from 'reactstrap'

class EditUserPage extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            error: null,
            success: null,
            zeroState: true,
            switchTheme: this.props.router.query.mode == "dark" ? "dark_theme" : "white_theme",
            title: props.router.query.title || 'Edit User',
            matrix: {},
            loading: false,
            isLoading: true,
            success: null,
            userId: props.router.query.user_id,
            user: null,
            users: [],
            modal: { delete: false },
            viewPassword: false,
            superior1: null,
            superior2: null,
            formData: {
                username: null,
                email: null,
                displayName: null,
                superiors: []
            },
        }
    }

    componentDidMount() {
        this.initData()
    }

    async initData() {
        if (this.state.userId != null) {

            IStudioService.getAdminUsers().then(res => {
                this.setState({
                    users: res.data
                })
            })
            // TODO: API - add this api
            let res1 = await IStudioService.getUserDetails(this.state.userId)
            let res2 = await IStudioService.getSuperiors()
            let user = res1.data.data
            let superiors = res2.data.map(o => {
                return {
                    label: o.displayName,
                    value: o.userId
                }
            })

            this.setState({
                user,
                isLoading: false,
                formData: {
                    displayName: user.displayName,
                    email: user.email,
                    username: user.username,
                    superiors: user.superiors,
                },
                superiors,
            }, () => {
                this.split(user.superiors)
            })
        }
    }

    split(item) {
        let u = this.state.users.filter(u => {
            return item.filter(i => i == u.userId).length > 0
        })

        if (u.length > 0) {
            this.setState({
                superior1: this.state.superiors.find(i => i.value == u[0].userId),
                superior2: this.state.superiors.find(i => i.value == u[1].userId),
            })
        }
        // })


    }

    // funGPStoCorordinate(item) {
    //     var latitude = (users['gpsCoordinates'] !== undefined) ? item['gpsCoordinates'].split(',')[0].trim() : ''
    //     item['latitude'] = latitude
    //     var longtitude = (item['gpsCoordinates'] !== undefined) ? item['gpsCoordinates'].split(',')[1].trim() : ''
    //     item['longitude'] = longtitude
    //     console.log(item)
    // }

    validateSuperiors() {
        if (this.state.superior1 != null
            && this.state.superior2 != null
            && this.state.superior1 == this.state.superior2) {
            this.setState({
                formError: '2 Superiors cannot be the same person'
            })
            return false
        }
        return true
    }

    submit(e) {
        e.preventDefault()

        this.setState({ loading: true })

        if (!this.validateSuperiors()) return

        this.state.formData.superiors = []
        if (this.state.superior1 != null) {
            this.state.formData.superiors.push(this.state.superior1.value)
        }

        if (this.state.superior2 != null) {
            this.state.formData.superiors.push(this.state.superior2.value)
        }
        let error = '';
        if (this.state.passwordError) {
            this.setState({ error: this.state.passwordError, loading: false });
            return;
        }

        // TODO: API - add this api
        IStudioService.updateUser(this.state.userId, this.state.formData).then(res => {
            this.setState({
                loading: false,
                success: true
            })
        }).catch(e => {
            this.setState({
                success: false,
                loading: false,
                error: e.response ? e.response.data.message : 'Service Unavailable, please try again later.'
            })
        })
    }

    deleteUser() {
        IStudioService.deleteUser(this.state.userId).then(res => {
            this.setState({
                success: true,
                modal: {
                    delete: false,
                    successMsg: 'User successfully deleted'
                }
            })
            window.location.assign(`${process.env.URL_PREFIX}/settings/team`)
        }).catch(e => {
            this.setState({
                success: false,
                modal: {
                    delete: false
                },
                error: e.response ? e.response.data.message : 'Service Unavailable, please try again later.',
            })
        })
    }

    validatePassword(password, confirmPassword) {
        let passwordError
        if (password != confirmPassword) {
            passwordError = 'Password not matched'
        } else if (!password || password.trim() == '' || password.length < 8) {
            passwordError = 'Password must be at least 8-character long'
        }
        return passwordError
    }

    onSnackClose() {
        this.setState({
            success: null, error: null
        })
        if (this.props.onSnackClose) {
            this.props.onSnackClose()
        }
    }
    directUrl(url) {

        let switchTheme = window.localStorage.getItem("switchTheme")
        if (switchTheme != null) {
            this.setState({
                switchTheme: switchTheme
            })
            if (switchTheme == "white_theme") {
                window.location.href = `${process.env.URL_PREFIX}${url}?mode=white`
            }
            else {
                window.location.href = `${process.env.URL_PREFIX}${url}?mode=dark`
            }
        }
    }
    render() {
        return (
            <Setting_Layout title="Users" mode={this.state.switchTheme}>
                {
                    this.state.isLoading ?
                        <div class="spinner-border text-primary center_spinner" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        :
                        <>
                            <div class="col-3 pl-2 pr-0 fade-in draft_container">
                                <div class="mt-1 d-flex justify-content-between pt-0" style={{ height: "10%" }}>
                                    <div onClick={() => this.directUrl("/setting/users")}>
                                        <p class="py-0 m-0 font-size-21 font-medium black-font theme-font-color">
                                            <span ><a className="font-size-21 font-medium theme-font-color"><img class="mr-3" src={`${process.env.URL_PREFIX}/static/img/Arrow - Back.png`} alt="" />Edit User</a></span>
                                        </p>
                                    </div>
                                </div>
                                <Form
                                    onSubmit={(e) => {
                                        this.submit(e)
                                    }}>
                                    <div className="font-size-18 pt-1 font-medium black-font theme-font-color">Account Information</div>
                                    <div className="font-size-16 pt-1 font-medium black-font theme-font-color">User's Account details</div>
                                    <hr />
                                    <div className="row form-group pt-1 d-flex align-items-center">
                                        <FormField
                                            title='Display Name'
                                            required={true}
                                            placeholder='John Doe'
                                            value={this.state.formData.displayName || ''}
                                            onChange={e => {
                                                let formData = update(this.state.formData, {
                                                    displayName: {
                                                        $set: e.target.value
                                                    }
                                                })
                                                this.setState({ formData })
                                            }}
                                        />
                                    </div>
                                    <div className="row form-group pt-1 d-flex align-items-center">
                                        <FormField
                                            title='Username'
                                            required={true}
                                            placeholder='johndoe'
                                            disabled={true}
                                            value={this.state.formData.username || ''}
                                            onChange={e => {
                                                let formData = update(this.state.formData, {
                                                    username: {
                                                        $set: e.target.value
                                                    }
                                                })
                                                this.setState({ formData })
                                            }}
                                        />
                                    </div>
                                    <div className="row form-group pt-1 d-flex align-items-center">
                                        <FormField
                                            title='Email'
                                            type='email'
                                            required={true}
                                            value={this.state.formData.email || ''}
                                            placeholder='abc@gmail.com'
                                            onChange={e => {
                                                let formData = update(this.state.formData, {
                                                    email: {
                                                        $set: e.target.value
                                                    }
                                                })
                                                this.setState({ formData })
                                            }}
                                        />
                                    </div>
                                    <div className="row form-group pt-1 d-flex align-items-center">
                                        <label htmlFor="name" className="col-sm-12 col-form-label grey-font font-size-14">Superior 1</label>
                                        <div className="col-sm-12">
                                            <Select
                                                options={this.state.superiors}
                                                value={this.state.superior1}
                                                onChange={item => {
                                                    this.setState({
                                                        superior1: item
                                                    })
                                                }}
                                            />
                                        </div>
                                    </div>

                                    <div className="row form-group pt-1 d-flex align-items-center">
                                        <label htmlFor="name" className="col-sm-12 col-form-label grey-font font-size-14">Superior 2</label>
                                        <div className="col-sm-12">
                                            <Select
                                                options={this.state.superiors}
                                                value={this.state.superior2}
                                                onChange={item => {
                                                    this.setState({
                                                        superior2: item
                                                    })
                                                }}
                                            />
                                        </div>
                                    </div>
                                    {/* <div className="row m-0 d-flex justify-content-center">
                        <button onClick={(e) => this.submit(e)} className="draft-proceed-btn btn-primary" style={{width:"100%"}}>Continue</button>
                    </div> */}
                                    <AppButton
                                        loading={this.state.loading}
                                        disabled={this.state.loading}>
                                        CONTINUE
                    </AppButton>
                                </Form>

                            </div>
                            <div className="col-5 p-0 fade-in" style={{ height: '100%' }}>
                                <div className="ml-5 mr-5">
                                    <div className="font-size-18 font-medium black-font theme-font-color" style={{ marginTop: "68px" }}>Security</div>
                                    <div className="font-size-16 font-medium black-font theme-font-color">Change User's Password</div>
                                    <hr style={{ marginTop: "20px" }} />
                                    <div className="row form-group pt-1 d-flex align-items-center">
                                        <FormFieldPassword
                                            title='Password'
                                            type={this.state.viewPassword ? 'text' : 'password'}
                                            icon={this.state.viewPassword ? 'fa fa-eye-slash' : 'fa fa-eye'}
                                            value={this.state.formData.password || ''}
                                            placeholder='******'
                                            onIconClick={() => {
                                                this.setState({
                                                    viewPassword: !this.state.viewPassword
                                                })
                                            }}
                                            onChange={e => {
                                                let formData = update(this.state.formData, {
                                                    password: {
                                                        $set: e.target.value
                                                    }
                                                })
                                                this.setState({
                                                    formData,
                                                    passwordError: this.validatePassword(formData.password, formData.confirmPassword)
                                                })
                                            }}
                                        />
                                    </div>
                                    <div className="row form-group pt-1 d-flex align-items-center">
                                        <FormFieldPassword
                                            title='Confirm Password'
                                            type={this.state.viewPassword ? 'text' : 'password'}
                                            icon={this.state.viewPassword ? 'fa fa-eye-slash' : 'fa fa-eye'}
                                            value={this.state.formData.confirmPassword || ''}
                                            placeholder='******'
                                            onIconClick={() => {
                                                this.setState({
                                                    viewPassword: !this.state.viewPassword
                                                })
                                            }}
                                            onChange={e => {
                                                let formData = update(this.state.formData, {
                                                    confirmPassword: {
                                                        $set: e.target.value
                                                    }
                                                })
                                                this.setState({
                                                    formData,
                                                    passwordError: this.validatePassword(formData.password, formData.confirmPassword)
                                                })
                                            }}
                                        />
                                    </div>
                                </div>
                            </div>

                        </>
                }
                {this.state.error ?
                    <Snack
                        color='#d74338'
                        text={this.state.error}
                        vertical='bottom'
                        horizontal='left'
                        open={this.state.error != null}
                        onClose={() => {
                            this.setState({ error: null })
                        }}
                        onActionButtonClick={() => {
                            this.setState({ error: null })
                        }}
                    />
                    : null
                }
                {this.state.success ?
                    <Snack
                        color='green'
                        text={this.state.success}
                        vertical='bottom'
                        horizontal='left'
                        open={this.state.success != null}
                        onClose={() => {
                            this.setState({ success: null })
                        }}
                        onActionButtonClick={() => {
                            this.setState({ success: null })
                        }}
                    />
                    : null
                }
            </Setting_Layout>
        )
    }
}

export default withAuth(withRouter(EditUserPage))