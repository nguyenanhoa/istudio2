import MyService from "../../services/my-service";
import { Snack } from '../../components/snack';
import Setting_Layout from '../../components/setting_layout';
import Madatory_Slider from '../../components/madatory_slider';
import { withAuth } from '../../components/auth-component';
import { withRouter } from "next/router";
class Token extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            zeroState: true,
            switchTheme: this.props.router.query.mode == "dark" ? "dark_theme" : "white_theme"
        }
    }

    componentDidMount() {

    }
    render() {
        return (
            <Setting_Layout title="General" mode={this.state.switchTheme}>
                <Madatory_Slider madatory="token" mode={this.state.switchTheme} />
                {/* drafts panel */}
                {/* right panel */}
                <div className="col-8 p-0 fade-in" style={{ height: '100%' }}>
                    <div className="col border-bottom p-0 d-flex align-items-center  noti-draft-right-panel-top-height theme-title-color">
                        <div className="col row p-0 m-0">
                            <div className="col-xl-6 " style={{ marginTop: '5px', marginBottom: '-5px' }}>
                                <div className="font-size-21 ml-1 font-medium theme-font-color">JWT Token</div>
                            </div>
                            <div className="col-xl-6 row m-0 d-flex justify-content-end">
                                <button className="draft-proceed-btn btn-primary" data-toggle="modal" data-target="#proceed">Update</button>
                            </div>
                        </div>
                    </div>
                    <div className="col p-0 m-0 row  noti-draft-right-panel-btm-height">
                        {/* draft details panel */}
                        <div className="col d-flex flex-column justify-content-start py-4 panel overflow-auto" style={{ height: '99%' }}>
                            <div className="font-size-18 pb-2 font-medium theme-font-color">Increase the security with JWT Token</div>
                            <div className="row form-group pt-1 d-flex align-items-center">
                                <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">JWT Key</label>
                                <div className="col-sm-7">
                                    <input type="text" className="form-control draft-input-height" placeholder="Mdejsov2BS" />
                                </div>
                            </div>
                            <div className="row form-group pt-1 d-flex align-items-center">
                                <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">JWT TTL</label>
                                <div className="col-sm-7">
                                    <input type="text" className="form-control draft-input-height" placeholder={30} />
                                </div>
                            </div>
                            <div className="row form-group pt-1 d-flex align-items-center">
                                <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">JWT Refresh TTL</label>
                                <div className="col-sm-7">
                                    <input type="text" className="form-control draft-input-height" placeholder={300} />
                                </div>
                            </div>
                            <div className="row form-group pt-1 d-flex align-items-center">
                                <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">JWT Algorythm</label>
                                <div className="col-sm-7">
                                    <input type="text" className="form-control draft-input-height" placeholder="HS256" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Setting_Layout>
        )
    }
}

export default withAuth(withRouter(Token))