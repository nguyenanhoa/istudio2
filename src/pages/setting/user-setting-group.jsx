
/**
 * @author Guster
 * @email guster@inglab.com.my
 * @create date 2020-04-16 15:35:19
 * @modify date 2020-04-16 15:35:19
 * @desc [description]
 */

import React, { Component } from 'react'
import Utils from '../../services/utils'
import { Row, Col, Card, Tooltip } from 'reactstrap'
import InfoTooltip from '../../components/info-tooltip'

export default class UserSettingGroup extends Component {

    constructor(props) {
        super(props)
        this.state = {
            tooltip: false,
            lv0id: Utils.uuid()
        }
    }

    render() {
        return (
            <div className={this.props.className}>
                <div className={`d-flex`}>
                    <h4 className="font-weight-bold subtitle ml-2">{this.props.title}</h4>
                    <InfoTooltip id={`tooltip-${this.props.id}`} tooltip={this.props.tooltip} />
                    {this.props.checkable ?
                        <label htmlFor={this.state.lv0id} className='ml-3' style={{ marginTop: '3px' }}>
                            <input
                                id={this.state.lv0id}
                                type='checkbox'
                                data-toggle="toggle" data-size="sm"
                                checked={this.props.checked}
                                onChange={e => {
                                    if (this.props.onChange) {
                                        this.props.onChange(e.target.checked)
                                    }
                                }} />
                            <span className="slider round"></span>
                        </label>
                        : null}
                </div>
                {this.props.children ?
                    <Card className='p-3 align-items-middle'>
                        {this.props.children}
                    </Card>
                    : null}
            </div>
        )
    }
}
