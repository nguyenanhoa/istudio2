import MyService from "../../services/my-service";
import { Snack } from '../../components/snack';
import Setting_Layout from '../../components/setting_layout';
import Madatory_Slider from '../../components/madatory_slider';
import { withAuth } from '../../components/auth-component';
import { withRouter } from "next/router";
class FileServer extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            zeroState: true,
            switchTheme: this.props.router.query.mode == "dark" ? "dark_theme" : "white_theme"
        }
    }

    componentDidMount() {

    }
    render() {
        return (
            <Setting_Layout title="General" mode={this.state.switchTheme}>
                <Madatory_Slider madatory="fileserver" mode={this.state.switchTheme} />
                {/* drafts panel */}
                {/* right panel */}
                <div className="col-8 p-0 fade-in" style={{ height: '100%' }}>
                    <div className="col border-bottom p-0 d-flex align-items-center  noti-draft-right-panel-top-height theme-title-color">
                        <div className="col row p-0 m-0">
                            <div className="col-xl-4 " style={{ marginTop: '5px', marginBottom: '-5px' }}>
                                <div className="font-size-21 ml-1 font-medium theme-font-color">File Server Configuration</div>
                            </div>
                            <div className="col-xl-8 row m-0 d-flex justify-content-end">
                                <button className="green-font font-size-14 mr-3" style={{ background: 'transparent', border: '0px' }}>Success @ 30-Dec-20, 05:17 PM</button>
                                <button className="draft-detail-btn theme-font-color mr-3" style={{ width: '180px' }}>Test Connection</button>
                                <button className="draft-proceed-btn btn-primary">Update</button>
                            </div>
                        </div>
                    </div>
                    <div className="col p-0 m-0 row  noti-draft-right-panel-btm-height">
                        {/* draft details panel */}
                        <div className="col d-flex flex-column justify-content-start py-4 panel overflow-auto" style={{ height: '99%' }}>
                            <div className="font-size-18 pb-2 font-medium theme-font-color">Connect to a File Server</div>
                            <div className="row form-group pt-1 d-flex align-items-center">
                                <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">Type</label>
                                <div className="col-sm-7">
                                    <select className="form-control draft-input-height" style={{ margin: '0px' }}>
                                        <option>FTP - File Transfer Protocol</option>
                                        <option>SFTP - SSH File Transfer Protocol</option>
                                    </select>
                                </div>
                            </div>
                            <div className="row form-group pt-1 d-flex align-items-center">
                                <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">Server Host</label>
                                <div className="col-sm-4">
                                    <input type="text" className="form-control draft-input-height" placeholder="storage.inglab.com.my" />
                                </div>
                                <div className="col-sm-1">
                                    <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">Port</label>
                                </div>
                                <div className="col-sm-2">
                                    <input type="text" className="form-control draft-input-height" placeholder={44} />
                                </div>
                            </div>
                            <div className="row form-group pt-1 d-flex align-items-center">
                                <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">Username</label>
                                <div className="col-sm-7">
                                    <input type="text" className="form-control draft-input-height" placeholder="no-reply@inglab.my" />
                                </div>
                            </div>
                            <div className="row form-group pt-1 d-flex align-items-center">
                                <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">Password</label>
                                <div className="col-sm-7">
                                    <input type="password" className="form-control draft-input-height" placeholder="******" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Setting_Layout>
        )
    }
}

export default withAuth(withRouter(FileServer))