import MyService from "../../services/my-service";
import { Snack } from '../../components/snack';
import Setting_Layout from '../../components/setting_layout';
import Madatory_Slider from '../../components/madatory_slider';
import { withAuth } from '../../components/auth-component';
import { withRouter } from "next/router";
class General extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            zeroState: true,
            switchTheme: this.props.router.query.mode == "dark" ? "dark_theme" : "white_theme"
        }
    }

    componentDidMount() {

    }
    render() {
        return (
            <Setting_Layout title="General" mode={this.state.switchTheme}>
                <Madatory_Slider madatory="general" mode={this.state.switchTheme} />
                {/* drafts panel */}
                {/* right panel */}
                <div className="col-8 p-0 fade-in" style={{ height: '100%' }}>
                    <div className="col border-bottom p-0 d-flex align-items-center  noti-draft-right-panel-top-height theme-title-color">
                        <div className="col row p-0 m-0">
                            <div className="col-xl-6 " style={{ marginTop: '5px', marginBottom: '-5px' }}>
                                <div className="font-size-21 ml-1 font-medium theme-font-color">General Settings</div>
                            </div>
                            <div className="col-xl-6 row m-0 d-flex justify-content-end">
                                <button className="draft-proceed-btn btn-primary" data-toggle="modal" data-target="#proceed">Update</button>
                            </div>
                        </div>
                    </div>
                    <div className="col p-0 m-0 row  noti-draft-right-panel-btm-height">
                        {/* draft details panel */}
                        <div className="col d-flex flex-column justify-content-start py-4 panel overflow-auto" style={{ height: '99%' }}>
                            <div className="font-size-18 pb-2 font-medium theme-font-color">Project</div>
                            <div className="row form-group pt-1 d-flex align-items-center">
                                <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">Project Name</label>
                                <div className="col-sm-7">
                                    <input type="text" className="form-control draft-input-height" id="name" placeholder="Ingenious Lab" />
                                </div>
                            </div>
                            <div className="row form-group pt-1 d-flex align-items-center">
                                <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">Domain Name</label>
                                <div className="col-sm-2">
                                    <select className="form-control draft-input-height" style={{ margin: '0px' }}>
                                        <option>http://</option>
                                        <option selected>https://</option>
                                    </select>
                                </div>
                                <div className="col-sm-5">
                                    <input type="text" className="form-control draft-input-height" id="name" placeholder="www.inglab.com.my" />
                                </div>
                            </div>
                            <div className="row form-group pt-1 d-flex align-items-center">
                                <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">System Time Zone</label>
                                <div className="col-sm-7">
                                    <select className="form-control draft-input-height" style={{ margin: '0px' }}>
                                        <option>GMT -12:00</option>
                                        <option>GMT -11:00</option>
                                        <option>GMT -10:00</option>
                                        <option>GMT -09:00</option>
                                        <option>GMT -08:00</option>
                                        <option>GMT -07:00</option>
                                        <option>GMT -06:00</option>
                                        <option>GMT -05:00</option>
                                        <option>GMT -04:00</option>
                                        <option>GMT -03:00</option>
                                        <option>GMT -02:00</option>
                                        <option>GMT -01:00</option>
                                        <option>GMT +00:00</option>
                                        <option>GMT +01:00</option>
                                        <option>GMT +02:00</option>
                                        <option>GMT +03:00</option>
                                        <option>GMT +04:00</option>
                                        <option>GMT +05:00</option>
                                        <option>GMT +06:00</option>
                                        <option>GMT +07:00</option>
                                        <option selected>GMT +08:00</option>
                                        <option>GMT +09:00</option>
                                        <option>GMT +10:00</option>
                                        <option>GMT +11:00</option>
                                        <option>GMT +12:00</option>
                                        <option>GMT +13:00</option>
                                        <option>GMT +14:00</option>
                                    </select>
                                </div>
                            </div>
                            <div className="font-size-18 pt-4 pb-2 font-medium black-font">Administration</div>
                            <div className="row form-group pt-1 d-flex align-items-center">
                                <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">Admin Email</label>
                                <div className="col-sm-7">
                                    <input type="text" className="form-control draft-input-height" id="name" placeholder="admin@mail.com" />
                                </div>
                            </div>
                            <div className="row form-group pt-1 d-flex align-items-center">
                                <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">Admin User Name</label>
                                <div className="col-sm-7">
                                    <input type="text" className="form-control draft-input-height" id="name" placeholder="admin" />
                                </div>
                            </div>
                            <div className="row form-group pt-1 d-flex align-items-center">
                                <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">Admin Password</label>
                                <div className="col-sm-7">
                                    <input type="text" className="form-control draft-input-height" id="name" placeholder="*****" />
                                </div>
                            </div>
                            <div className="font-size-18 pt-4 pb-2 font-medium black-font">Security</div>
                            <div className="row form-group pt-1 d-flex align-items-center">
                                <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">AES256 Encryption</label>
                                <div className="col-sm-7">
                                    <input type="checkbox" data-toggle="toggle" data-size="sm" />
                                </div>
                            </div>
                            <div className="row form-group pt-1 d-flex align-items-center">
                                <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">HSTS HTTP</label>
                                <div className="col-sm-7">
                                    <input type="checkbox" data-toggle="toggle" data-size="sm" />
                                </div>
                            </div>
                            <div className="row form-group pt-1 d-flex align-items-center">
                                <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">Diffie Hellman (DH)</label>
                                <div className="col-sm-7">
                                    <input type="checkbox" data-toggle="toggle" data-size="sm" />
                                </div>
                            </div>
                            <div className="row form-group pt-1 d-flex align-items-center">
                                <label htmlFor="name" className="col-sm-2 col-form-label grey-font font-size-14">SSL Pinning</label>
                                <div className="col-sm-7">
                                    <input type="checkbox" defaultChecked data-toggle="toggle" data-size="sm" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </Setting_Layout>
        )
    }
}

export default withAuth(withRouter(General))