import MyService from "../../services/my-service";
import { Snack } from '../../components/snack';
import Reset_Layout from '../../components/reset_layout';
import { withRouter } from "next/router";

class Reset_Password extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            showpassword: false
        }


    }

    componentDidMount() {

    }
    showPassword() {
        if (this.state.showpassword) {
            this.setState({ showpassword: false })
        }
        else {
            this.setState({ showpassword: true })
        }
    }
    render() {
        return (
            <Reset_Layout title="Reset Password">
                <div class="img-div d-flex justify-content-center">
                    <img class="reset-password-background position-absolute" src={`${process.env.URL_PREFIX}/static/img/ic_background.jpg`} alt="" />
                    <div class="background-cover position-absolute"></div>
                    <div class="w-25 shadow d-flex flex-column justify-content-center align-items-center reset-password-panel position-relative">
                        <img class="mt-4" src={`${process.env.URL_PREFIX}/static/img/logo_iStudio@3x.png`} alt="iStudio logo" width="187px" height="52px" />
                        <h2 class="mt-3 mb-2">Reset Password</h2>
                        <div action="" class="w-75 d-flex flex-column justify-content-center my-5">
                            <div class="col-15 form-group my-2">
                                <input type="password" class="form-control form-control-lg field-placeholder" placeholder="Enter Current Password" />
                                <span class="field-icon"></span>
                            </div>
                            <div class="col-15 form-group my-2">
                                <input type="password" class="form-control form-control-lg field-placeholder" placeholder="Enter New Password" />
                                <span class="field-icon"></span>
                            </div>
                            <button class="btn btn-primary mt-5 btn-lg" type="submit">Log In</button>
                        </div>
                    </div>
                </div>
            </Reset_Layout>
        )
    }
}

export default withRouter(Reset_Password)