import MyService from "../../services/my-service";
import IStudioService from "../../services/istudio-service";
import { Snack } from '../../components/snack';
import update from 'immutability-helper';
import Authentical_Layout from '../../components/authentical_layout'
import { withRouter } from "next/router";
class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            showpassword: false,
            data: {},
            error: null,
            success: null
        }
    }
    componentDidMount() {



    }
    updateFormData(key, value) {
        let data = update(this.state.data, {
            [key]: {
                $set: value
            }
        })
        this.setState({ data })
    }
    async submitForm(e) {
        e.preventDefault();
        this.setState({
            error:null
        })
        let error = null;
        //Validate value before submit
        if (!this.state.data.username) {
            error = 'Username is required'
        } else if (!this.state.data.password) {
            error = 'Password is required'
        }
        if (error) {
            this.setState({ error: error });
            return;
        }        
        try {
            let res = await IStudioService.login(this.state.data.username, this.state.data.password)
            window.localStorage.setItem("username",this.state.data.username)
            window.localStorage.setItem("switchTheme","white_theme")      
            if(res!=undefined)
            {                
                //window.location.href=`${process.env.URL_PREFIX}/`;
                window.location.href=`${process.env.URL_PREFIX}/?mode=white`
            }

        } catch (e) {
            this.setState({
                error: e.response ? e.response.data.message : 'Service Unavailable, please try again later.'
            })
        }
    }
    showPassword() {
        if (this.state.showpassword) {
            this.setState({ showpassword: false })
        }
        else {
            this.setState({ showpassword: true })
        }
    }
    render() {
        return (
            <Authentical_Layout title="Login">
                {/* Snack Message  */}
                {this.state.error ?
                    <Snack
                        color='#d74338'
                        text={this.state.error}
                        vertical='bottom'
                        horizontal='left'
                        open={this.state.error != null}
                        onClose={() => {
                            this.setState({ error: null })
                        }}
                        onActionButtonClick={() => {
                            this.setState({ error: null })
                        }}
                    />
                    : null
                }
                {this.state.success ?
                    <Snack
                        color='green'
                        text={this.state.success}
                        vertical='bottom'
                        horizontal='left'
                        open={this.state.success != null}
                        onClose={() => {
                            this.setState({ success: null })
                        }}
                        onActionButtonClick={() => {
                            this.setState({ success: null })
                        }}
                    />
                    : null
                }
                {/*  */}
                <div class="h-75 m-auto col row justify-content-between">
                    <div class="col-md-5 d-flex flex-column justify-content-center auth-panel fade-in">
                        <h1 class="text-center my-4 mx-5">Login</h1>
                        <div class="d-flex flex-column justify-content-center m-5">
                            <div class="form-group">
                                <input type="text" id="username" name="username" value={this.state.data.username || ''} onChange={(e) => this.updateFormData("username", e.target.value)} class="form-control form-control-lg auth-user-input" autofocus />
                            </div>
                            <div class="form-group my-2">
                                <input type={this.state.showpassword ? "text" : "password"} id="password" name="password" value={this.state.data.password || ''} onChange={(e) => this.updateFormData("password", e.target.value)} class="form-control form-control-lg auth-password-input" />
                                <span class="field-icon" onClick={(e) => this.showPassword()}></span>
                            </div>
                            <div class="form-check my-2">
                                <input type="checkbox" class="form-check-input" name="" id="defaultCheck1" />
                                <label class="form-check-label" for="defaultCheck1">
                                    Keep me logged in
                          </label>
                            </div>
                            <button class="btn btn-primary mt-5 btn-lg" onClick={(e) => this.submitForm(e)} >Log In</button>
                            <div class="d-flex justify-content-center mt-4">
                                <a href={`${process.env.URL_PREFIX}/forgot_password`}>Can’t access your account?</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 d-flex flex-column justify-content-center align-items-center text-center">
                        <img class="mb-4" src={`${process.env.URL_PREFIX}/static/img/logo_iStudio@3x.png`} alt="logo" style={{ height: "62px" }} />
                        <img class="m-5" src={`${process.env.URL_PREFIX}/static/img/Illustration_Login.png`} alt="login" style={{ height: "235px" }} />
                        <h3 class="mb-3">Helping you build a lasting brand</h3>
                        <h5 style={{ lineHeight: "150%", color: "#9599B6" }}>We help you manage your marketing strategies easier and more effective than even before.</h5>
                    </div>
                </div>
            </Authentical_Layout>
        )
    }
}

export default withRouter(Login)