import MyService from "../../services/my-service";
import IStudioService from "../../services/istudio-service";
import update from 'immutability-helper';
import { Snack } from '../../components/snack';
import Authentical_Layout from '../../components/authentical_layout';
import { withRouter } from "next/router";

class ForgotPassword extends React.Component {

    constructor(props) {
        super(props)
        this.state = {           
            data: {},
            error: null,
            success: null
        }


    }

    componentDidMount() {

    }

    updateFormData(key, value) {
        let data = update(this.state.data, {
            [key]: {
                $set: value
            }
        })
        this.setState({ data })
    }
    
    validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    submitForm(e)
    {
        e.preventDefault();
        this.setState({
            error:null
        })
        let error = null;
              
        if (!this.state.data.emailaddress) {
            error = 'Email Address is required'
        } else if (!this.validateEmail(this.state.data.emailaddress)) {
            error="Email address must contain “@” and “.” "
        }
        if (error) {
            this.setState({ error: error });
            return;
        }
               
        MyService.forgetPassword(this.state.data.emailaddress).then(res => {            
            if(res.data.message!="")
            {                
                window.location.href=`${process.env.URL_PREFIX}/forgot_password_success`;
            }
        }).catch(e => {
            
            this.setState({                
                error: e.response ? e.response.data.message : 'Service Unavailable, please try again later.'
            })
        })
    }
    render() {
        return (
            <Authentical_Layout title="Login">
                {/* Snack Message  */}
                {this.state.error ?
                    <Snack
                        color='#d74338'
                        text={this.state.error}
                        vertical='bottom'
                        horizontal='left'
                        open={this.state.error != null}
                        onClose={() => {
                            this.setState({ error: null })
                        }}
                        onActionButtonClick={() => {
                            this.setState({ error: null })
                        }}
                    />
                    : null
                }
                {this.state.success ?
                    <Snack
                        color='green'
                        text={this.state.success}
                        vertical='bottom'
                        horizontal='left'
                        open={this.state.success != null}
                        onClose={() => {
                            this.setState({ success: null })
                        }}
                        onActionButtonClick={() => {
                            this.setState({ success: null })
                        }}
                    />
                    : null
                }
                {/*  */}
                <div class="h-75 m-auto col row justify-content-between">
                    <div class="col-md-5 d-flex flex-column justify-content-center auth-panel fade-in">
                        <div class="col back-arrow px-5 mt-3 pt-5">
                            <span class="font-weight-bold"><a href={`${process.env.URL_PREFIX}/login`}><img class="mr-3" src={`${process.env.URL_PREFIX}/static/img/Arrow - Back.png`} alt="" />Back to Login</a></span>
                        </div>
                        <h2 class="col text-center px-5">Forgot Password?</h2>
                        <div class="col px-5 text-center">
                            <h6 style={{ lineHeight: "150%", color: "#9599B6" }}>Don't worry, it happens! Just let us know the email address you signed up with and we will send you a link to reset password.</h6>
                        </div>
                        <div style={{ height: "15px" }}></div>
                        <div class="col d-flex flex-column justify-content-center px-5">
                            <div class="col form-group">
                                <input type="email" id="emailaddress" name="emailaddress" value={this.state.data.emailaddress || ''} onChange={(e) => this.updateFormData("emailaddress", e.target.value)} class="form-control form-control-lg field-placeholder auth-email-input  mb-2" placeholder="Email Address" autofocus />
                                <button class="col btn btn-primary mb-3 btn-lg" onClick={(e) => this.submitForm(e)}>Send Email</button>
                                <div class="col d-flex justify-content-center mb-5 pb-5">
                                    <a href={`${process.env.URL_PREFIX}/login`}>Back to Login</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 d-flex flex-column justify-content-center align-items-center text-center">
                        <img class="mb-4" src={`${process.env.URL_PREFIX}/static/img/logo_iStudio@3x.png`} alt="logo" style={{ height: "62px" }} />
                        <img class="m-5" src={`${process.env.URL_PREFIX}/static/img/Illustration_Login.png`} alt="login" style={{ height: "235px" }} />
                        <h3 class="mb-3">Helping you build a lasting brand</h3>
                        <h5 style={{ lineHeight: "150%", color: "#9599B6" }}>We help you manage your marketing strategies easier and more effective than even before.</h5>
                    </div>
                </div>
            </Authentical_Layout>
        )
    }
}

export default withRouter(ForgotPassword)