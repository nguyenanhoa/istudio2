import MyService from "../../services/my-service";
import { Snack } from '../../components/snack';
import Authentical_Layout from '../../components/authentical_layout';
import { withRouter } from "next/router";

class ForgotPasswordSuccess extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            showpassword: false
        }


    }

    componentDidMount() {

    }
    showPassword() {
        if (this.state.showpassword) {
            this.setState({ showpassword: false })
        }
        else {
            this.setState({ showpassword: true })
        }
    }
    render() {
        return (
            <Authentical_Layout title="Login">
                <div class="h-75 m-auto col row justify-content-between">
                    <div class="col-md-5 d-flex flex-column justify-content-center auth-panel fade-in">
                        <div class="col back-arrow mx-5 mt-2 pt-5">
                            <span class="font-weight-bold"><a href={`${process.env.URL_PREFIX}/login`}><img class="mr-3" src={`${process.env.URL_PREFIX}/static/img/Arrow - Back.png`} alt="" />Back to Login</a></span>
                        </div>
                        <h4 class="col text-center mt-5 px-5">An activation link has been sent to your email.</h4>
                        <div class="col px-5 mb-2 text-center">
                            <h6 style={{ lineHeight: "150%", color: "#9599B6" }}>You will receive an email shortly with a link to set up a new password for your account. Click the link to reset your password.
                            </h6>
                        </div>
                        <form action="" class="col d-flex flex-column justify-content-center mb-5 pb-5 px-5">
                            <a class="btn btn-primary mb-5 btn-lg" href={`${process.env.URL_PREFIX}/login`}>Got it!</a>
                        </form>
                    </div>
                    <div class="col-md-6 d-flex flex-column justify-content-center align-items-center text-center">
                        <img class="mb-4" src={`${process.env.URL_PREFIX}/static/img/logo_iStudio@3x.png`} alt="logo" style={{ height: "62px" }} />
                        <img class="m-5" src={`${process.env.URL_PREFIX}/static/img/Illustration_Login.png`} alt="login" style={{ height: "235px" }} />
                        <h3 class="mb-3">Helping you build a lasting brand</h3>
                        <h5 style={{ lineHeight: "150%", color: "#9599B6" }}>We help you manage your marketing strategies easier and more effective than even before.</h5>
                    </div>
                </div>
            </Authentical_Layout>
        )
    }
}

export default withRouter(ForgotPasswordSuccess)