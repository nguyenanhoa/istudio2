import MyService from "../../services/my-service";
import IStudioService from "../../services/istudio-service";
import { Snack } from '../../components/snack';
import Main_Layout_Analytic from '../../components/main_layout_analytic';
import { withAuth } from '../../components/auth-component';
import { withRouter } from "next/router";
import Moment from 'moment';

class Notification_Analytics extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            data: {},
            error: null,
            success: null,
            scheduleData: [],
            scheduleIndividual: {},
            filters: [],
            recipientType: "",
            switchTheme: this.props.router.query.mode == "dark" ? "dark_theme" : "white_theme",
            loading: true
        }
    }

    componentDidMount() {
        //Get data schedule from backend
        IStudioService.getSchedules().then(res => {
            this.setState({
                scheduleData: res.data.data,
                loading: false
            })
        }).catch(e => {
            this.setState({
                error: "Error to getting schedules",
                loading: false
            });
        })
    }
    selectedSchedule(item) {
        this.setState({
            scheduleIndividual: item,
            filters: item.extras.filters,
            recipientType: item.extras.recipientType
        })
    }
    updateFormData(key, value) {
        let data = update(this.state.data, {
            [key]: {
                $set: value
            }
        })
        this.setState({ data })
    }

    resetForm() {
        this.setState({
            data: {
                header: '',
                body: '',
                url: '',
                image: '',
                header: '',
                type: '',
                userIds: '',
                data: {},
                scheduledAt: ''

            }
        })
    }
    render() {
        return (
            <Main_Layout_Analytic title="Notification Analytics" mode={this.state.switchTheme}>
                {/* view modal */}
                <div className="modal fade" id="view" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header text-center border-bottom-0">
                                <div className="col  row d-flex align-items-center ">
                                    <p className="col modal-title  black-font font-size-21 font-semi" align="left">{this.state.scheduleIndividual.header}</p>
                                    <div className="col row d-flex justify-content-end m-0 ">
                                        <button className="draft-detail-btn blue-font font-semi mr-3">Duplicate</button>
                                        <button className="draft-detail-btn blue-font font-semi">Unsend</button>
                                    </div>
                                </div>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body d-flex flex-column  pt-3 pb-5 m-0 border-top">
                                <div className="d-flex row justify-content-end pr-3 mb-4">
                                    <div className="pt-1">
                                        <img src={`${process.env.URL_PREFIX}/static/img/ic_Delete.png`} alt="delete" height="13px" />
                                    </div>
                                    <a className="pl-3 pt-1 m-0 font-semi blue-font" href="draft.html" onclick="return confirmDelete()">Delete</a>
                                </div>
                                <div className="row m-0">
                                    <div className="col-8 m-0">
                                        <div className="col d-flex justify-content-center">
                                            <img src={`${process.env.URL_PREFIX}/static/img/Rfid.png`} alt="RFID" />
                                        </div>
                                        <div>
                                            <p className="m-0 title-xs grey-font">Title</p>
                                            <p className="black-font">{this.state.scheduleIndividual.header}</p>
                                        </div>
                                        <div>
                                            <p className="m-0 title-xs grey-font">Summary (optional)</p>
                                            <p className="black-font">Panggilan terakhir! Pasang RFID di pusat pemasangan</p>
                                        </div>
                                        <div>
                                            <p className="m-0 title-xs grey-font">Content</p>
                                            <p className="black-font">{this.state.scheduleIndividual.body}</p>
                                        </div>
                                    </div>
                                    <div className="col-4  m-0">
                                        <div>
                                            <p className="m-0 font-size-21 black-font font-semi">Send Options</p>
                                        </div>
                                        <div className="col row py-3">
                                            <img className="user mr-4 mt-1" src={`${process.env.URL_PREFIX}/static/img/ic_User.png`} alt="user" />
                                            {
                                                this.state.scheduleIndividual.userIds == null
                                                    ?
                                                    <span className="mb-2 font-semi grey-font">Sending to <span className="black-font"><span >all</span> users</span></span>
                                                    :
                                                    <span className="mb-2 font-semi grey-font">Sending to <span className="black-font"><span className="black-font h3">{this.state.scheduleIndividual.userIds.length}</span> users</span></span>
                                            }
                                        </div>
                                        <div>
                                            <p className="grey-font font-semi title-sm">Recipient</p>
                                            <p className="black-font font-semi">Filtered users</p>
                                        </div>
                                        <div className="pb-3">
                                            {
                                                this.state.recipientType == "filter" ?
                                                    this.state.filters.map((ev, index) => {
                                                        return (
                                                            <button className="col radio-panel draft-input d-flex align-items-center pl-3 mb-2 rounded">
                                                                <div className="mr-3">
                                                                    <p className="m-0 grey-font">{ev.criterias.label}:</p>
                                                                </div>
                                                                <label className="form-check-label theme-font-color font-semi" htmlFor="exampleRadios1">
                                                                    {ev.values[0]}
                                                                </label>
                                                            </button>
                                                        );
                                                    })
                                                    :
                                                    null
                                            }
                                        </div>
                                        <div>
                                            <p className="m-0 font-semi grey-font title-sm">Scheduled for</p>
                                        </div>
                                        <div className="row m-0">
                                            <div className="row m-0 pr-4 ">
                                                <div className="pr-3">
                                                    <img src={`${process.env.URL_PREFIX}/static/img/ic_Calendar.png`} alt="calendar" height="18px" />
                                                </div>
                                            </div>
                                            <div className="row m-0 p-0">
                                                <div className="pr-3">
                                                    <p className="m-0 font-semi black-font">{Moment(this.state.scheduleIndividual.scheduledAt).format('DD/MM/YYYY')}</p>
                                                </div>
                                                <div>
                                                    <div>
                                                        <p className="m-0 font-semi black-font">{Moment(this.state.scheduleIndividual.scheduledAt).format('h:mm A')}</p>
                                                    </div>
                                                    <div>
                                                        <p className="font-semi grey-font">(GMT 08:00)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <p className="m-0 font-semi grey-font title-sm">Notification Behavior</p>
                                        </div>
                                        <div className="row m-0 d-flex align-items-center">
                                            {
                                                !this.state.scheduleIndividual.silent ?
                                                    <>
                                                        <div className="pr-3">
                                                            <img src={`${process.env.URL_PREFIX}/static/img/ic_NotifSound.png`} alt="notification" height="18px" />
                                                        </div>
                                                        <div>
                                                            <p className="m-0 font-semi black-font">Regular Push</p>
                                                        </div>
                                                    </>
                                                    :
                                                    <>
                                                        <div className="pr-3">
                                                            <img src={`${process.env.URL_PREFIX}/static/img/ic_NotifSilent.png`} alt="notification" height="18px" />
                                                        </div>
                                                        <div>
                                                            <p className="m-0 font-semi black-font">Regular Push</p>
                                                        </div>
                                                    </>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-2 pt-4 pl-3 left-menu-notification theme-left-nav">
                    <div className="text-center" >
                        <div className="font-size-32  font-medium theme-font-color">Push Notification</div>
                        <div className="mt-4 text-left p-2 theme-border-color">
                            02/11/2021-03/12/2021
                        </div>
                        <div className="mt-4 text-left p-2 theme-border-color">
                            <div className="font-size-24 theme-font-color font-medium">17,164 </div>
                            <div className="font-size-16 theme-font-color font-medium">
                                Total push notification
                            </div>
                        </div>
                        <div className="mt-4 text-left p-2 theme-border-color">
                            <div className="font-size-16 theme-font-color font-medium">
                                <div className="d-flex">
                                    <div style={{ width: "20%" }}>45</div>
                                    <div style={{ width: "80%" }}>Birthday Greeting</div>
                                </div>
                                <div className="d-flex">
                                    <div style={{ width: "20%" }}>21</div>
                                    <div style={{ width: "80%" }}>Insurance Renewal</div>
                                </div>
                                <div className="d-flex">
                                    <div style={{ width: "20%" }}>435</div>
                                    <div style={{ width: "80%" }}>Appt Remind</div>
                                </div>
                                <div className="d-flex">
                                    <div style={{ width: "20%" }}>32</div>
                                    <div style={{ width: "80%" }}>Geo Fencing</div>
                                </div>
                                <div className="d-flex">
                                    <div style={{ width: "20%" }}>225</div>
                                    <div style={{ width: "80%" }}>Registration</div>
                                </div>
                                <div className="d-flex">
                                    <div style={{ width: "20%" }}>225</div>
                                    <div style={{ width: "80%" }}>WIP</div>
                                </div>
                                <div className="d-flex">
                                    <div style={{ width: "20%" }}>222</div>
                                    <div style={{ width: "80%" }}>Job Done</div>
                                </div>
                                <div className="d-flex">
                                    <div style={{ width: "20%" }}>221</div>
                                    <div style={{ width: "80%" }}>Ready for Collection</div>
                                </div>
                                <div className="d-flex">
                                    <div style={{ width: "20%" }}>543</div>
                                    <div style={{ width: "80%" }}>CMS Blasting</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-10 pt-4 fade-in" style={{ height: '100%' }}>
                    <div className="col d-flex pb-2" style={{ height: '7%' }}>
                        <p className="font-size-21 black-font font-medium">Scheduled</p>
                    </div>
                    <div className="col overflow-auto" style={{ height: '93%' }}>
                        <div className="d-flex">
                            <div>
                                <input type="text"
                                    className="form-control form-control-lg draft-input-height"
                                    style={{ width: "320px", fontSize: "16px", border: "1px solid #9599B6", marginTop: "0px", marginBottom: "0px" }}
                                    placeholder="Search by User name / Email"
                                    value={this.state.scheduledAtDate || ""}
                                    onChange={e => {
                                        this.setState({ scheduledAtDate: e.target.value })
                                    }}
                                />
                            </div>
                            <div className="ml-3">
                                <select
                                    class="form-control form-control-lg draft-input-height"
                                    style={{ width: "240px", fontSize: "16px", border: "1px solid #9599B6", marginTop: "0px", marginBottom: "0px" }}
                                    value={""}
                                    onChange={e => {
                                    }} >
                                    <option key="" value="">All Category</option>
                                </select>
                            </div>
                            <div>
                                <button class="w50 draft-proceed-btn draft-input-height" style={{ width: "140px", fontSize: "16px", border: "1px solid #9599B6", marginTop: "0px", marginBottom: "0px" }}>
                                    Search
                                </button>
                            </div>
                        </div>
                        <table className="table table-bordered table-hover table-striped mt-5">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>User Name</th>
                                    <th>User Mail</th>
                                    <th>User Device</th>
                                    <th>Type</th>
                                    <th>Message</th>
                                    <th style={{ width: '40px' }}>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr className="theme-font-color">
                                    <td className="text-left">
                                        10-Feb-2020 <br />
                                        13:21:24
                                    </td>
                                    <td>Jeremy</td>
                                    <td>
                                        jeremy@inglab.com.my
                                    </td>
                                    <td>
                                        Oppo F9
                                    </td>
                                    <td>
                                        Birthday
                                    </td>
                                    <td>
                                        Happy Birthday bla bla
                                    </td>
                                    <td>
                                        Sent
                                    </td>
                                </tr>
                                <tr className="theme-font-color">
                                    <td>10-Feb-2020 <br />
                                        13:21:24
                                    </td>
                                    <td>Jeremy</td>
                                    <td>
                                        jeremy@inglab.com.my
                                    </td>
                                    <td>
                                        Oppo F9
                                    </td>
                                    <td>
                                        Birthday
                                    </td>
                                    <td>
                                        Happy Birthday bla bla
                                    </td>
                                    <td>
                                        Sent
                                    </td>
                                </tr>
                                <tr className="theme-font-color">
                                    <td>10-Feb-2020 <br />
                                        13:21:24
                                    </td>
                                    <td>Jeremy</td>
                                    <td>
                                        jeremy@inglab.com.my
                                    </td>
                                    <td>
                                        Oppo F9
                                    </td>
                                    <td>
                                        Birthday
                                    </td>
                                    <td>
                                        Happy Birthday bla bla
                                    </td>
                                    <td>
                                        Sent
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="col row d-flex justify-content-between p-3 pb-5 m-0 table-footer" style={{ height: '40px' }}>
                        <div className="col-4 row d-flex justify-content-between align-items-center p-0 m-0">
                            <div>
                                <p className="title-xs m-0 theme-font-color">1-10 of 195 items</p>
                            </div>
                            <div className="d-flex row align-items-center m-0 p-0">
                                <p className="m-0 mr-3 title-sm">Show: <span className="theme-font-color numbering">10</span></p>
                                <img src={`${process.env.URL_PREFIX}/static/img/ic_SelectDropdown.png`} alt="select dropdown" />
                            </div>
                        </div>
                        <div className="col-4 d-flex row justify-content-end align-items-center m-0 p-0">
                            <div className="mr-4">
                                <img src={`${process.env.URL_PREFIX}/static/img/ic_Chevron Left.png`} alt="left" />
                            </div>
                            <div className="numbering theme-font-color">
                                <span>1</span>
                                <span>2</span>
                                <span>3</span>
                                <span>4</span>
                                <span>5</span>
                                <span>...</span>
                                <span>19</span>
                            </div>
                            <div className="ml-4">
                                <img src={`${process.env.URL_PREFIX}/static/img/ic_Chevron Right.png`} alt="right" />
                            </div>
                        </div>
                    </div>
                </div>

            </Main_Layout_Analytic>
        )
    }
}

export default withAuth(withRouter(Notification_Analytics))