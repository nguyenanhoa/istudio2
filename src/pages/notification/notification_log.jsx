import MyService from "../../services/my-service";
import { Snack } from '../../components/snack';
import Main_Layout from '../../components/main_layout';
import { withAuth } from '../../components/auth-component';
import { withRouter } from "next/router";
class Notification_Log extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            zeroState: true,
            switchTheme: this.props.router.query.mode == "dark" ? "dark_theme" : "white_theme",
            loading: true
        }
    }
    componentDidMount() {
        this.setState({
            loading: false
        })
    }
    render() {
        return (
            <Main_Layout title="Notification Log" mode={this.state.switchTheme}>
                {
                    this.state.loading ?
                        <div class="spinner-border text-primary center_spinner" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        :
                        <div className="col-10 pt-4 fade-in" style={{ height: '76%' }}>
                            {/* title */}
                            <div className="col row  d-flex align-items-center justify-content-between pb-2 m-0" style={{ height: '10%' }}>
                                <div className="col-6  row d-flex align-items-center m-0 p-0">
                                    <div className="mr-5">
                                        <p className="font-size-21 theme-font-color font-medium">Notification Log</p>
                                    </div>
                                </div>
                                <div className="col-3  row d-flex align-items-center justify-content-end m-0 p-0 pr-2">
                                    <div className="mr-3">
                                        <img src={`${process.env.URL_PREFIX}/static/img/ic_Download.png`} alt="download" height="18px" />
                                    </div>
                                    <div>
                                        <p className="m-0 p-0 blue-font font-medium">Download Data</p>
                                    </div>
                                </div>
                            </div>
                            {/* search bar */}
                            <div className="col row d-flex justify-content-between m-0" style={{ height: '10%', maxHeight: '50px' }}>
                                <table width="100%" style={{ marginTop: '0px' }}><tbody><tr>
                                    <td>
                                        <input className="form-control form-control-lg search-bar" type="text" placeholder="Search for notification by title or description" />
                                    </td>
                                    <td>
                                        <button className="draft-proceed-btn btn-primary mr-3">Search</button>
                                    </td>
                                    <td align="right" width="250px" style={{ paddingRight: '7px' }}>
                                        <div className="dropdown">
                                            <button className="sort-bar sort-bar-filter text-left pr-0 row align-items-center" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span>Filter Results</span>
                                                <div className="col d-flex justify-content-end pt-1 pl-2">
                                                    <img src={`${process.env.URL_PREFIX}/static/img/ic_SelectDropdown.png`} alt="dropdown" />
                                                </div>
                                            </button>
                                            <div className="col dropdown-menu dropdown-menu-right shadow">
                                                <form className="py-3 px-3">
                                                    <div className="col m-0 p-0">
                                                        <div className="col row m-0">
                                                            <div className="col-9 row p-0">
                                                                <div className="mr-3">
                                                                    <img src={`${process.env.URL_PREFIX}/static/img/ic_Filter.png`} alt="filter" />
                                                                </div>
                                                                <div>
                                                                    <p className="m-0 black-three-font">Filter</p>
                                                                </div>
                                                            </div>
                                                            <div className="col-3">
                                                                <p className="m-0 blue-font">Reset</p>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div className="form-group">
                                                                <select className="form-control draft-input-height mt-3" id="exampleFormControlSelect1">
                                                                    <option>Search Notification</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <p className="title-sm grey-font font-medium">Status:</p>
                                                        </div>
                                                        <div>
                                                            <div className="col row m-0 p-0 pb-3">
                                                                <div className="col form-check">
                                                                    <input className="form-check-input checkbox" type="checkbox" defaultValue id="Opened" />
                                                                    <label className="form-check-label ml-3 grey-two-font font-semi" htmlFor="Opened">
                                                                        Opened
                                                            </label>
                                                                </div>
                                                                <div className="col form-check">
                                                                    <input className="form-check-input checkbox" type="checkbox" defaultValue id="Delivered" />
                                                                    <label className="form-check-label ml-3 grey-two-font font-semi" htmlFor="Delivered">
                                                                        Delivered
                                                            </label>
                                                                </div>
                                                            </div>
                                                            <div className="col m-0 p-0 pb-3">
                                                                <div className="col form-check">
                                                                    <input className="form-check-input checkbox" type="checkbox" defaultValue id="Undelivered" />
                                                                    <label className="form-check-label ml-3 grey-two-font font-semi" htmlFor="Undelivered">
                                                                        Undelivered
                                                            </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <p className="title-sm grey-font font-medium">System:</p>
                                                        </div>
                                                        <div>
                                                            <div className="col row m-0 p-0 pb-3">
                                                                <div className="col form-check">
                                                                    <input className="form-check-input checkbox" type="checkbox" defaultValue id="Android" />
                                                                    <label className="form-check-label ml-3 grey-two-font font-semi" htmlFor="Android">
                                                                        Android
                                                            </label>
                                                                </div>
                                                                <div className="col form-check">
                                                                    <input className="form-check-input checkbox" type="checkbox" defaultValue id="iOS" />
                                                                    <label className="form-check-label ml-3 grey-two-font font-semi" htmlFor="iOS">
                                                                        iOS
                                                            </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div className="form-group">
                                                                <select className="form-control draft-input-height mt-3 filter-item-last-dropdown" id="exampleFormControlSelect1">
                                                                    <option>Today</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr></tbody></table>
                            </div>
                            <div style={{ height: '15px' }} />
                            {/* scrollable item */}
                            <div className="col">
                                <table className="table table-borderless">
                                    <tbody><tr className="col table-header">
                                        <th width="20%">USER</th>
                                        <th width="30%">NOTIFICATION</th>
                                        <th width="20%">
                                            <div className="row m-0 d-flex align-items-center">
                                                <span className="mr-3">DATE</span>
                                                <img src={`${process.env.URL_PREFIX}/static/img/ic_SelectDropdown.png`} alt="dropdown" />
                                            </div>
                                        </th>
                                        <th width="15%">
                                            <div className="row m-0 d-flex align-items-center">
                                                <span className="mr-3">STATUS</span>
                                                <img src={`${process.env.URL_PREFIX}/static/img/ic_SelectDropdown.png`} alt="dropdown" />
                                            </div>
                                        </th>
                                        <th>
                                            <div className="row m-0 d-flex align-items-center">
                                                <span className="mr-3">SYSTEM</span>
                                                <img src={`${process.env.URL_PREFIX}/static/img/ic_SelectDropdown.png`} alt="dropdown" />
                                            </div>
                                        </th>
                                    </tr>
                                    </tbody></table>
                            </div>
                            <div className="col overflow-auto" style={{ height: '85%' }}>
                                <table className="table table-borderless">
                                    <tbody className="table-body">
                                        <tr className="col theme-font-color">
                                            <td width="20%">Jeremy Liew</td>
                                            <td width="30%">Pasang RFID, dapatkan RM5 Cashback</td>
                                            <td width="20%">
                                                <span className="mr-3">26/03/2020</span>
                                                <span>2:37 PM</span>
                                            </td>
                                            <td width="15%">Delivered</td>
                                            <td className="row m-0 d-flex align-items-center">
                                                <img className="mr-3" src={`${process.env.URL_PREFIX}/static/img/ic_Android.png`} alt="" />
                                                <span>10.0</span>
                                            </td>
                                        </tr>
                                        <tr className="col theme-font-color">
                                            <td>Nur Diana Nat…</td>
                                            <td>Pasang RFID, dapatkan RM5 Cashback</td>
                                            <td>
                                                <span className="mr-3">26/03/2020</span>
                                                <span>2:37 PM</span>
                                            </td>
                                            <td>Delivered</td>
                                            <td className="row m-0 d-flex align-items-center">
                                                <img className="mr-3" src={`${process.env.URL_PREFIX}/static/img/ic_Android.png`} alt="" />
                                                <span>10.0</span>
                                            </td>
                                        </tr>
                                        <tr className="col theme-font-color">
                                            <td>Yam Thye On</td>
                                            <td>Pasang RFID, dapatkan RM5 Cashback</td>
                                            <td>
                                                <span className="mr-3">26/03/2020</span>
                                                <span>2:37 PM</span>
                                            </td>
                                            <td>Delivered</td>
                                            <td className="row m-0 d-flex align-items-center">
                                                <img className="mr-3" src={`${process.env.URL_PREFIX}/static/img/ic_Apple.png`} alt="" />
                                                <span>13.1.0</span>
                                            </td>
                                        </tr>
                                        <tr className="col theme-font-color">
                                            <td>Darren Lim</td>
                                            <td>Pasang RFID, dapatkan RM5 Cashback</td>
                                            <td>
                                                <span className="mr-3">26/03/2020</span>
                                                <span>2:37 PM</span>
                                            </td>
                                            <td className="red-font">Undelivered</td>
                                            <td className="row m-0 d-flex align-items-center">
                                                <img className="mr-3" src={`${process.env.URL_PREFIX}/static/img/ic_Apple.png`} alt="" />
                                                <span>13.0.1</span>
                                            </td>
                                        </tr>
                                        <tr className="col theme-font-color">
                                            <td>Guster Chua</td>
                                            <td>Pasang RFID, dapatkan RM5 Cashback</td>
                                            <td>
                                                <span className="mr-3">26/03/2020</span>
                                                <span>2:37 PM</span>
                                            </td>
                                            <td className="green-font">Opened</td>
                                            <td className="row m-0 d-flex align-items-center">
                                                <img className="mr-3" src={`${process.env.URL_PREFIX}/static/img/ic_Android.png`} alt="" />
                                                <span>10.0</span>
                                            </td>
                                        </tr>
                                        <tr className="col theme-font-color">
                                            <td>Nyiam Gan Yew</td>
                                            <td>Pasang RFID, dapatkan RM5 Cashback</td>
                                            <td>
                                                <span className="mr-3">26/03/2020</span>
                                                <span>2:37 PM</span>
                                            </td>
                                            <td className="green-font">Opened</td>
                                            <td className="row m-0 d-flex align-items-center">
                                                <img className="mr-3" ssrc={`${process.env.URL_PREFIX}/static/img/ic_Apple.png`} alt="" />
                                                <span>12.1.2</span>
                                            </td>
                                        </tr>
                                        <tr className="col theme-font-color">
                                            <td>Tan Li Peng</td>
                                            <td>Pasang RFID, dapatkan RM5 Cashback</td>
                                            <td>
                                                <span className="mr-3">26/03/2020</span>
                                                <span>2:37 PM</span>
                                            </td>
                                            <td>Delivered</td>
                                            <td className="row m-0 d-flex align-items-center">
                                                <img className="mr-3" src={`${process.env.URL_PREFIX}/static/img/ic_Android.png`} alt="" />
                                                <span>10.0</span>
                                            </td>
                                        </tr>
                                        <tr className="col theme-font-color">
                                            <td>Neoh Wei</td>
                                            <td>Pasang RFID, dapatkan RM5 Cashback</td>
                                            <td>
                                                <span className="mr-3">26/03/2020</span>
                                                <span>2:37 PM</span>
                                            </td>
                                            <td className="red-font">Undelivered</td>
                                            <td className="row m-0 d-flex align-items-center">
                                                <img className="mr-3" src={`${process.env.URL_PREFIX}/static/img/ic_Android.png`} alt="" />
                                                <span>10.0</span>
                                            </td>
                                        </tr>
                                        <tr className="col theme-font-color">
                                            <td>John Doe</td>
                                            <td>Pasang RFID, dapatkan RM5 Cashback</td>
                                            <td>
                                                <span className="mr-3">26/03/2020</span>
                                                <span>2:37 PM</span>
                                            </td>
                                            <td className="green-font">Opened</td>
                                            <td className="row m-0 d-flex align-items-center">
                                                <img className="mr-3" src={`${process.env.URL_PREFIX}/static/img/ic_Android.png`} alt="" />
                                                <span>10.0</span>
                                            </td>
                                        </tr>
                                        <tr className="col theme-font-color">
                                            <td>John Doe</td>
                                            <td>Pasang RFID, dapatkan RM5 Cashback</td>
                                            <td>
                                                <span className="mr-3">26/03/2020</span>
                                                <span>2:37 PM</span>
                                            </td>
                                            <td className="red-font">Undelivered</td>
                                            <td className="row m-0 d-flex align-items-center">
                                                <img className="mr-3" src={`${process.env.URL_PREFIX}/static/img/ic_Android.png`} alt="" />
                                                <span>10.0</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="col row d-flex justify-content-between p-3 pb-5 m-0 table-footer" style={{ height: '40px' }}>
                                <div className="col-4  row d-flex justify-content-between align-items-center p-0 m-0">
                                    <div>
                                        <p className="title-xs m-0 theme-font-color">1-10 of 195 items</p>
                                    </div>
                                    <div className="d-flex row align-items-center m-0 p-0">
                                        <p className="m-0 mr-3 title-sm">Show: <span className="theme-font-color numbering">10</span></p>
                                        <img src={`${process.env.URL_PREFIX}/static/img/ic_SelectDropdown.png`} alt="select dropdown" />
                                    </div>
                                </div>
                                <div className="col-4  d-flex row justify-content-end align-items-center m-0 p-0">
                                    <div className="mr-4">
                                        <img src={`${process.env.URL_PREFIX}/static/img/ic_Chevron Left.png`} alt="left" />
                                    </div>
                                    <div className="theme-font-color numbering">
                                        <span>1</span>
                                        <span>2</span>
                                        <span>3</span>
                                        <span>4</span>
                                        <span>5</span>
                                        <span>...</span>
                                        <span>19</span>
                                    </div>
                                    <div className="ml-4">
                                        <img src={`${process.env.URL_PREFIX}/static/img/ic_Chevron Right.png`} alt="right" />
                                    </div>
                                </div>
                            </div>
                        </div>

                }
            </Main_Layout>
        )
    }
}

export default withAuth(withRouter(Notification_Log))