import MyService from "../../services/my-service";
import IStudioService from "../../services/istudio-service";
import update from 'immutability-helper';
import Select from 'react-select';
import * as _ from 'lodash';
import { ACCESS_PUSH_DRAFT, AccessService, ACCESS_PUSH_DRAFT_SEND } from '../../services/access-service';
import { Snack } from '../../components/snack';
import Main_Layout from '../../components/main_layout';
import PushMainContent from '../notification/push-main-content'
import SideListing from '../../components/side-listing';
import { withAuth } from '../../components/auth-component';
import { withRouter } from "next/router";
import Moment from 'moment';

class Draft extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            sendType: true,
            filterCount: 0,
            scheduledAtDate: "",
            scheduledAtHours: "",
            scheduledAtMinutes: "",
            scheduledAtSun: "",
            zeroState: false,
            showPreview: false,
            currentDate: (new Date().toISOString().split('.')[0] + "Z".replace('Z', '')).substring(0, 16),
            optionReason: 0,
            totalClick: 0,
            data: {
                id: 0,
                body: '',
                url: '',
                image: '',
                header: '',
                type: 'Announcement',
                scheduledAt: null,
                silent: false,
                actionType: 'details',
                extras: {}
            },
            error: null,
            success: null,
            shceduleDisable: true,
            recipients: [], // recipients from 'all'
            selectedRecipients: [], // recipients from 'select'
            rawSelectedRecipients: [], // recipients from 'select' without formatting
            filteredRecipients: [], // recipients from 'filter'
            rawFilteredRecipients: [], // recipients from 'filter' without formatting
            filters: [], // selected filter criterias
            criterias: [], // criterias field in api data
            selectedCriteria: "",
            criteriasValue: [],
            selectedcriteriasValue: "",
            filterData: {}, // original data from api
            filterAddable: true,
            recipientType: 'all',
            recipientCount: 0,
            selectedIndex: props.router.query.selected,
            deleting: false,
            modal: {
                showPreview: false,
                deleteDraft: false,
            },
            deleteDraftId: null,
            savingDraft: null,
            draftData: [],
            switchTheme: this.props.router.query.mode == "dark" ? "dark_theme" : "white_theme",
            loading: true
        }
    }

    componentDidMount() {
        IStudioService.getRecipients().then(res => {
            console.log("Reciept", res.data)
            this.setState({
                recipientCount: res.data.length,
                zeroState: res.data.length ? true : false,
                recipients: res.data.map(item => {
                    return {
                        label: `${item.displayName}`,
                        value: item.userId,
                        data: item
                    }
                })
            })
        })
        //getFilteringCriteria
        IStudioService.getFilteringCriteria1().then(res => {
            console.log("FilteringCriteria", res.data)
            let criterias = res.data.map(item => {
                return {
                    label: item.name,
                    value: item.value
                }
            })
            console.log("Criteria", criterias)
            this.setState({
                filterData: res.data,
                criterias: res.data.map(item => {
                    return {
                        label: item.name,
                        value: item.value
                    }
                }),
            })
        })
        this.getDraftMessages()
    }
    closeDialog(id) {
        let proceedClose = document.getElementById(`${id}`);
        proceedClose.click()
    }
    filterCriterialOption(id) {
        if (this.state.recipientType == 'filter') {
            alert(" Call new Filter api(pending backend) before close dialog")
            //loop filter array and add filter to api
            let filter = this.state.filters;
        }
        else {
            let proceedClose = document.getElementById(`${id}`);
            proceedClose.click()
        }


    }
    savescheduledAt() {
        let scheduledAtDate = this.state.scheduledAtDate;
        let scheduledAtHours = this.state.scheduledAtHours;
        let scheduledAtMinutes = this.state.scheduledAtMinutes;
        let scheduledAt = "";
        scheduledAt = `${scheduledAtDate}T${scheduledAtHours}:${scheduledAtMinutes}:00`;
        this.updateFormData('scheduledAt', scheduledAt);
        //Close dialog
        this.closeDialog("scheduledAtTime")
    }

    dateConvert(date) {
        if (date != null) {
            var str = date.substring(8, 10) + "-" + date.substring(5, 7) + "-" + date.substring(0, 4) + " " + date.substring(11, 16);
        }
        else {
            str = "N/A"
        }
        return str
    }

    createNewDraft() {
        let draftData = _.cloneDeep(this.state.draftData)
        draftData.unshift({})
        draftData.forEach((e, index) => {
            e.selected = (index == 0)
        })
        this.setState({
            data: {
                id: 0,
                body: '',
                url: '',
                image: '',
                header: '',
                type: 'Announcement',
                scheduledAt: null,
                actionType: 'details'
            },
            draftData
        })
    }

    getDraftMessages() {
        let query = this.props.router.query
        IStudioService.getDraftMessages().then(res => {
            let data = res.data.data
            if (query.selected != null && data.length > 0) {
                data[query.selected].selected = true
            }
            console.log("draftData", data)

            this.setState({
                draftData: data,
                deleting: false,
                modal: {
                    deleteDraft: false,
                },
                loading: false
            })
            if (query.selected != null && data.length > 0) {
                this.onNotificationSelected(data[query.selected], query.selected)
            }


        }).catch(e => {
            this.setState({
                deleting: false,
                modal: {
                    deleteDraft: false,
                },
                loading: false

            })
        })
    }
    getDraftMessagesAfterDelete() {
        let query = this.props.router.query
        IStudioService.getDraftMessages().then(res => {
            let data = res.data.data
            if (query.selected != null && data.length > 0) {
                data[query.selected].selected = true
            }
            console.log("draftData", data)

            this.setState({
                draftData: data,
                deleting: false,
                modal: {
                    deleteDraft: false,
                }
            })
            if (query.selected != null && data.length > 0) {
                this.onNotificationSelected(data[query.selected], query.selected)
            }
            else {
                window.location.href = `${process.env.URL_PREFIX}/draft/?selected=0`
            }

        }).catch(e => {
            this.setState({
                deleting: false,
                modal: {
                    deleteDraft: false,
                }
            })
        })
    }

    updateFormData(key, value) {
        let data = update(this.state.data, {
            [key]: {
                $set: value
            }
        })
        this.setState({ data })

        this.saveDraftMessage()
    }

    saveDraftMessage() {
        if (this.draftSubmitFunc != null) {
            clearTimeout(this.draftSubmitFunc)
        }

        this.setState({ savingDraft: true })
        this.draftSubmitFunc = setTimeout(() => {
            clearTimeout(this.draftSubmitFunc)

            let data = this.getSubmissionData()
            IStudioService.saveDraftMessage(data).then(res => {

                let data = _.cloneDeep(this.state.data)
                data.id = res.data.data.id
                this.setState({
                    savingDraft: false,
                    data
                })
                this.getDraftMessages()

            }).catch(e => {
                this.setState({ savingDraft: false })
            })
        }, 2000)
    }

    confirmDeleteMessage(id) {
        this.setState({
            deleteDraftId: id,
        })
        var s = confirm("Do you want to delete the selected item?");
        if (s) {
            this.deleteDraftMessage(id)
        }

    }

    deleteDraftMessage(id) {
        if (id == null || id == 0) {
            this.setState({
                deleting: false,
                modal: {
                    deleteDraft: false,
                }
            })
            return
        }

        this.setState({ deleting: true })

        IStudioService.deleteDraftMessages(id).then(res => {
            this.getDraftMessagesAfterDelete()
        }).catch(e => {
            this.setState({
                deleting: false,
                modal: {
                    deleteDraft: false,
                }
            })
        })
    }

    resetForm() {
        this.setState({
            data: {
                body: '',
                url: '',
                image: '',
                header: '',
                type: '',
                userIds: '',
                data: {},
                scheduledAt: ''
            }
        })
    }

    preview() {
        /*  this.setState({            
             modal: { showPreview: true }
         }) */
        this.state.showPreview = true
        this.setState({
            showPreview: true
        })
    }

    getSubmissionData() {

        let data = _.cloneDeep(this.state.data)

        if (this.state.data.actionType != 'details') {
            data.body = ''
        }
        if (this.state.data.actionType != 'url') {
            data.url = ''
        }
        //data.pushDeviceIds=[];
        switch (this.state.recipientType) {
            case 'all': {
                data.userIds = []
                break
            }
            case 'select': {
                data.userIds = this.state.selectedRecipients
                break
            }
            case 'filter': {
                data.userIds = this.state.filteredRecipients
                break
            }
        }

        // add extras
        data.extras = {
            filters: this.state.filters,
            criterias: this.state.recipientType === "select" ? [] : this.state.criterias,
            recipientType: this.state.recipientType,
            rawFilteredRecipients: this.state.rawFilteredRecipients,
            rawSelectedRecipients: this.state.rawSelectedRecipients
        }
        console.log("SUBMISSION DATA:", data)
        return data
    }

    send() {
        this.setState({ error: null, success: null });
        let error = '';
        let data = this.getSubmissionData()

        switch (this.state.data.actionType) {
            case 'details': {
                if (data.body.trim() == '') {
                    error = "Please enter notification's Title and Content"
                }
                break
            }
            case 'url': {
                if (data.url && data.url.trim() == '') {
                    error = "Url is required"
                }
                break
            }
        }

        if (!data.header || data.header.trim() == '') {
            error = 'Title is required'
        }

        if (error) {
            this.setState({ error: error });
            return;
        }
        console.log("Data Submit in Send", data)
        let res = IStudioService.submitMessageForApproval(data.id)
        res.then(res => {
            let draftData = _.cloneDeep(this.state.draftData)
            let index = draftData.findIndex(e => e.id == data.id)
            if (index >= 0) {
                draftData.splice(index, 1)
            }
            this.setState({
                success: "Submission successful",
                modal: { sendConfirm: false },
                draftData
            })
        }).catch(e => {
            this.setState({
                error: "Error to sending notification",
                modal: { sendConfirm: false }
            })
        })
    }

    addFilter() {
        let filters = _.cloneDeep(this.state.filters);
        let tmpfilters = filters;
        let criterias = this.state.criterias;
        if (filters.length == 0) {
            filters.push({
                criterias: _.cloneDeep(this.state.criterias[0]),
                values: []
            })
            this.setState({
                filters
            }, () => {
                this.saveDraftMessage()
            })
        }
        else {
            for (let i = 0; i < criterias.length; i++) {
                for (let j = 0; j < filters.length; j++) {
                    if (filters[j].criterias.label != criterias[i].label) {
                        if (filters[j].criterias.label == "Gender") {
                            tmpfilters.push({
                                criterias: _.cloneDeep(criterias[i]),
                                values: ["Male"]
                            })
                        } else if (filters[j].criterias.label == "Member") {
                            tmpfilters.push({
                                criterias: _.cloneDeep(criterias[i]),
                                values: ["Honda Owner"]
                            })
                        }
                        break
                    }
                }
            }
            var obj = {};
            for (var i = 0, len = tmpfilters.length; i < len; i++)
                obj[tmpfilters[i]['criterias'].label] = tmpfilters[i];
            let duplicateArray = new Array();
            for (var key in obj)
                duplicateArray.push(obj[key]);
            // Add default value for select            
            for (let i = 0; i < tmpfilters.length; i++) {
                if (tmpfilters[i].criterias.label == "Gender") {
                    tmpfilters[i].values[0] = "Male"

                } else if (tmpfilters[i].criterias.label == "Member") {
                    tmpfilters[i].values[0] = "Honda Owner"
                }
            }
            //
            this.setState({
                filters: duplicateArray
            }, () => {
                this.saveDraftMessage()
            })
        }
    }

    onNotificationSelected(item, index) {
        let data = _.cloneDeep(this.state.draftData)
        data.forEach((obj, i) => obj.selected = (i == index))
        let selectedData = this.state.draftData[index]
        let recipientCount;
        switch (selectedData.extras.recipientType) {
            case 'filter':
                recipientCount = selectedData.extras.rawFilteredRecipients.length
                break;
            case 'select':
                recipientCount = selectedData.extras.rawSelectedRecipients.length
                break;
        }
        this.setState({
            data: selectedData,
            tmpScheduledAt: data[index].scheduledAt,
            scheduledAtDate: data[index].scheduledAt != null ? data[index].scheduledAt.split("T")[0] : "",
            scheduledAtHours: data[index].scheduledAt != null ? data[index].scheduledAt.split("T")[1].split(":")[0] : "",
            scheduledAtMinutes: data[index].scheduledAt != null ? data[index].scheduledAt.split("T")[1].split(":")[1] : "",
            titleHeader: item.header,
            draftData: data,
            filters: selectedData.extras.filters,
            recipientType: selectedData.extras.recipientType,
            rawSelectedRecipients: selectedData.extras.rawSelectedRecipients,
            recipientCount,
        })
    }

    render() {
        return (
            <Main_Layout title="Drafts" mode={this.state.switchTheme}>
                {this.state.error ?
                    <Snack
                        color='#d74338'
                        text={this.state.error}
                        vertical='bottom'
                        horizontal='left'
                        open={this.state.error != null}
                        onClose={() => {
                            this.setState({ error: null })
                        }}
                        onActionButtonClick={() => {
                            this.setState({ error: null })
                        }}
                    />
                    : null
                }
                {this.state.success ?
                    <Snack
                        color='green'
                        text={this.state.success}
                        vertical='bottom'
                        horizontal='left'
                        open={this.state.success != null}
                        onClose={() => {
                            this.setState({ success: null })
                        }}
                        onActionButtonClick={() => {
                            this.setState({ success: null })
                        }}
                    />
                    : null
                }
                {/* Dialog Delete Confirmation*/}
                {/* <!-- recipient modal --> */}
                <div class="modal fade" id="recipient" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header text-center border-bottom-0">
                                <div class="col  d-flex flex-column justify-content-center align-items-center">
                                    <p class="modal-title black-font font-size-21 font-medium w-75" align="left">Recipients</p>
                                    <p class="modal-title grey-font w-75" align="left">Sending your message to the right audience will be more effective!</p>
                                </div>

                                <button id="recipientDialog" type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body d-flex row justify-content-center  ml-3 mr-5">
                                <div class="w-75  mb-3">
                                    <table width="100%">
                                        <tr>
                                            <td width="35%" valign="top">
                                                <div class="pb-2">
                                                    <button class="col radio-panel draft-input-height d-flex align-items-center pl-3 rounded theme-font-color">
                                                        <div class="mr-3 mt-1">
                                                            <input type="radio" name="recipient" id="recipient1"
                                                                checked={this.state.recipientType == 'all'}
                                                                // defaultChecked={true}
                                                                onChange={() => {
                                                                    // get number of users
                                                                    this.setState({
                                                                        recipientType: 'all',
                                                                        recipientCount: this.state.recipients.length
                                                                    }, () => {
                                                                        this.saveDraftMessage()
                                                                    })
                                                                }}
                                                            />
                                                        </div>
                                                        <label class={this.state.recipientType == 'all' ? "form-check-label font-medium" : "form-check-label grey-font font-medium"} for="recipient1">
                                                            Send to all users
                                                        </label>
                                                    </button>
                                                </div>
                                                <div class="pb-2">
                                                    <button class="col radio-panel draft-input-height d-flex align-items-center pl-3 rounded theme-font-color">
                                                        <div class="mr-3 mt-1">
                                                            <input type="radio" name="recipient" id="recipient2"
                                                                checked={this.state.recipientType == 'select'}
                                                                onChange={e => {
                                                                    if (e.target.checked) {
                                                                        this.setState({
                                                                            recipientCount: this.state.selectedRecipients.length,
                                                                            recipientType: 'select'
                                                                        }, () => {
                                                                            this.saveDraftMessage()
                                                                        })
                                                                    }
                                                                }}
                                                            />
                                                        </div>
                                                        <label class={this.state.recipientType == 'select' ? "form-check-label  font-medium" : "form-check-label grey-font font-medium"} for="recipient2">
                                                            Selected users
                                                        </label>
                                                    </button>
                                                </div>
                                                <div class="pb-2">
                                                    <button class="col radio-panel draft-input-height d-flex align-items-center pl-3 rounded theme-font-color">
                                                        <div class="mr-3 mt-1">
                                                            <input type="radio" name="recipient" id="recipient3"
                                                                checked={this.state.recipientType == 'filter'}
                                                                onChange={e => {
                                                                    if (e.target.checked) {
                                                                        this.setState({
                                                                            recipientType: 'filter',
                                                                            recipientCount: this.state.filteredRecipients.length
                                                                        }, () => {
                                                                            this.saveDraftMessage()
                                                                        })
                                                                    }
                                                                }}
                                                            />
                                                        </div>
                                                        <label class={this.state.recipientType == 'filter' ? "form-check-label font-medium" : "form-check-label grey-font font-medium"} for="recipient3">
                                                            Filter users
                                                        </label>
                                                    </button>
                                                </div>
                                            </td>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            <td valign="top">
                                                {
                                                    this.state.recipientType == 'filter' ?
                                                        <>

                                                            <div class="w-80  p-0 m-0 row d-flex justify-content-between" style={{ marginTop: "5px" }} >
                                                                <div class="col-xl-12 px-0 pb-3 m-0 font-size-18">Filter users</div>
                                                                {
                                                                    this.state.filters.map((filter, index) => {
                                                                        return (
                                                                            <>
                                                                                <div class="col-xl-1 px-0 pb-3 m-0"><div class=" font-size-10 pr-1" style={{ paddingTop: "15px", textAlign: "right" }}>{index > 0 ? "AND" : "WHERE"}</div></div>
                                                                                <div class="col-xl-4 px-0 pb-3 m-0">
                                                                                    {/* <Select
                                                                                        class="form-control form-control-lg draft-input-height"
                                                                                        style={{ fontSize: "16px", border: "1px solid #9599B6", marginTop: "0px", marginBottom: "0px" }}
                                                                                        isDisabled={this.state.recipientType != 'filter'}
                                                                                        options={this.state.criterias}
                                                                                        value={this.state.selectedCriteria}
                                                                                        onChange={item => {
                                                                                            this.state.selectedcriteriasValue = "";
                                                                                            let criteriasValue = [];
                                                                                            criteriasValue = item.value.split(",")
                                                                                            this.setState(
                                                                                                {
                                                                                                    selectedCriteria: item,
                                                                                                    criteriasValue: criteriasValue.map(item => {
                                                                                                        return {
                                                                                                            label: item,
                                                                                                            value: item
                                                                                                        }
                                                                                                    }),
                                                                                                }
                                                                                            )
                                                                                        }} /> */}
                                                                                    <select
                                                                                        class="form-control form-control-lg draft-input-height"
                                                                                        style={{ fontSize: "16px", border: "1px solid #9599B6", marginTop: "0px", marginBottom: "0px" }}
                                                                                        disabled={this.state.recipientType != 'filter'}
                                                                                        value={''}
                                                                                        onChange={e => {

                                                                                        }} >
                                                                                        <option key={filter.criterias.label} value={filter.criterias.label}>{filter.criterias.label}</option>
                                                                                    </select>

                                                                                </div>
                                                                                <div class="col-xl-6 px-0 pb-3 m-0 pl-3">
                                                                                    <select
                                                                                        class="form-control form-control-lg draft-input-height"
                                                                                        style={{ fontSize: "16px", border: "1px solid #9599B6", marginTop: "0px", marginBottom: "0px" }}
                                                                                        disabled={this.state.recipientType != 'filter'}
                                                                                        value={filter.values[0]}
                                                                                        onChange={e => {
                                                                                            filter.values = [];
                                                                                            filter.values.push(e.target.value)
                                                                                            this.saveDraftMessage()
                                                                                        }} >
                                                                                        {
                                                                                            filter.criterias.value.split(",").map((ev, index) => {
                                                                                                return (
                                                                                                    <option key={ev} value={ev}>{ev}</option>
                                                                                                );
                                                                                            })
                                                                                        }

                                                                                    </select>
                                                                                    {/* <Select
                                                                                        class="form-control form-control-lg draft-input-height"
                                                                                        style={{ fontSize: "16px", border: "1px solid #9599B6", marginTop: "0px", marginBottom: "0px" }}
                                                                                        isDisabled={this.state.recipientType != 'filter'}
                                                                                        options={this.state.criteriasValue || []}
                                                                                        value={this.state.selectedcriteriasValue || {}}
                                                                                        onChange={item => {
                                                                                            this.setState({
                                                                                                selectedcriteriasValue: item
                                                                                            })
                                                                                            let selectedCriteria = this.state.selectedCriteria;
                                                                                            IStudioService.getCriteriaResult(selectedCriteria.label, item.value).then(res => {
                                                                                                console.log("After Search", res.data)
                                                                                                let filteredRecipients = res.data.map(item => item)
                                                                                                console.log("filteredRecipients", filteredRecipients)
                                                                                                this.setState({
                                                                                                    filteredRecipients: res.data.map(item => item),
                                                                                                    recipientCount: res.data.length
                                                                                                }, () => {
                                                                                                    this.saveDraftMessage()
                                                                                                })
                                                                                            })
                                                                                        }} /> */}
                                                                                </div>
                                                                                <div class="col-xl-1 px-0 pb-3 m-0" onClick={() => {
                                                                                    let filters = this.state.filters;
                                                                                    filters.splice(index, 1)
                                                                                    this.setState({
                                                                                        filters
                                                                                    }, () => {
                                                                                        this.saveDraftMessage()
                                                                                    })
                                                                                }}><div class=" font-size-14 pl-3" style={{ paddingTop: "10px" }}><i class="fa fa-trash-o"></i></div></div>
                                                                            </>
                                                                        );
                                                                    })
                                                                }
                                                            </div>
                                                            <div class="w-80  mb-0 row d-flex justify-content-end" style={{ margin: "0px" }}
                                                                onClick={() => {
                                                                    if (this.state.recipientType != 'filter') return
                                                                    this.addFilter()
                                                                }}
                                                            >
                                                                <a class="m-0 grey-font font-medium">+ Add more filter</a>
                                                            </div>
                                                        </>
                                                        :
                                                        null
                                                }
                                                {
                                                    this.state.recipientType == 'select' ?
                                                        <>
                                                            <div class="w-80  p-0 m-0 row d-flex justify-content-between" style={{ marginTop: "5px" }} >
                                                                <div class="col-xl-12 px-0 pb-3 m-0 font-size-18">Selected users</div>
                                                                <div class="col-xl-8 px-0 pb-3 m-0">
                                                                    <Select
                                                                        class="form-control form-control-lg draft-input-height" style={{ fontSize: "16px", border: "1px solid #9599B6", marginTop: "0px", marginBottom: "0px" }}
                                                                        options={this.state.recipients}
                                                                        isMulti={true}
                                                                        value={this.state.rawSelectedRecipients}
                                                                        isDisabled={this.state.recipientType != 'select'}
                                                                        onChange={item => {
                                                                            if (!item) item = []
                                                                            let selectedRecipients = item.map(obj => obj.value)
                                                                            let rawSelectedRecipients = item
                                                                            delete rawSelectedRecipients.data
                                                                            this.setState({
                                                                                selectedRecipients,
                                                                                rawSelectedRecipients,
                                                                                recipientCount: selectedRecipients.length
                                                                            }, () => {
                                                                                this.saveDraftMessage()
                                                                            })
                                                                        }}
                                                                    />
                                                                </div>
                                                            </div>
                                                        </>
                                                        :
                                                        null
                                                }
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="w-75  mb-5 pr-3 row d-flex justify-content-center">
                                    <button class="w50 draft-proceed-btn btn-primary" onClick={(e) => this.filterCriterialOption("recipientDialog")}>
                                        Done
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* select date modal */}
                <div className="modal fade" id="select-date" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header text-center border-bottom-0">
                                <div className="col  d-flex flex-column justify-content-center align-items-center">
                                    <p className="modal-title  black-font font-size-21 font-medium w-50" align="left">Schedule</p>
                                    <p className="modal-title  grey-font w-50" align="left">Timing your message right will ensure you get better leads.</p>
                                </div>
                                <button id="scheduledAtTime" type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body d-flex row justify-content-center  ml-3 mr-5">
                                <div className="w-50">
                                    <table width="100%" align="center">
                                        <tbody><tr><td>
                                            <div className="col px-0 pb-0 m-0">
                                                <input type="date" className="form-control form-control-lg draft-input-height modal-input" placeholder="DD / MM / YYYY"
                                                    value={this.state.scheduledAtDate || ""}
                                                    onChange={e => {
                                                        this.setState({ scheduledAtDate: e.target.value })
                                                    }}
                                                />
                                            </div>
                                        </td></tr>
                                            <tr><td>
                                                <div className="m-0 pb-2 p-0 row d-flex align-items-center">
                                                    <div className="col-2 d-flex align-items-center p-0 ">
                                                        <div className="m-0 grey-font font-size-14 font-medium pl-3">Set Time</div>
                                                    </div>
                                                    <div className="col-9 m-0 row d-flex justify-content-center align-items-center ">
                                                        <input className="col-2 pl-3 form-control draft-input-height"
                                                            value={this.state.scheduledAtHours || ""}
                                                            onChange={e => {
                                                                this.setState({ scheduledAtHours: e.target.value })
                                                            }}
                                                        />
                                                        <div className="mx-2">
                                                            <p className="m-0">:</p>
                                                        </div>
                                                        <input className="col-2 pl-3 form-control draft-input-height"
                                                            value={this.state.scheduledAtMinutes || ""}
                                                            onChange={e => {
                                                                this.setState({ scheduledAtMinutes: e.target.value })
                                                            }}
                                                        />
                                                        <select className="col-3 mx-3 form-control draft-input-height"
                                                            onChange={e => {

                                                                //"2021-01-25T08:26:33.523"
                                                                let scheduledAtDate = this.state.scheduledAtDate;
                                                                let scheduledAtHours = this.state.scheduledAtHours;
                                                                let scheduledAtMinutes = this.state.scheduledAtMinutes;
                                                                let scheduledAt = "";
                                                                scheduledAt = `${scheduledAtDate}T${scheduledAtHours}:${scheduledAtMinutes}:00`
                                                                this.updateFormData('scheduledAt', scheduledAt);
                                                            }}
                                                        >
                                                            <option value="AM" selected={this.state.data.scheduledAt ? parseInt(this.state.data.scheduledAt.split("T")[1].split(":")[0]) <= 12 ? true : false : true}>AM</option>
                                                            <option value="PM" selected={this.state.data.scheduledAt ? parseInt(this.state.data.scheduledAt.split("T")[1].split(":")[0]) > 12 ? true : false : false}>PM</option>
                                                        </select>
                                                        <div>
                                                            <p className="m-0 grey-font font-medium font-size-14">GMT +08:00</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td></tr></tbody></table>
                                    <div className="w-100  mb-5 pr-3 row d-flex justify-content-center">
                                        <button className="w50 draft-proceed-btn btn-primary" onClick={(e) => this.savescheduledAt()}>
                                            Done
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* preview modal */}
                <div className="modal fade" id="preview" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header text-center border-bottom-0">
                                <div className="col  d-flex align-items-center">
                                    <p className="modal-title  black-font font-size-21 font-semi" align="left">Pasang RFID, dapatkan RM5 cashback</p>
                                </div>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body d-flex row justify-content-center  p-0 m-0" style={{ background: '#F0F2F4', borderTop: '1px solid #CCCEDB' }}>
                                <div className="col-xl-6 pt-3 pb-5 px-5 d-flex flex-column" style={{ borderRight: '1px solid #CCCEDB' }}>
                                    <div className="col text-center">
                                        <p className="font-semi black-font">iOS Preview</p>
                                    </div>
                                    <div className="col  p-0 d-flex justify-content-center">
                                        <img src={`${process.env.URL_PREFIX}/static/img/ex_iOS.png`} alt="iOS preview" />
                                    </div>
                                </div>
                                <div className="col-xl-6 pt-3 pb-5 px-5 d-flex flex-column">
                                    <div className="col text-center">
                                        <p className="font-semi black-font">Android Preview</p>
                                    </div>
                                    <div className="col  p-0 d-flex justify-content-center">
                                        <img src={`${process.env.URL_PREFIX}/static/img/ex_Android.png`} alt="Android preview" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* proceed modal */}
                <div className="modal fade" id="proceed" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header text-center border-bottom-0">
                                <div className="col  d-flex align-items-center">
                                    <p className="modal-title  black-font font-size-21 font-medium" align="left">Prepare to Launch</p>
                                </div>
                                <button id="proceedClose" type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body d-flex flex-column justify-content-center align-items-center  p-5 m-0 border-top">
                                <div className="pb-4">
                                    <img src={`${process.env.URL_PREFIX}/static/img/proceed.png`} alt="proceed" />
                                </div>
                                <div>
                                    <p className="font-size-21 font-medium m-0">You're about to send a push notification</p>
                                </div>
                                <div className="pb-2">
                                    <p className="font-size-14 grey-font">This is your moment of glory.</p>
                                </div>
                                <div className="row pb-4">
                                    <span className="mb-1 font-medium grey-font font-size-14">Sending to&nbsp;&nbsp;</span>
                                    <span className="black-font font-medium" style={{ marginTop: '-10px' }}><nobr><span className="black-font font-medium font-size-24">{this.state.recipientCount || 0}</span>&nbsp;&nbsp;users</nobr></span>
                                </div>
                                <div className="w-75 pb-3 row d-flex justify-content-center">
                                    <button className="w-50 confirm-btn rounded font-medium" onClick={(e) => this.send()}>
                                        Send Now
                                    </button>
                                </div>
                                <div>
                                    <p className="font-medium blue-font" onClick={(e) => this.closeDialog("proceedClose")}>Cancel</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* End Dialog */}
                {
                    this.state.loading ?
                        <div class="spinner-border text-primary center_spinner" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        :
                        this.state.draftData.length > 0 || this.state.selectedIndex == null ?
                            <>
                                <div class="col-3 pl-2 pr-0 fade-in draft_container">
                                    <div class=" d-flex align-items-center pt-0" style={{ height: "10%" }}>
                                        <p class="py-0 m-0 pl-3 font-size-21 font-medium black-font theme-font-color">Drafts</p>
                                    </div>
                                    {/* Side_Listing */}
                                    <SideListing
                                        title='Drafts'
                                        data={this.state.draftData}
                                        selected={this.state.selectedIndex}
                                        onItemRendered={(item, index) =>
                                            <div key={item.id}
                                                className="edit_hover_className"
                                                onClick={() => {
                                                    this.onNotificationSelected(item, index)
                                                }}>
                                                <div class="pt-1 pb-2 font-size-14 black-font font-semi theme-font-color">
                                                    {
                                                        item.header && item.header.trim() != '' ?
                                                            <>
                                                                {item.header}
                                                            </>
                                                            :
                                                            "Untitled"
                                                    }
                                                </div>
                                                <div class="pb-1 font-size-14 font-medium">{item.body}</div>
                                                <a class="pr-0 pt-0 m-0" style={{ color: "#b1b2b2", float: "right" }} onClick={e => this.confirmDeleteMessage(item.id)}><i class="fa fa-trash-o"></i></a>
                                                <div class="font-medium font-size-12">Created {Moment(item.createdDate).format('DD MMM')} {Moment(item.createdDate).format('h:mm A')}</div>
                                            </div>
                                        } />
                                    {/* End Side Listing */}
                                </div>
                                {/*  <!-- right panel --> */}
                                <div class="col-7 p-0 fade-in" style={{ height: "100%" }}>
                                    <div class="col p-0 d-flex align-items-center  noti-draft-right-panel-top-height right-top-panel theme-title-color">
                                        <div class="col row p-0 m-0">
                                            <table width="100%" border="0" >
                                                <tr>
                                                    <td style={{ paddingLeft: "10px" }}>
                                                        <div class="font-size-21 ml-1 font-medium theme-font-color">{this.state.data.header}</div>
                                                    </td>
                                                    <td align="right" style={{ paddingRight: "15px" }}>
                                                        <button class="green-font font-medium mr-3" style={{ background: "transparent", border: "0px" }}>
                                                            {this.state.savingDraft != null ?
                                                                <>
                                                                    {this.state.savingDraft ? 'Saving...' : `Saved @ ${Moment(this.state.savingDraft.createdDate).format('h:mm A')}`}
                                                                </>
                                                                :
                                                                null
                                                            }
                                                        </button>
                                                        <button class="draft-detail-btn theme-font-color mr-3" data-toggle="modal" data-target="#preview" onClick={(e) => this.preview()} >Preview</button>
                                                        <button class="draft-proceed-btn btn-primary" data-toggle="modal" data-target="#proceed">Proceed</button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="col p-0 m-0 row  noti-draft-right-panel-btm-height ">
                                        {/* <!-- draft details panel --> */}
                                        {this.state.draftData.length > 0 || this.state.selectedIndex == null ?
                                            <PushMainContent
                                                data={this.state.data}
                                                activeActionType={this.state.data.actionType}
                                                readOnly={false}
                                                onImageUpload={loading => {
                                                    this.setState({
                                                        savingDraft: loading
                                                    })
                                                }}
                                                onActionChanged={action => {
                                                    this.updateFormData('actionType', action)
                                                }}
                                                onDataChanged={(key, value) => {
                                                    this.updateFormData(key, value)
                                                }}
                                                onError={error => {
                                                    this.setState({ error })
                                                }}
                                            />
                                            :
                                            <div className="col-sm-8" style={{ padding: "0px", borderRight: "1px solid #ddd", backgroundSize: "100%" }}>
                                                <div className='p-5 text-center'>
                                                    <h5 style={{ color: '#AAA' }}>
                                                        Click "+ New Notification" to create a new message
                                            </h5>
                                                </div>
                                            </div>
                                        }
                                        {/* <!-- send options panel --> */}
                                        <div class="col-4 send-option panel overflow-auto" style={{ height: "99%" }}>
                                            <div class="title theme-font-color font-medium font-size-18 m-0 pt-3">Send Options</div>

                                            <div class="col row mt-3">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="35px"><i class="fa fa-user-o font-size-24"></i></td>
                                                        <td>
                                                            <div class="mb-1 font-medium font-size-12">Sending to&nbsp;&nbsp;</div>
                                                            <div class="theme-font-color font-medium" style={{ marginTop: "-10px" }} ><nobr><span class="theme-font-color font-medium font-size-24">{this.state.recipientCount || 0}</span>&nbsp;&nbsp;users</nobr></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                            <div class="mt-3">
                                                <div class="font-medium font-size-16 pb-2">Recipient</div>
                                                <div class="form-group">
                                                    <button class="form-control draft-input-height p-0 radio-panel" data-toggle="modal" data-target="#recipient">
                                                        <div class="col row d-flex justify-content-between m-0 px-3">
                                                            <div>
                                                                <span class="theme-font-color font-size-14">
                                                                    {this.state.recipientType == "all" ? "Send to all users" : null}
                                                                    {this.state.recipientType == "select" ? "Select recipients" : null}
                                                                    {this.state.recipientType == "filter" ? "Filter recipients" : null}
                                                                </span>
                                                            </div>
                                                            <div>
                                                                <i class="fa fa-filter grey-font font-size-14" style={{ paddingRight: "14px" }}></i>
                                                            </div>
                                                        </div>
                                                    </button>
                                                </div>
                                            </div>

                                            <div class="mt-4">
                                                <div class="font-medium font-size-16 pb-2">Schedule</div>
                                                <button class={`col radio-panel draft-input-height d-flex align-items-center pl-3 mb-2 rounded theme-font-color`}>
                                                    <div class="mr-3 mt-1">
                                                        <input type="radio" id="schedule1" checked={this.state.data.scheduledAt == null}
                                                            onChange={e => {
                                                                let data = _.cloneDeep(this.state.data)
                                                                data.scheduledAt = null
                                                                this.setState({
                                                                    data
                                                                })
                                                            }}
                                                        />
                                                    </div>
                                                    <label class={`form-check-label font-medium font-size-14 ${this.state.data.scheduledAt == null ? "" : "grey-font"}`} for="schedule1">
                                                        Send Now
                                                    </label>
                                                </button>
                                                <button class="col form-check radio-panel draft-input-height d-flex align-items-center pl-3 rounded theme-font-color" data-toggle="modal" data-target="#select-date">
                                                    <div class="mr-3 mt-1">
                                                        <input type="radio" name="schedule" id="schedule2" checked={this.state.data.scheduledAt != null}
                                                            id="rb_scheduled_at"
                                                            onChange={e => {
                                                                if (e.target.checked) {
                                                                    if (this.state.data.scheduledAt == null) {
                                                                        this.updateFormData('scheduledAt', this.state.tmpScheduledAt || this.state.currentDate);
                                                                        this.setState({
                                                                            scheduledAtDate: this.state.currentDate.split("T")[0],
                                                                            scheduledAtHours: this.state.currentDate.split("T")[1].split(":")[0],
                                                                            scheduledAtMinutes: this.state.currentDate.split("T")[1].split(":")[1],
                                                                        })
                                                                    }

                                                                }
                                                            }}
                                                        />
                                                    </div>
                                                    <label class={`form-check-label font-medium font-size-14 ${this.state.data.scheduledAt != null ? "" : "grey-font"}`} for="schedule2">
                                                        {this.state.data.scheduledAt ? this.state.data.scheduledAt : "Select Date"}
                                                    </label>
                                                    <div class="d-flex col  justify-content-end">
                                                        <i class="fa fa-calendar grey-font"></i>
                                                    </div>
                                                </button>
                                            </div>

                                            <div class="mt-4">
                                                <div class="font-medium font-size-16 pb-2">Notification Behavior</div>
                                                <button class="col radio-panel d-flex align-items-start pl-3 pt-2 pb-1 mb-2 rounded text-left theme-element-color">
                                                    <div class="mt-1">
                                                        <input type="radio" name="push_type" id="push_type1" checked={!this.state.data.silent}
                                                            onChange={e => {
                                                                this.updateFormData('silent', false)
                                                            }} />
                                                    </div>
                                                    <div class="col mr-3">
                                                        <i class="fa fa-bell-o theme-font-color" style={{ paddingRight: "5px" }}></i>
                                                        <label class={!this.state.data.silent ? "form-check-label theme-font-color font-medium font-size-14" : "form-check-label grey-font font-medium font-size-14"} for="push_type1">
                                                            Regular Push
                                                        </label>
                                                        <div class="py-2 font-size-10 grey-font font-medium">Displays phone notification and makes sound. Use it for important messages.</div>
                                                    </div>
                                                </button>
                                                <button class="col radio-panel d-flex align-items-start pl-3 pt-2 pb-1 mb-2 rounded text-left theme-element-color">
                                                    <div class="mt-1">
                                                        <input type="radio" name="push_type" id="push_type2" checked={this.state.data.silent}
                                                            onChange={e => {
                                                                this.updateFormData('silent', true)
                                                            }} />
                                                    </div>
                                                    <div class="col mr-3">
                                                        <i class="fa fa-bell-slash-o grey-font" style={{ paddingRight: "5px" }}></i>
                                                        <label class={this.state.data.silent ? "form-check-label theme-font-color font-medium font-size-14" : "form-check-label grey-font font-medium font-size-14"} for="push_type2">
                                                            Silent Push
                                                        </label>
                                                        <div class="py-2 font-size-10 grey-font font-medium">Displays phone notification without sound. Recommended for regular messages that do not require immediate action.</div>
                                                    </div>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </>
                            :
                            <div class="col-10 d-flex flex-column text-center align-items-center justify-content-start p-5 fade-in">
                                <img class="mt-5 pt-3" src={`${process.env.URL_PREFIX}/static/img/Illustration_Login.png`} alt="illustration" style={{ height: "220px" }} />
                                <p class="mt-5 font-semi font-size-24">No drafts yet.</p>
                                <p class="mt-2 font-size-18 grey-font">Your notification drafts will appear here.</p>
                                <a class="big-btn mt-4 text-center p-0" href="/draft">
                                    <div style={{ marginTop: "16px" }}>+ Create a new notification</div>
                                </a>
                            </div>
                }
            </Main_Layout>
        )
    }
}

export default withAuth(withRouter(Draft)) 