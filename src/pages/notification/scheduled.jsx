import MyService from "../../services/my-service";
import IStudioService from "../../services/istudio-service";
import { Snack } from '../../components/snack';
import Main_Layout from '../../components/main_layout';
import { withAuth } from '../../components/auth-component';
import { withRouter } from "next/router";
import Moment from 'moment';

class Scheduled extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            data: {},
            error: null,
            success: null,
            scheduleData: [],
            scheduleIndividual: {},
            filters: [],
            recipientType: "",
            switchTheme: this.props.router.query.mode == "dark" ? "dark_theme" : "white_theme",
            loading: true
        }
    }

    componentDidMount() {
        //Get data schedule from backend
        IStudioService.getSchedules().then(res => {
            this.setState({
                scheduleData: res.data.data,
                loading: false
            })
        }).catch(e => {
            this.setState({
                error: "Error to getting schedules",
                loading: false
            });
        })
    }
    selectedSchedule(item) {
        this.setState({
            scheduleIndividual: item,
            filters: item.extras.filters,
            recipientType: item.extras.recipientType
        })
    }
    updateFormData(key, value) {
        let data = update(this.state.data, {
            [key]: {
                $set: value
            }
        })
        this.setState({ data })
    }

    resetForm() {
        this.setState({
            data: {
                header: '',
                body: '',
                url: '',
                image: '',
                header: '',
                type: '',
                userIds: '',
                data: {},
                scheduledAt: ''

            }
        })
    }
    render() {
        return (
            <Main_Layout title="Scheduled" mode={this.state.switchTheme}>
                {/* view modal */}
                <div className="modal fade" id="view" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header text-center border-bottom-0">
                                <div className="col  row d-flex align-items-center ">
                                    <p className="col modal-title  black-font font-size-21 font-semi" align="left">{this.state.scheduleIndividual.header}</p>
                                    <div className="col row d-flex justify-content-end m-0 ">
                                        <button className="draft-detail-btn blue-font font-semi mr-3">Duplicate</button>
                                        <button className="draft-detail-btn blue-font font-semi">Unsend</button>
                                    </div>
                                </div>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body d-flex flex-column  pt-3 pb-5 m-0 border-top">
                                <div className="d-flex row justify-content-end pr-3 mb-4">
                                    <div className="pt-1">
                                        <img src={`${process.env.URL_PREFIX}/static/img/ic_Delete.png`} alt="delete" height="13px" />
                                    </div>
                                    <a className="pl-3 pt-1 m-0 font-semi blue-font" href="draft.html" onclick="return confirmDelete()">Delete</a>
                                </div>
                                <div className="row m-0">
                                    <div className="col-8 m-0">
                                        <div className="col d-flex justify-content-center">
                                            <img src={`${process.env.URL_PREFIX}/static/img/Rfid.png`} alt="RFID" />
                                        </div>
                                        <div>
                                            <p className="m-0 title-xs grey-font">Title</p>
                                            <p className="black-font">{this.state.scheduleIndividual.header}</p>
                                        </div>
                                        <div>
                                            <p className="m-0 title-xs grey-font">Summary (optional)</p>
                                            <p className="black-font">Panggilan terakhir! Pasang RFID di pusat pemasangan</p>
                                        </div>
                                        <div>
                                            <p className="m-0 title-xs grey-font">Content</p>
                                            <p className="black-font">{this.state.scheduleIndividual.body}</p>
                                        </div>
                                    </div>
                                    <div className="col-4  m-0">
                                        <div>
                                            <p className="m-0 font-size-21 black-font font-semi">Send Options</p>
                                        </div>
                                        <div className="col row py-3">
                                            <img className="user mr-4 mt-1" src={`${process.env.URL_PREFIX}/static/img/ic_User.png`} alt="user" />
                                            {
                                                this.state.scheduleIndividual.userIds == null
                                                    ?
                                                    <span className="mb-2 font-semi grey-font">Sending to <span className="black-font"><span >all</span> users</span></span>
                                                    :
                                                    <span className="mb-2 font-semi grey-font">Sending to <span className="black-font"><span className="black-font h3">{this.state.scheduleIndividual.userIds.length}</span> users</span></span>
                                            }
                                        </div>
                                        <div>
                                            <p className="grey-font font-semi title-sm">Recipient</p>
                                            <p className="black-font font-semi">Filtered users</p>
                                        </div>
                                        <div className="pb-3">
                                            {
                                                this.state.recipientType == "filter" ?
                                                    this.state.filters.map((ev, index) => {
                                                        return (
                                                            <button className="col radio-panel draft-input d-flex align-items-center pl-3 mb-2 rounded">
                                                                <div className="mr-3">
                                                                    <p className="m-0 grey-font">{ev.criterias.label}:</p>
                                                                </div>
                                                                <label className="form-check-label theme-font-color font-semi" htmlFor="exampleRadios1">
                                                                    {ev.values[0]}
                                                                </label>
                                                            </button>
                                                        );
                                                    })
                                                    :
                                                    null
                                            }
                                        </div>
                                        <div>
                                            <p className="m-0 font-semi grey-font title-sm">Scheduled for</p>
                                        </div>
                                        <div className="row m-0">
                                            <div className="row m-0 pr-4 ">
                                                <div className="pr-3">
                                                    <img src={`${process.env.URL_PREFIX}/static/img/ic_Calendar.png`} alt="calendar" height="18px" />
                                                </div>
                                            </div>
                                            <div className="row m-0 p-0">
                                                <div className="pr-3">
                                                    <p className="m-0 font-semi black-font">{Moment(this.state.scheduleIndividual.scheduledAt).format('DD/MM/YYYY')}</p>
                                                </div>
                                                <div>
                                                    <div>
                                                        <p className="m-0 font-semi black-font">{Moment(this.state.scheduleIndividual.scheduledAt).format('h:mm A')}</p>
                                                    </div>
                                                    <div>
                                                        <p className="font-semi grey-font">(GMT 08:00)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <p className="m-0 font-semi grey-font title-sm">Notification Behavior</p>
                                        </div>
                                        <div className="row m-0 d-flex align-items-center">
                                            {
                                                !this.state.scheduleIndividual.silent ?
                                                    <>
                                                        <div className="pr-3">
                                                            <img src={`${process.env.URL_PREFIX}/static/img/ic_NotifSound.png`} alt="notification" height="18px" />
                                                        </div>
                                                        <div>
                                                            <p className="m-0 font-semi black-font">Regular Push</p>
                                                        </div>
                                                    </>
                                                    :
                                                    <>
                                                        <div className="pr-3">
                                                            <img src={`${process.env.URL_PREFIX}/static/img/ic_NotifSilent.png`} alt="notification" height="18px" />
                                                        </div>
                                                        <div>
                                                            <p className="m-0 font-semi black-font">Regular Push</p>
                                                        </div>
                                                    </>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* End */}
                {
                    this.state.loading ?
                        <div class="spinner-border text-primary center_spinner" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        :
                        <div className="col-10 pt-4 fade-in" style={{ height: '100%' }}>
                            <div className="col d-flex pb-2" style={{ height: '7%' }}>
                                <p className="font-size-21 black-font font-medium">Scheduled</p>
                            </div>
                            <div className="col overflow-auto" style={{ height: '93%' }}>
                                {
                                    this.state.scheduleData.map((ev, index) => {
                                        return (
                                            <button className="col mb-3 px-3 py-3 w-100 scheduled-item-box">
                                                <div className="row d-flex justify-content-between align-items-center m-0">
                                                    <div className="col-3 row m-0 p-0">
                                                        <div className="pr-3">
                                                            <i className={ev.silent ? "fa fa-bell-slash-o font-size-18 grey-font" : "fa fa-bell-o font-size-18 grey-font"} />
                                                        </div>
                                                        <div>
                                                            <p className="m-0 mt-0 font-medium">
                                                                {
                                                                    ev.silent ? "Silent Push" : "Regular Push"
                                                                }
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="col-1 m-0 p-0 black-font" align="right">
                                                        <p className="m-0 p-0 action-right"><a className="black-font font-medium" onClick={(e) => this.selectedSchedule(ev)} data-toggle="modal" data-target="#view">View</a></p>
                                                    </div>
                                                </div>
                                                <div align="left">
                                                    <div className="col px-0 pt-3 pb-1 black-font">
                                                        {ev.header}
                                                    </div>
                                                    <div className="col px-0 pb-3 title-xs grey-font font-medium">
                                                        {ev.body}
                                                    </div>
                                                </div>
                                                <div className="row m-0 p-0 d-flex justify-content-between">
                                                    <div className="col-lg-4 row m-0 p-0 align-items-center font-medium">
                                                        <i className="fa fa-user-o grey-font font-size-18 pr-3" />
                                                        {
                                                            ev.userIds == null
                                                                ?
                                                                <span className="mb-2">Sent to <span className="black-font">
                                                                    <span>all</span> users</span>
                                                                </span>
                                                                :
                                                                <span className="mb-2">Sent to <span className="black-font">
                                                                    <span className="black-font h3">{ev.userIds.length}</span> users</span>
                                                                </span>
                                                        }
                                                    </div>
                                                    <div className="col-lg-8 row m-0 p-0 d-flex justify-content-between align-items-center">
                                                        <div className="font-medium">
                                                            <i className="fa fa-calendar pr-3" />
                                                            <span>Scheduled on<span className="mx-2 black-font">{Moment(ev.scheduledAt).format('DD/MM/YYYY')}</span><span className="black-font">{Moment(ev.scheduledAt).format('h:mm A')}</span></span>
                                                        </div>
                                                        <div className="row m-0 p-0 d-flex align-items-center">
                                                            <div className="action-right">
                                                                <i className="fa fa-trash-o font-size-18 pr-2" />
                                                            </div>
                                                            <div className="action-right">
                                                                <span className="black-font font-medium">Delete</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </button>
                                        );
                                    })
                                }
                            </div>
                            <div className="col row d-flex justify-content-between p-3 pb-5 m-0 table-footer" style={{ height: '40px' }}>
                                <div className="col-4 row d-flex justify-content-between align-items-center p-0 m-0">
                                    <div>
                                        <p className="title-xs m-0 theme-font-color">1-10 of 195 items</p>
                                    </div>
                                    <div className="d-flex row align-items-center m-0 p-0">
                                        <p className="m-0 mr-3 title-sm">Show: <span className="theme-font-color numbering">10</span></p>
                                        <img src={`${process.env.URL_PREFIX}/static/img/ic_SelectDropdown.png`} alt="select dropdown" />
                                    </div>
                                </div>
                                <div className="col-4 d-flex row justify-content-end align-items-center m-0 p-0">
                                    <div className="mr-4">
                                        <img src={`${process.env.URL_PREFIX}/static/img/ic_Chevron Left.png`} alt="left" />
                                    </div>
                                    <div className="numbering theme-font-color">
                                        <span>1</span>
                                        <span>2</span>
                                        <span>3</span>
                                        <span>4</span>
                                        <span>5</span>
                                        <span>...</span>
                                        <span>19</span>
                                    </div>
                                    <div className="ml-4">
                                        <img src={`${process.env.URL_PREFIX}/static/img/ic_Chevron Right.png`} alt="right" />
                                    </div>
                                </div>
                            </div>
                        </div>
                }
            </Main_Layout>
        )
    }
}

export default withAuth(withRouter(Scheduled))