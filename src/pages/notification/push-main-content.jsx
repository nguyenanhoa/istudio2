
import React, { Component } from 'react'
import * as _ from 'lodash';
import IStudioService from '../../services/istudio-service';
import config from '../../../istudio.config';
import { Spinner, Button, Col, Row } from 'reactstrap';

export default class PushMainContent extends Component {

    modules = Object.keys(config.push.modules).map((key, value) => {
        return {
            label: key,
            value: config.push.modules[key]
        }
    })

    constructor(props) {
        super(props)
    }

    updateFormData(key, value) {
        if (!this.props.onDataChanged) return
        this.props.onDataChanged(key, value)
    }


    uploadImage(file) {

        if (this.props.onImageUpload) {
            this.props.onImageUpload(true)
        }

        IStudioService.uploadImage(file).then(res => {
            this.updateFormData('image', res.data.data.url)

            if (this.props.onImageUpload) {
                this.props.onImageUpload(false)
            }
        }).catch(e => {
            if (this.props.onImageUpload) {
                this.props.onImageUpload(false)
            }
            if (!this.props.onError) return
            this.props.onError('Upload image error, please try again.')
        })
    }

    openUpload() {
        if (this.props.readOnly) return
        document.getElementById("file").click();
    }
    dropHandler(ev) {
        ev.preventDefault();
        let file = ev.dataTransfer.files[0];
        this.uploadImage(file);
    }
    dragOverHandler(ev) {
        ev.preventDefault();
    }

    render() {
        return (
            <div class="col-8 d-flex flex-column  justify-content-start align-items-center py-4 panel overflow-auto theme-composor-background" style={{ height: "99%" }}>
                <div class="p-3 text-center theme-composor-content" style={{ width: "100%", borderRadius: "10px" }}>
                    <div style={{ height: "250px" }} onDrop={(event) => this.dropHandler(event)} onDragOver={(event) => this.dragOverHandler(event)} className="w-100 d-flex flex-column  justify-content-start align-items-center pl-15 py-4 mt-0 drag-drop-panel theme-element-color">
                        {
                            this.props.data.image ?
                                <>
                                    <img onClick={(e) => this.openUpload()} src={this.props.data.image ? this.props.data.image : `${process.env.URL_PREFIX}/static/img/tngo_ezlink.jpg`} width="100%" style={{ maxHeight: "250px", borderRadius: "10px", marginTop: "-24px" }} />
                                </>
                                :
                                <>
                                    <p align="center" className="font-size-14 font-medium">Drag and drop image file here<br />(support jpg, png only) </p>
                                    <p className="py-2 font-size-14 font-medium">OR</p>
                                    <button className="w-50 draft-detail-btn theme-font-color" onClick={(e) => this.openUpload()}>Browse</button>
                                </>
                        }
                        <input type="file" accept="image/*" id="file" name="file" style={{ display: "none" }}
                            onChange={e => {
                                this.uploadImage(e.target.files[0])
                            }} />
                    </div>

                    <div class="w-100 form-group mt-4 pt-2">
                        <input
                            type="text"
                            name="header"
                            required={true}
                            className="form-control draft-input-height"
                            placeholder="Title"
                            value={this.props.data.header || ''}
                            disabled={this.props.readOnly}
                            onChange={e => {
                                this.updateFormData('header', e.target.value)
                            }} />
                    </div>

                    <div class="w-100 form-group pt-1">
                        <input type="text" class="form-control draft-input-height" placeholder="Summary (Optional)" value="" />
                    </div>

                    <div class="w-100 form-group pt-1">
                        <textarea
                            id="exampleFormControlTextarea1"
                            rows="4"
                            name="body"
                            placeholder="Content Details"
                            className="form-control"
                            value={this.props.data.body || ''}
                            disabled={this.props.readOnly}
                            onChange={e => {
                                this.updateFormData('body', e.target.value)
                            }}>
                        </textarea>
                    </div>

                    <div class="w-100 form-group pt-1">
                        <input
                            type="text"
                            className="form-control draft-input-height"
                            placeholder="Youtube URL (optional)"
                        />

                    </div>
                    <div class="w-100 form-group pt-1">
                        <select
                            className="form-control draft-input-height"
                            style={{ margin: "0px" }}
                            disabled={this.props.readOnly}
                            value={this.props.data.category || ''}
                            onChange={e => {
                                this.updateFormData('category', e.target.value)
                            }} >
                            {this.modules.map(e =>
                                <option key={e.value} value={e.value}>{e.label}</option>
                            )}
                        </select>
                    </div>
                    <br />
                    {/* <div className="col draft-list-item py-3 pl-4 mb-3 draft-list-item_active" align="left" style={{ background: '#f0f6ff' }}>
                        <span className="pl-0 pt-2 pb-3 font-medium">Reference #1</span>
                        <span style={{ float: 'right' }}><a href="draft_new.html" onclick="return confirmRemove()"><i className="fa fa-trash-o" /></a></span>
                        <div className="w-100 form-group pt-3">                            
                            <input
                                type="text"
                                className="form-control draft-input-height"
                                style={{ fontSize: "10pt", height: "40px", }}
                                placeholder="Reference Title"
                                value={''}
                                disabled={this.props.readOnly}
                                onChange={e => {
                                    let links = _.cloneDeep(this.props.data.links)
                                    if (links) {
                                        links[index].title = e.target.value
                                    }
                                    this.updateFormData('links', links)
                                }} />
                        &nbsp;                        
                            <input
                                type="text"
                                className="form-control draft-input-height"
                                style={{ fontSize: "10pt", height: "40px", marginTop: '5px' }}
                                placeholder="Reference URL"
                                value={''}
                                disabled={this.props.readOnly}
                                onChange={e => {
                                    let links = _.cloneDeep(this.props.data.links)
                                    if (links) {
                                        links[index].url = e.target.value
                                    }
                                    this.updateFormData('links', links)
                                }} />
                        </div>
                    </div> */}
                    {this.props.data.links && this.props.data.links.map((link, index) =>
                        <div className="col draft-list-item py-3 pl-4 mb-3 draft-list-item_active" align="left" style={{ background: '#f0f6ff' }}>
                            <span className="pl-0 pt-2 pb-3 font-medium">Reference #1</span>
                            <span style={{ float: 'right' }}><a onClick={() => {
                                // delete link
                                let links = _.cloneDeep(this.props.data.links)
                                if (links) {
                                    links.splice(index, 1)
                                }
                                this.updateFormData('links', links)
                            }}><i className="fa fa-trash-o" /></a></span>
                            <div className="w-100 form-group pt-3">
                                <input
                                    type="text"
                                    className="form-control draft-input-height"
                                    style={{ fontSize: "10pt", height: "40px", }}
                                    placeholder="Reference Title"
                                    value={link.title || ''}
                                    disabled={this.props.readOnly}
                                    onChange={e => {
                                        let links = _.cloneDeep(this.props.data.links)
                                        if (links) {
                                            links[index].title = e.target.value
                                        }
                                        this.updateFormData('links', links)
                                    }} />
                                    &nbsp;
                                    <input
                                    type="text"
                                    className="form-control draft-input-height"
                                    style={{ fontSize: "10pt", height: "40px", marginTop: '5px' }}
                                    placeholder="Reference URL"
                                    value={link.url || ''}
                                    disabled={this.props.readOnly}
                                    onChange={e => {
                                        let links = _.cloneDeep(this.props.data.links)
                                        if (links) {
                                            links[index].url = e.target.value
                                        }
                                        this.updateFormData('links', links)
                                    }} />
                            </div>
                        </div>
                    )}
                    <a class="big-btn mt-0 text-center p-2 pl-5 pr-5" onClick={() => {
                        let links = _.cloneDeep(this.props.data.links) || []
                        links.push({ title: '', url: '' })
                        this.updateFormData('links', links)
                    }}>
                        + Add new Reference URL
                    </a>
                </div>
            </div>
        )
    }
}
