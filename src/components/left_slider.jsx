import Head from 'next/head'
import Cookies from 'universal-cookie'
import { AccessService, ACCESS_PUSH_DRAFT_CREATE, ACCESS_PUSH_DRAFT, ACCESS_PUSH_SCHEDULED, ACCESS_PUSH_SENT } from '../services/access-service'
class Left_Slider extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            switchTheme: this.props.mode
        };
    }
    componentDidMount() {

    }
    directUrl(url) {
        let switchTheme = window.localStorage.getItem("switchTheme")
        if (switchTheme != null) {
            this.setState({
                switchTheme: switchTheme
            })
            
            if (switchTheme == "white_theme") {
                if(url.includes("0"))
                {
                    window.location.href = `${process.env.URL_PREFIX}${url}&mode=white`
                }
                else
                {
                    window.location.href = `${process.env.URL_PREFIX}${url}?mode=white`
                }
                
            }
            else {
                if(url.includes("0"))
                {
                    window.location.href = `${process.env.URL_PREFIX}${url}&mode=dark`
                }
                else
                {
                    window.location.href = `${process.env.URL_PREFIX}${url}?mode=dark`
                }
            }
        }
    }
    render() {
        return (
            <div class="col-2 pt-4 pl-3 left-menu theme-left-nav">
                <a onClick={(e) => this.directUrl("/draft")}>
                    {/*  {
                    AccessService.accessible(ACCESS_PUSH_DRAFT_CREATE) ?
                    <button class="col left-panel-btn add-btn">
                        <i class="fa fa-plus" style={{ paddingRight: "10px" }}></i> New Notification
                    </button>
                    :null

                }    */}
                    <button class="col left-panel-btn add-btn theme-font-color">
                        <i class="fa fa-plus" style={{ paddingRight: "10px" }}></i> New Notification
                    </button>
                </a>
                <a onClick={(e) => this.directUrl("/draft/?selected=0")}>
                    <button class={this.props.title == "Drafts" ? "col left-panel-btn selected-btn" : "col left-panel-btn other-btn"}>
                        <i class="fa fa-pencil-square-o" style={{ paddingRight: "10px" }}></i> Drafts
                </button>
                </a>
                <a onClick={(e) => this.directUrl("/scheduled")}>
                    <button class={this.props.title == "Scheduled" ? "col left-panel-btn selected-btn" : "col left-panel-btn other-btn"}>
                        <i class="fa fa-calendar-check-o" style={{ paddingRight: "10px" }}></i> Scheduled
                </button>
                </a>
                <a onClick={(e) => this.directUrl("/sent")}>
                    <button class={this.props.title == "Sent" ? "col left-panel-btn selected-btn" : "col left-panel-btn other-btn"}>
                        <i class="fa fa-paper-plane-o" style={{ paddingRight: "10px" }}></i> Sent
                </button>
                </a>
                <a onClick={(e) => this.directUrl("/notification_log")}>
                    <button class={this.props.title == "Notification Log" ? "col left-panel-btn selected-btn" : "col left-panel-btn other-btn"}>
                        <i class="fa fa-history" style={{ paddingRight: "10px" }}></i> Notification Log
                </button>
                </a>
            </div>
        )
    }
}

export default Left_Slider