import { Row, Col } from 'reactstrap';

/**
 * @author Guster
 * @email guster@inglab.com.my
 * @create date 2019-07-22 09:50:57
 * @modify date 2019-07-22 09:50:57
 * @desc A form field with a label and a input field
 * @props
 * - title: string
 * - value: string
 * - type: string. default = 'text'
 * - required: boolean
 * - placeholder: string
 * - onChange: (string) {}
 */
class FormField extends React.Component {

    constructor(props) {
        super(props)        
    }

    render() {
        return (
            <Col className='create-user-field'>
                <div className='col-form-label grey-font font-size-14'>{this.props.title}</div>
                <div className='d-flex align-items-center'>
                    <input
                        className="form-control draft-input-height"
                        style={{height:"40px",fontSize:"11pt"}}
                        type={this.props.type || 'text'}
                        required={this.props.required}
                        placeholder={this.props.placeholder}
                        value={this.props.value}
                        disabled={this.props.disabled}
                        onChange={this.props.onChange} />
                    {this.props.icon ?
                        <div className='ml-2' style={{ color: '#777' }} onClick={this.props.onIconClick}>
                            <i className={this.props.icon}></i>
                        </div>
                        : null
                    }
                </div>
            </Col>
        )
    }
}

export default FormField