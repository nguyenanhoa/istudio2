import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Alert } from 'reactstrap';
import Loader from '../components/loader'

class DecisionModal extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            isOpen: this.props.isOpen,
            // loading: this.props.loading,
        }
    }

    componentWillReceiveProps(props) {
        this.setState({
            isOpen: props.isOpen,
            // loading: props.loading,
        })
    }

    toggleModal() {
        let isOpen = !this.state.isOpen
        this.setState({
            isOpen: isOpen
        })

        // callback to the caller to notify state change
        this.props.onStateChange(isOpen)
    }

    render() {
        return (
            <Modal
                size="xs"
                isOpen={this.state.isOpen}
                toggle={() => this.toggleModal()}>
                <ModalHeader
                    toggle={() => this.toggleModal()}>
                    {this.props.header}
                </ModalHeader>
                <ModalBody>
                    <p style={{ wordWrap: 'break-word' }}>{this.props.body}</p>
                    {this.props.children}
                    {this.props.error && this.props.error.trim() != '' ?
                        <Alert color='danger' className='mt-1'>{this.props.error}</Alert> : null
                    }
                </ModalBody>
                <ModalFooter>

                    {this.props.loading ?
                        <Loader loading={this.props.loading} /> : null
                    }

                    {this.props.cancelable ?
                        <Button
                            color="secondary"
                            outline
                            disabled={this.props.loading}
                            onClick={this.props.onNegativeButtonClick || (() => this.toggleModal())}>
                            {this.props.negativeButtonText || 'Cancel'}
                        </Button>
                        :
                        null
                    }

                    <Button
                        color="danger"
                        disabled={this.props.loading}
                        onClick={this.props.onPositiveButtonClick}>{this.props.positiveButtonText || 'Done'}</Button>

                </ModalFooter>
            </Modal>
        )
    }
}
export default DecisionModal