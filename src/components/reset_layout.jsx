import Head from 'next/head'
class Reset_Layout extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
        };
    }
    componentDidMount() {

    }
    render() {
        return (
            <html lang="en">
                <Head>
                    <meta charset="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
                    <link rel="shortcut icon" type="image/png" href={`${process.env.URL_PREFIX}/static/img/favicon_blue.png`} />
                    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" />
                    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' />
                    <link rel="stylesheet" href="/static/css/styles.css" />
                    <title>{this.props.title}</title>
                </Head>
                <body class="auth-background auth-container position-relative">
                    <div class="container-fluid">
                        {this.props.children}
                    </div>
                </body>
            </html>
        )
    }
}

export default Reset_Layout