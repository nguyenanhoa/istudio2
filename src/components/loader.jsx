/**
 * @author Guster
 * @email guster@inglab.com.my
 * @create date 2020-04-16 22:12:41
 * @modify date 2020-04-16 22:12:41
 */

import React, { Component } from 'react'
import ScaleLoader from "react-spinners/ScaleLoader";

export default class Loader extends Component {

    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {
        this.updateLoading(this.props.loading)
    }

    componentWillReceiveProps(props) {
        this.updateLoading(props.loading)
    }

    updateLoading(loading) {
        if (loading) {
            this.setState({ loading })
        } else {
            setTimeout(() => {
                this.setState({ loading: false })
            }, 1000)
        }
    }

    render() {
        return (
            <ScaleLoader
                size={30}
                color={"#123abc"}
                loading={this.state.loading} />
        )
    }
}
