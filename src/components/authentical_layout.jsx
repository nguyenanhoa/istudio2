import Head from 'next/head'
class Authentical_Layout extends React.Component {

   constructor(props) {
      super(props)
      this.state = {
      };
   }
   componentDidMount() {

   }
   render() {
      return (
         <html lang="en">
            <Head>
               <meta charset="utf-8" />
               <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
               <link rel="shortcut icon" type="image/png" href={`${process.env.URL_PREFIX}/static/img/favicon_blue.png`} />
               <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" />
               <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' />
               <link rel="stylesheet" href={`${process.env.URL_PREFIX}/static/css/styles.css`} />
               <title>{this.props.title}</title>
            </Head>
            <body class="auth-background">
               <div class="container d-flex align-items-center auth-container">
                  {/* Content Section */}
                  {this.props.children}
                  {/*  */}
               </div>
               <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
               <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
               <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
            </body>
         </html>
      )
   }
}

export default Authentical_Layout