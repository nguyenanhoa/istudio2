import Router from 'next/router'
import IStudioService from "../services/istudio-service";
import { AccessService } from '../services/access-service';
import * as _ from 'lodash'

export function withAuth(WrappedComponent, id) {
    return class extends React.Component {
        static async getInitialProps(context) {
            const { req, res, query } = context
            if (res) {
                let cookies = req.cookies
                if (!cookies.token) {
                    res.redirect(`${process.env.URL_PREFIX}/login`)
                } else {
                    try {
                        let response = await IStudioService.getMyUserMatrix(null, cookies.token)
                        if (response.status != 200) return {}

                        let matrix = response.data.data
                        if (matrix.filter(e => id.indexOf(e) >= 0).length > 0) {
                            console.log('no error')
                            AccessService.matrix = matrix
                            return { matrix }
                        } else {
                            console.log('error')
                            res.redirect(`${process.env.URL_PREFIX}/error`)
                        }
                    } catch (e) {
                        if (e.response == null) return {}
                        console.error('withAuth error', e.response.status)
                        if (e.response.status == 401) {
                            res.redirect(`${process.env.URL_PREFIX}/login`)
                        }
                    }
                }
            }
            return {}
        }

        componentDidMount() {
            window.localStorage.setItem('matrix', JSON.stringify(this.props.matrix))
        }

        render() {
            return <WrappedComponent {...this.props} />
        }
    }
}