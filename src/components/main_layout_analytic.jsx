import Head from 'next/head';
import Cookies from 'universal-cookie';
import Left_Slider from '../components/left_slider';
class Main_Layout_Analytic extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            username: "",
            switchTheme:this.props.mode
        };
    }
    componentDidMount() {
        //Check switchTheme from last state
        /* let switchTheme = window.localStorage.getItem("switchTheme")
        if (switchTheme != null) {
            this.setState({
                switchTheme: switchTheme
            })
        } */
        this.setState({
            username: window.localStorage.getItem("username")
        })
    }
    logout() {
        var s = confirm("Do you want to sign out now?");
        if (s) {
            const cookies = new Cookies();
            cookies.remove('token', { path: `${process.env.URL_PREFIX}` })
            window.localStorage.clear();
            window.location.href = `${process.env.URL_PREFIX}/login`
        }
    }
    switchTheme(e) {
        let switchTheme = "";
        if (this.state.switchTheme == "white_theme") {
            switchTheme = "dark_theme";
        }
        else {
            switchTheme = "white_theme";
        }
        this.setState({
            switchTheme: switchTheme
        })
        // Store into localStorage
        window.localStorage.setItem("switchTheme", switchTheme)
    }
    directUrl(url)
    {    
        let switchTheme = this.state.switchTheme;    
        if(switchTheme == "white_theme")
        {
            window.location.href=`${process.env.URL_PREFIX}${url}?mode=white`
        }
        else
        {
            window.location.href=`${process.env.URL_PREFIX}${url}?mode=dark`
        }
        
    }
    render() {
        return (
            <html lang="en">
                <Head>
                    <meta charset="utf-8" />
                    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
                    <link rel="shortcut icon" type="image/png" href={`${process.env.URL_PREFIX}/static/img/favicon_blue.png`} />
                    <link rel="stylesheet" href={`${process.env.URL_PREFIX}/static/css/bootstrap.min.css`} />
                    <link href={`${process.env.URL_PREFIX}/static/css/bootstrap4-toggle.css`} rel="stylesheet" />
                    <link rel="stylesheet" href={`${process.env.URL_PREFIX}/static/css/font-awesome/css/font-awesome.min.css`} />
                    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' />
                    <link rel="stylesheet" href={`${process.env.URL_PREFIX}/static/css/styles.css`} />
                    {
                        this.state.switchTheme=="white_theme" ?
                        <link rel="stylesheet" href={`${process.env.URL_PREFIX}/static/css/white_theme.css`} />
                        :
                        <link rel="stylesheet" href={`${process.env.URL_PREFIX}/static/css/dark_theme.css`} />
                    }                    
                    {/*  <!-- javascript and jquery --> */}
                    <script src={`${process.env.URL_PREFIX}/static/js/jquery.min.js`}></script>
                    <script src={`${process.env.URL_PREFIX}/static/js/scripts.js`}></script>
                    <title>{this.props.title}</title>
                </Head>
                <body class="theme-background theme-font-color">
                    {/* <!-- navigation --> */}
                    <div class="col row d-flex justify-content-between m-0 py-0 px-0 nav-bar-bs nav-height theme-nav">
                        <div class="col-lg-1 d-flex align-items-center">
                            <img class="nav-logo-bs" src={`${process.env.URL_PREFIX}/static/img/logo_menuiStudio.png`} alt="logo" />
                        </div>
                        <div class="col-lg-11 row d-flex justify-content-end align-items-center p-0 pl-2 m-0" id="nav-right">
                            <div class="col-auto p-0 d-flex justify-content-end h-100">
                                <div class="nav-section-bs m-0 p-0">
                                    <ul class="h-100 p-0 m-0 row">
                                        <a class="text-decoration-none" href="#"><li class="d-inline-block h-100 m-0 p-0 px-4 d-flex align-items-center theme-font-color">ProjectName</li></a>
                                        <a class="text-decoration-none" onClick={e=>this.directUrl("/draft")} title="Push Notification"><li class="d-inline-block h-100 m-0 py-0 px-4 d-flex align-items-center nav-active"><i class="fa fa-bell-o"></i></li></a>
                                        <a class="text-decoration-none" href="#" title="Activity Log"><li class="d-inline-block h-100 m-0 py-0 px-4 d-flex align-items-center theme-font-color"><i class="fa fa-file-text-o"></i></li></a>
                                        <a class="text-decoration-none" href="#" title="Reports"><li class="d-inline-block h-100 m-0 py-0 px-4 d-flex align-items-center theme-font-color"><i class="fa fa-bar-chart"></i></li></a>
                                        <a class="text-decoration-none" onClick={e=>this.directUrl("/setting/general")} title="Settings"><li class="d-inline-block h-100 m-0 py-0 px-4 d-flex align-items-center theme-font-color"><i class="fa fa-sliders"></i></li></a>
                                    </ul>
                                </div>
                            </div>
                            <div class="m-0 row d-flex justify-content-end align-items-center dropdown">
                                <div class="px-2">
                                    <img id="user-icon" src={`${process.env.URL_PREFIX}/static/img/user_jd.png`} alt="user_profile" />
                                </div>
                                <div class="pr-2 theme-font-color">
                                    <div id="user-name" data-toggle="tooltip" data-placement="bottom" title={this.state.username} style={{ width: "80px", whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis" }}>{this.state.username.toUpperCase()}</div>
                                    <div id="user-desc">ADMINISTRATOR </div>
                                </div>
                                <div class="dropdown-content">
                                    <div class="vertical-menu">
                                        <a href="#">My Profile</a>
                                        <a href="#">Change Password</a>
                                        <a href="#" onClick={(e) => this.switchTheme(e)} >Switch Theme</a>
                                        <a href="#" onClick={(e) => this.logout()}>Logout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-0 row content-height">                        
                        {
                            this.props.children
                        }
                    </div>
                    <script src={`${process.env.URL_PREFIX}/static/js/jquery-3.5.1.slim.min.js`} ></script>
                    <script src={`${process.env.URL_PREFIX}/static/js/popper.min.js`}></script>
                    <script src={`${process.env.URL_PREFIX}/static/js/bootstrap.min.js`}></script>
                    <script src={`${process.env.URL_PREFIX}/static/js/bootstrap4-toggle.js`}></script>
                </body>
            </html>
        )
    }
}

export default Main_Layout_Analytic