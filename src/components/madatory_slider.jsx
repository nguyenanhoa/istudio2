import Head from 'next/head'
import Cookies from 'universal-cookie'
import { AccessService, ACCESS_PUSH_DRAFT_CREATE, ACCESS_PUSH_DRAFT, ACCESS_PUSH_SCHEDULED, ACCESS_PUSH_SENT } from '../services/access-service'
class Madatory_Slider extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            switchTheme: this.props.mode
        };
    }
    componentDidMount() {

    }
    directUrl(url) {

        let switchTheme = window.localStorage.getItem("switchTheme")
        if (switchTheme != null) {
            this.setState({
                switchTheme: switchTheme
            })
            if (switchTheme == "white_theme") {
                window.location.href = `${process.env.URL_PREFIX}${url}?mode=white`
            }
            else {
                window.location.href = `${process.env.URL_PREFIX}${url}?mode=dark`
            }
        }
    }
    render() {
        return (
            <div className="col-2 pt-4 pl-3">
                <div className="font-size-10 font-semi grey-font pl-3 pb-2">MANDATORY</div>
                <a onClick={(e) => this.directUrl("/setting/general")}>
                    <button className={this.props.madatory == "general" ? "col left-panel-btn selected-btn" : "col left-panel-btn other-btn"}>
                        General
                </button></a>
                <a onClick={(e) => this.directUrl("/setting/database")}>
                    <button className={this.props.madatory == "database" ? "col left-panel-btn selected-btn" : "col left-panel-btn other-btn"} >
                        Database
                </button></a>
                <a onClick={(e) => this.directUrl("/setting/token")}>
                    <button className={this.props.madatory == "token" ? "col left-panel-btn selected-btn" : "col left-panel-btn other-btn"}>
                        JWT Token
                </button></a>
                <div className="font-size-10 font-semi grey-font pt-5 pl-3 pb-2">OPTIONAL</div>
                <a onClick={(e) => this.directUrl("/setting/notification")}>
                    <button className={this.props.madatory == "notification" ? "col left-panel-btn selected-btn" : "col left-panel-btn other-btn"}>
                        Push Notification
                </button></a>
                <a onClick={(e) => this.directUrl("/setting/smtp")}>
                    <button className={this.props.madatory == "smtp" ? "col left-panel-btn selected-btn" : "col left-panel-btn other-btn"}>
                        SMTP
                </button></a>
                <a onClick={(e) => this.directUrl("/setting/fileserver")}>
                    <button className={this.props.madatory == "fileserver" ? "col left-panel-btn selected-btn" : "col left-panel-btn other-btn"}>
                        File Server
                </button></a>
                <a onClick={(e) => this.directUrl("/setting/smsserver")}>
                    <button className={this.props.madatory == "smsserver" ? "col left-panel-btn selected-btn" : "col left-panel-btn other-btn"}>
                        SMS Server
                </button></a>
                <a onClick={(e) => this.directUrl("/setting/paymentgateway")}>
                    <button className={this.props.madatory == "paymentgateway" ? "col left-panel-btn selected-btn" : "col left-panel-btn other-btn"}>
                        Payment Gateway
                </button></a>
            </div>

        )
    }
}

export default Madatory_Slider