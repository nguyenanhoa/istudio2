/**
 * @author Guster
 * @email guster@inglab.com.my
 * @create date 2020-04-21 18:44:07
 * @modify date 2020-04-21 18:44:07
 * @desc [description]
 */

import { Button } from 'reactstrap';
import Loader from './loader';

class AppButton extends React.Component {

    styles = {
        marginTop: "10px",
        marginBottom: "10px",
        padding: "12px",
        fontSize: "11pt",
        fontWeight: "bold",
        textAlign: "center",
        borderRadius: "7px",
    }

    constructor(props) {
        super(props)

        if (props.outline) {
            this.styles.padding = '4px'
            this.styles.width = '110px'
            this.styles.height = '50px'
            this.styles.fontSize = '12pt'
            this.styles.margin = '0px 8px 0px 8px'
            this.styles.border = '2px solid #007bff'
        }

        if (props.color == 'danger') {
            this.styles.border = `2px solid red`
        }
    }

    render() {
        if (this.props.loading) {
            return (
                <div className={`text-center ${this.props.className}`} style={{ marginTop: '5px', marginRight: '10px'}}>
                    <Loader loading={this.props.loading} />
                </div>
            )
        }

        return (
            <button
                disabled={this.props.loading || this.props.disabled}
                className={`btn btn-block ${this.props.outline ? this.props.color == 'danger' ? 'btn-outline-danger' : 'btn-outline-primary' : 'btn-primary'} ${this.props.className}`}
                style={this.styles}
                onClick={this.props.onClick}>
                {this.props.children}
            </button>
        )
    }
}
export default AppButton