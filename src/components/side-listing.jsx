
import React, { Component } from 'react'

export default class SideListing extends Component {

    render() {
        return (
            <div class="m-0 p-0 pr-1 draft-panel overflow-auto" style={{ height: "90%" }}>
                <div style={{ padding: "10px 3px 0px 3px" }} >

                    {
                        this.props.data && this.props.data.map((item, index) => {
                            return (
                                <div class={item.selected ? "col draft-list-item py-3 pl-4 mb-3 draft-list-item_active" :"col draft-list-item py-3 pl-4 mb-3" }>
                                    {this.props.onItemRendered && this.props.onItemRendered(item, index)}
                                </div>
                            );
                        })
                    }
                </div>
            </div>

        )
    }
}
