/**
 * @author Guster
 * @email guster@inglab.com.my
 * @create date 2020-04-16 16:49:33
 * @modify date 2020-04-16 16:49:33
 */

import React, { Component } from 'react'
import { Row, Col, Card, Tooltip } from 'reactstrap'

export default class InfoTooltip extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tooltip: false
        }
    }

    render() {
        return (
            <div>
                <i id={`tooltip-${this.props.id}`} className="ml-2 mt-1 fa fa-info-circle" style={{ color: '#aaa' }} />
                <Tooltip
                    placement="top"
                    isOpen={this.state.tooltip}
                    target={`tooltip-${this.props.id}`}
                    style={{ backgroundColor: 'grey', maxWidth: '600px' }}
                    toggle={() => {
                        this.setState({
                            tooltip: !this.state.tooltip
                        })
                    }}>
                    {this.props.tooltip}
                </Tooltip>
            </div>
        )
    }
}
