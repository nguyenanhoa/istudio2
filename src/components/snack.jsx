import React, { Component } from 'react'
import { Snackbar, SnackbarContent, IconButton } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import { red } from '@material-ui/core/colors'
import { withStyles } from '@material-ui/styles'

// const styles = theme => ({
//     error: {
//         backgroundColor: red[600]
//     }
// })

export class Snack extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Snackbar
                anchorOrigin={{
                    vertical: this.props.vertical,
                    horizontal: this.props.horizontal
                }}
                open={this.props.open}
                autoHideDuration={this.props.autoHideDuration || 3000}
                onClose={() => {
                    if (this.props.onClose) {
                        this.props.onClose()
                    }
                }}>
                <SnackbarContent
                    style={{ backgroundColor: this.props.color || '#333333' }}
                    message={this.props.text}
                    action={[
                        <IconButton
                            key="close"
                            aria-label="close"
                            color="inherit"
                            onClick={() => {
                                if (this.props.onActionButtonClick) {
                                    this.props.onActionButtonClick()
                                }
                            }}>
                            <CloseIcon className='icon' />
                        </IconButton>
                    ]}
                />

            </Snackbar>
        )
    }
}

// export default withStyles(styles)(Snack)
export default Snack
