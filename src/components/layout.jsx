
import MyService from "../services/my-service";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Container } from 'reactstrap';
import Head from 'next/head'
import { Snack } from '../components/snack';
import Moment from 'moment';
class Layout extends React.Component {

   constructor(props) {
      super(props)
      this.state = {


      };
   }

   componentDidMount() {

   }
   render() {
      return (
         <html style={{ height: "100vh" }}>
            <Head>
               <meta charset="utf-8" />
               <meta http-equiv="X-UA-Compatible" content="IE=edge" />
               <title>Honda Online Booking | {this.props.title}</title>
               <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover, maximum-scale=1.0, user-scalable=no"></meta>
               <link rel="shortcut icon" type="image/png" href={`${process.env.URL_PREFIX}/static/images/honda-favicon.ico`} />
               <link rel="stylesheet" href={`${process.env.URL_PREFIX}/static/dist/css/adminlte.css`} />
               <link rel="stylesheet" href={`${process.env.URL_PREFIX}/static/dist/css/custom.css`} />
               <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
               {/* <link rel="stylesheet" href={`${process.env.URL_PREFIX}/static/dist/css/styles.css`} /> */}
               {/* <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css"></link> */}
            </Head>
            <body className="hold-transition sidebar-mini bgstyle1" /* {this.props.show=="true" ? "hold-transition sidebar-mini bgstyle1":"hold-transition sidebar-mini bgstyle2"} */ style={{ fontFamily: "Lato-Regular", height: "100vh", marginBottom: "0px" }}>
               <section className="content refresh_sec">
                  <div className="container-fluid responsive-padding">
                     {this.props.children}
                  </div>
               </section>
               <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossOrigin="anonymous"></script>
               <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossOrigin="anonymous"></script>
               <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js" integrity="sha512-cJMgI2OtiquRH4L9u+WQW+mz828vmdp9ljOcm/vKTQ7+ydQUktrPVewlykMgozPP+NUBbHdeifE6iJ6UVjNw5Q==" crossOrigin="anonymous"></script>
               <script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js" integrity="sha512-qH+R6YL4/40iiIrnN5aNZ1sEeEalNAdnzP9jfsxFPBdIslTkwUddkSazjVWhJ3f/3Y26QF6aql0xeneuVw0h/Q==" crossOrigin="anonymous"></script>
               <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.0.5/js/adminlte.min.js" integrity="sha512-++c7zGcm18AhH83pOIETVReg0dr1Yn8XTRw+0bWSIWAVCAwz1s2PwnSj4z/OOyKlwSXc4RLg3nnjR22q0dhEyA==" crossOrigin="anonymous"></script>
            </body>
         </html>
      )
   }
}

export default Layout