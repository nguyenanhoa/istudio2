/**
 * @author Guster
 * @email guster@inglab.com.my
 * @create date 2019-07-22 09:44:43
 * @modify date 2019-07-22 09:44:43
 * @desc APIs
 */
import axios from "axios"
import Constants from './constants'
import Cookies from "universal-cookie"

class Apis {
    static BASE_URL = Constants.BASE_URL
    static API_KEY = 'a12345b12345c12345'

    static getHeaders(token) {
        let headers = {
            'x-apikey': this.API_KEY
        }
        if (token) {
            headers['Authorization'] = `Bearer ${token}`
        }
        return headers
    }

    static get(url, token, params = {}) {
        if (!token) {
            let cookies = new Cookies()
            token = cookies.get('token')
        }

        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.get(url, {
            headers: this.getHeaders(token),
            params: params
        });
    }

    static postBlob(url, token, payload = {}) {
        // if (!token) {
        //     token = window.localStorage.getItem("token");
        // }

        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.post(url, payload, {
            responseType: 'blob',
            headers: this.getHeaders(token)
        });
    }

    static post(url, token, payload = {}) {
        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.post(url, payload, {
            timeout: 60000,
            headers: this.getHeaders(token)
        });
    }

    static postNoToken(url, payload = {}) {
        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.post(url, payload, {
            headers: {
                'X-ApiKey': this.API_KEY
            }
        })
    }
    static postFile(url, token, payload = {}) {
        if (!token) {
            token = window.localStorage.getItem("token");
        }

        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.post(url, payload, {
            headers: {
                'X-ApiKey': this.API_KEY,
                'X-SessionKey': token,
                'Content-Type': 'application/json'
            }
        });
    }
    
    static postMultipartForm(url, token, payload = {}) {
        if (!token) {
            token = window.localStorage.getItem("token");
        }

        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        let formData = new FormData()
        Object.keys(payload).forEach(key => {
            if (payload[key] instanceof FileList) {
                // append multiple files
                for (let i = 0; i < payload[key].length; i++) {
                    formData.append(key, payload[key][i])
                }
            } else {
                formData.append(key, payload[key])
            }
        })

        let headers = this.getHeaders(token)
        headers['Content-Type'] = 'multipart/form-data'

        return axios.post(url, formData, {
            headers
        });
    }

    static put(url, token, payload = {}) {
        if (!token) {
            token = window.localStorage.getItem("token");
        }

        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.put(url, payload, {
            headers: this.getHeaders(token) 
        });
    }
    static putMultiFile(url, token, payload = {}) {
        if (!token) {
            token = window.localStorage.getItem("token");
        }

        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.put(url, payload, {
            headers: {
                'X-ApiKey': this.API_KEY,
                'X-SessionKey': token,
                'Content-Type': 'multipart/form-data'
            }
        });
    }

    static putMultipartForm(url, token, payload = {}) {
        if (!token) {
            token = window.localStorage.getItem("token");
        }

        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        let formData = new FormData()
        Object.keys(payload).forEach(key => {
            if (payload[key] instanceof FileList) {
                // append multiple files
                for (let i = 0; i < payload[key].length; i++) {
                    formData.append(key, payload[key][i])
                }
            } else {
                formData.append(key, payload[key])
            }
        })

        let headers = this.getHeaders(token)
        headers['Content-Type'] = 'multipart/form-data'

        return axios.put(url, formData, {
            headers
        });
    }

    static delete(url, token) {
        if (!token) {
            token = window.localStorage.getItem("token");
        }

        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.delete(url, {
            headers: this.getHeaders(token)
        });
    }

    // -------------------------------------------------------------------

    static async authenticate(response) {
        if (response.status == 401) {
            window.location.replace("/");
        }
    }

    static async authToken() {
        return this.post(`${this.BASE_URL}/token/auth`);
    }
}
export default Apis;
