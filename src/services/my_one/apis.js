/**
 * @author Guster
 * @email guster@inglab.com.my
 * @create date 2019-07-22 09:44:43
 * @modify date 2019-07-22 09:44:43
 * @desc APIs
 */
import axios from "axios"
import Constants from './constants'

class Apis {
    
    static BASE_URL = process.env.BASE_URL
    static API_KEY = 'a12345b12345c12345'

    static get(url, token, params = {}) {
        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
            
        }

        return axios.get(url, {
            
            headers: {
                "Content-Type": "application/json",
                "X-ApiKey":this.API_KEY          
            }
        });
    }
    

    static postBlob(url, token, payload = {}) {
        if (!token) {
            token = window.localStorage.getItem("token");
        }

        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.post(url, payload, {
            responseType: 'blob',
            headers: this.getHeaders(token)
        });
    }
    static delete(url) {
       
        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.delete(url,{
            headers: {                
                "X-DeviceOs":"web" 
            } 
        });
    }

    static post(url, token, payload = {}) {
        
        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.post(url, payload, {
            headers: {
                "Content-Type": "application/json",
                "X-ApiKey":this.API_KEY          
            }
        });
    }
    static postIpay(url, payload = {}) {
                
        return axios.post(url, payload, {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",         
            }
        });
    }
    static postUser(url, token, payload = {}) {
        
        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.post(url, payload, {
            headers: {
                "Content-Type": "application/json",
                "X-DeviceOs":"web",
                "Authorization":token          
            }
        });
    }

    static postNoToken(url, payload = {}) {
        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.post(url, payload, {
            headers: {
                'X-ApiKey': this.API_KEY
            }
        })
    }
    static postFile(url, token, payload = {}) {
        if (!token) {
            token = window.localStorage.getItem("token");
        }

        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.post(url, payload, {
            headers: {
                
                'Content-Type': 'application/json',
                "X-DeviceOs":"web"
            }
        });
    }
    static postMultiFile(url,payload = {}) {
        

        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.post(url, payload, {
            headers: {
                "X-DeviceOs":"web" ,
                'Content-Type': 'multipart/form-data'
            }
        });
    }

    static postMultipartForm(url, token, payload = {}) {
        if (!token) {
            token = window.localStorage.getItem("token");
        }

        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        let formData = new FormData()
        Object.keys(payload).forEach(key => {
            if (payload[key] instanceof FileList) {
                // append multiple files
                for (let i = 0; i < payload[key].length; i++) {
                    formData.append(key, payload[key][i])
                }
            } else {
                formData.append(key, payload[key])
            }
        })

        let headers = this.getHeaders(token)
        headers['Content-Type'] = 'multipart/form-data'

        return axios.post(url, formData, {
            headers
        });
    }

    static put(url, token, payload = {}) {
        if (!token) {
            token = window.localStorage.getItem("token");
        }

        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.put(url, payload, {
            headers: {
                "Content-Type": "application/json", 
                //"Authorization": token,
                "X-DeviceOs":"web" 
            } 
        });
    }
    static putEmail(url, token, payload = {}) {
        

        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.put(url, payload, {
            headers: {
                "Content-Type": "application/json", 
                "Authorization": token,
                "X-DeviceOs":"web" 
            } 
        });
    }
    static putRescheduleAppointment(url, token, payload = {}) {
        if (!token) {
            token = window.localStorage.getItem("token");
        }

        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.put(url, payload, {
            headers: {
                "Content-Type": "application/json", 
                "X-DeviceOs":"web" 
            } 
        });
    }
    static putVehicle(url, token, payload = {}) {
        if (!token) {
            token = window.localStorage.getItem("token");
        }

        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.put(url, payload, {
            headers: {
                "Content-Type": "application/json", 
                "X-DeviceOs":"web" 
            } 
        });
    }
    static putMultiFile(url, token, payload = {}) {
        if (!token) {
            token = window.localStorage.getItem("token");
        }

        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        return axios.put(url, payload, {
            headers: {
                'X-ApiKey': this.API_KEY,
                'X-SessionKey': token,
                'Content-Type': 'multipart/form-data'
            }
        });
    }

    static putMultipartForm(url, token, payload = {}) {
        if (!token) {
            token = window.localStorage.getItem("token");
        }

        // append API hostname if it's a path
        if (url.startsWith('/')) {
            url = `${this.BASE_URL}${url}`
        }

        let formData = new FormData()
        Object.keys(payload).forEach(key => {
            if (payload[key] instanceof FileList) {
                // append multiple files
                for (let i = 0; i < payload[key].length; i++) {
                    formData.append(key, payload[key][i])
                }
            } else {
                formData.append(key, payload[key])
            }
        })

        let headers = this.getHeaders(token)
        headers['Content-Type'] = 'multipart/form-data'

        return axios.put(url, formData, {
            headers
        });
    }

    

    // -------------------------------------------------------------------

    static async authenticate(response) {
        if (response.status == 401) {
            window.location.replace("/");
        }
    }

    static async authToken() {
        return this.post(`${this.BASE_URL}/token/auth`);
    }
}
export default Apis;
