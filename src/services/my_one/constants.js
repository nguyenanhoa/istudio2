/**
 * @author Guster
 * @email guster@inglab.com.my
 * @create date 2019-10-23 16:04:40
 * @modify date 2019-10-23 16:04:40
 * @desc Constants
 */

class Constants {
    static BASE_URL = process.env.BASE_URL
    static BASE_ASSET_URL = process.env.BASE_ASSET_URL
    static API_KEY = process.env.API_KEY

    // module actions
    static ACT_READ = 'read'
    static ACT_CREATE = 'create'
    static ACT_UPDATE = 'update'
    static ACT_DELETE = 'delete'
}
export default Constants