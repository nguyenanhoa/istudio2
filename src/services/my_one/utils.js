import Constants from "./constants";

/**
 * @author Guster
 * @email guster@inglab.com.my
 * @create date 2019-07-22 09:46:44
 * @modify date 2019-07-22 09:46:44
 * @desc utility class
 */
class Utils {
    /**
     * Parse an object to a url-friendly encoded query string
     * @param {*} obj 
     */
    static querify(obj) {
        var str = [];
        for (var p in obj)
            if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        return str.join("&");
    }

    static splitArray(array, rows, cols) {
        var count = 0
        var result = []

        if (!array) return result

        for (var i = 0, j = array.length; i < j; i += rows) {
            count++

            if (count < cols) {
                result.push(array.slice(i, i + rows))
            } else {
                result.push(array.slice(i))
                break
            }
        }
        return result
    }

    static appendUrlParams(params) {
        if (!params) return

        let url = `${window.location.protocol}//${window.location.host}${window.location.pathname}`
        let q = ''
        for (let key in params) {
            if (q != '') {
                q += '&'
            }
            q += `${key}=${params[key]}`
        }

        if (q.trim() != '') {
            url += `?${q}`
        }

        window.history.pushState({
            path: url
        }, '', url)
    }

    static updateQueryStringParameter(key, value) {
        let uri = `${window.location.protocol}//${window.location.host}${window.location.pathname}`
        // console.log('hahaha', )
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
            return uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
            return uri + separator + key + "=" + value;
        }
    }

    static goToPage(router, url, params) {
        
        params = params || {}
        let query = router.query || {}
        for (let key in params) {
            query[key] = params[key]
        }
        router.push({
            pathname: url,
            query: query
        })
    }

    /**
     * Get the absoluate asset url for rendering
     * @param {*} path 
     */
    static assetUrl(path) {
        return `${Constants.BASE_ASSET_URL}${path}`
    }

    /**
     * Download a blob data from axios response.
     * Note: need to use `responseType = blob` in axios.
     * @param {*} response 
     * @param {*} filename 
     */
    static downloadBinary(response, filename) {
        let a = document.createElement('a');
        a.href = window.URL.createObjectURL(new Blob([response.data]))
        a.download = filename
        document.body.appendChild(a)
        a.click();
        document.body.removeChild(a)
    }
}
export default Utils