/**
 * @author Guster
 * @email guster@inglab.com.my
 * @create date 2019-07-22 09:46:31
 * @modify date 2019-07-22 09:46:31
 * @desc service class current login user
 */
import Apis from "./apis";
import axios from "axios"
class MyService {

    static generateSignature(data) {
        return Apis.post(`/internal/test/ipay88Signature`,null,data);
    }
    static getSettings(data) {
        return Apis.post(`onlineBooking/setting`,null,data);
    }

    static getTracking(id,email) {
        return Apis.get(`/touch/tracking/${id}/${email}`)
    }
    
    static getReviewInformation(id) {
        return Apis.get(`/touch/getReviewInformation/${id}`)
    }    
    static getMasterData() {
        return Apis.get('/touch/syncData')
    }
    static getVehicleDetails(data) {
        return Apis.post('/touch/getVehicleDetails',null,data)
    }
    static processPayment(data) {
        return Apis.post('/touch/processPayment',null,data)
    }
    static processIPayment(data) {
        return Apis.postIpay('https://payment.ipay88.com.my/epayment/entry.asp',data)
    }
    static resentEmail(data) {
        return Apis.post('/touch/tracking/resendEmail',null,data)
    }
    static sentEmail(data) {
        return Apis.post('/touch/sendEmail',null,data)
    }
    static createPreBooking(data) {
        return Apis.post('/touch', null, data)
    }
    static saveAccessToken(token) {
        window.localStorage.setItem('token', token)
        document.cookie = `token=${token}`
        
    }
    

    static getRole() {
        if (!process.browser) return false
        let token = window.localStorage.getItem('token')

        if (!token) return false

        let arr = token.split('.')

        if (arr.length < 3) return false

        return JSON.parse(window.atob(arr[1])).user.role
    }

    static isSuperAdmin(targetRole) {
        return (targetRole || this.getRole()) == 'ROLE_SUPER_ADMIN'
    }

    static saveUserMatrix(matrix) {
        window.localStorage.setItem('matrix', JSON.stringify(matrix))
    }

    static saveDealer(body) {
        if (!body.dealerCode) return
        window.localStorage.setItem('dcode', body.dealerCode)
        window.localStorage.setItem('dname', body.dealerName)
    }

    static getUserMatrix() {
        if (!process.browser) return {}
        let matrix = window.localStorage.getItem('matrix')
        if (matrix) {
            return JSON.parse(matrix)
        }
        return {}
    }

    static isAdminOrAbove() {
        return ['ROLE_SUPER_ADMIN', 'ROLE_ADMIN'].indexOf(this.getRole()) > -1
    }

    static accessible(targetModule, action) {
        let matrix = this.getUserMatrix()
        return matrix[targetModule] && matrix[targetModule][action]
    }
}

export default MyService