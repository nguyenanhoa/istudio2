import Apis from './apis'

class MyService {

    /* Forget password */
    static forgetPassword(params) {
        console.log(params)
        return Apis.post('/auth/admin/forgot-password', null, {
            email: params,
        })
    }

    /*Business */

    /* Get Dealer */
    static getDealers() {
        return Apis.get('/dealers/portal')
    }

        //Create Dealer 
    static createDealer(dealer) {
        console.log(dealer)
        return Apis.post('/dealers/create', null, dealer)
    }

    static updateDealer(dealer , dealerId) {
        console.log(dealer)
        console.log(dealerId)

        return Apis.put(`/dealers/update/${dealerId}`, null, dealer)
    }

    static getDealerDetails(dealerId){
        console.log(dealerId)
        return Apis.get(`/dealers/dealerDetails/${dealerId}`)
    }

    static removeDealer(dealerId){
        console.log(dealerId)
        return Apis.delete(`/dealers/delete/${dealerId}`)
    }



        //Create delaer info
    static createDealerInfo(dealer , dealerId) {
        console.log(dealer)
        console.log(dealerId)

        return Apis.put(`/dealers/createDealerInfo/${dealerId}`, null, dealer)
    }
    static removeDealerInfo(dealerId){
        console.log(dealerId)
        return Apis.delete(`/dealers/dealerDetails/${dealerId}`)
    }


 
    static getDealerInfo(userId){
        return Apis.get(`/dealers/${userId}`)
    }

    static getStates(){
        return Apis.get('/master/cityStatePostCode')
    }

    static getDealerCodeList(){
        return Apis.get('/dealers/dealerCode')
    }


    /* RefUrl */
    static saveRefUrl(settings) {
        return Apis.post('/portal/configData',null, settings)
    }

    static getRefUrl() {
        return Apis.get(`/portal`, {})
    }


    /* Report */
    /*User register*/
    static getRegistrationUserReport(params) {
        return Apis.postBlob('/portal/getRegisteredUser', null, params)   
    }
    static getRegistrationUserReportList(params) {
        return Apis.post('/portal/getRegisteredUser/list', null, params)   
    }
    /*Non honda Details*/
    static getNonHondaDetails(params) {
        return Apis.postBlob('/portal/getNonHondaDetails', null, params)   
    }
    static getgetNonHondaDetailsList(params) {
        return Apis.post('/portal/getNonHondaDetails/list', null, params)   
    }
    /*Non honda to honda*/
    static getNonHondaToHonda(params){
        return Apis.postBlob('/portal/getNonHondaToHonda', null, params)   
    }
    static getNonHondaToHondaList(params) {
        return Apis.post('/portal/getNonHondaToHonda/list', null, params)   
    }
    /*Honda to non-honda*/
    static getHondaToNonHonda(params){
        return Apis.postBlob('/portal/getHondaToNonHonda', null, params)   
    } 
    static getHondaToNonHondaList(params){
        return Apis.post('/portal/getHondaToNonHonda/list', null, params)   
    }

    /*Data Changes*/
    static getDataChanges(params){
        console.log(params)
        return Apis.postBlob('/portal/getDataChanges', null, params)   
    }
    static getDataChangesLit(params){
        console.log(params)
        return Apis.post('/portal/getDataChanges/list', null, params)   
    }
    /*Appoint submission*/
    static getApptSubmission(params) {
        return Apis.postBlob('/portal/getApptSubmission', null, params)   
    }
    static getApptSubmissionList(params) {
        console.log(params)
        return Apis.post('/portal/getApptSubmission/list', null, params)   
    }
    /*Cusomter Activity*/
    static getCustomerActivity(params){
        return Apis.postBlob('/portal/getAuditTrail', null , params)
    }
    static getCustomerActivityList(params){
        return Apis.post('/portal/getAuditTrail/list', null , params)
    }
    /*Vechicle Service Tracking*/
    static getVehicleServiceTracking(params){
        return Apis.postBlob('/portal/getVehicleServiceTracking', null , params)
    }
    static getVehicleServiceTrackingList(params){
        return Apis.post('/portal/getVehicleServiceTracking/list', null , params)
    }
    /*Enquiry Details*/
    static getEnquiryDetails(params){
        return Apis.postBlob('/portal/getEnquiryDetails', null , params)
    }
    static getEnquiryDetailsList(params){
        return Apis.post('/portal/getEnquiryDetails/list', null , params)
    }

    


}
export default MyService