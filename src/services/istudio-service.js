import Apis from './apis'
import Cookies from 'universal-cookie'

class IStudioService {

    static getToken() {
        let cookies = new Cookies()
        return cookies.get('token')
    }

    static login(username, password) {
        return Apis.post('/auth/admin/login', null, {
            username: username,
            password: password,
            grantType: "password"
        }).then(async res => {
            let token = res.data.accessToken

            let cookies = new Cookies()
            cookies.set('token', token)

           let result = await Apis.get('/users/matrix', token)
            if (result.status == 200) {
                window.localStorage.setItem('matrix', JSON.stringify(result.data.data))
            }
            return res
        })
    }

    static logout() {
        window.localStorage.removeItem('matrix')
        let cookies = new Cookies()
        cookies.remove('token')
    }

    static tokenCheck(token) {
        return Apis.post('/auth/token/auth', token)
    }

    static sendNotification(params) {
        let token = this.getToken()
        return Apis.post('/push/broadcast', token, params)
    }

    // static getRecipients() {
    //     let token = this.getToken()
    //     return Apis.get('/push/devices', token, {
    //       groupByUser: true
    //     })
    // }

    static getRecipients() {
        return Apis.get('/logs/metadata/recipients')
    }

    static getRecipientsByCriteria(criteria) {
        let token = this.getToken()
        return Apis.post('/push/devices/meta', token, criteria)
    }

    static getFilteringCriteria() {
        return Apis.get('/push/filtering-criteria')
    }
    static getFilteringCriteria1() {
        return Apis.get('/adminPortal/getFilteringCriteria')
    }
    static getCriteriaResult(name,value) {
        return Apis.get(`/adminPortal/getFilteredUser?name=${name}&value=${value}`)
    }

    static getSents() {
        return Apis.get('/push/messages/sent')
    }

    static getApprovals() {
        return Apis.get('/push/messages/approval')
    }

    static getSchedules() {
        return Apis.get('/push/messages/scheduled')
    }

    static getDraftMessages() {
        return Apis.get('/push/messages/draft')
    }

    static deleteDraftMessages(id) {
        return Apis.delete(`/push/messages/draft/${id}`)
    }

    static saveDraftMessage(message) {
        let token = this.getToken()
        return Apis.post('/push/messages', token, message)
    }

    static submitMessageForApproval(id) {
        let token = this.getToken()
        return Apis.post(`/push/messages/${id}/submit`, token)
    }

    static approveMessage(id) {
        let token = this.getToken()
        return Apis.put(`/push/messages/${id}/approve`, token)
    }

    static rejectMessage(id) {
        let token = this.getToken()
        return Apis.put(`/push/messages/${id}/reject`, token)
    }

    static uploadImage(file) {
        let token = this.getToken()
        return Apis.postMultipartForm('/push/upload-image', token, { image: file })
    }

    static getIStudioServices(names = []) {
        return Apis.get('/core/logs/system', null, {
            names: names != null ? names.map(item => item.trim()).join(',') : ''
        })
    }

    static getIStudioServiceEndpoints(serviceId) {
        return this.getSearchs({
            index: "i_service_transactions_myhonda",
            timeCol: "time",
            includeMainTerm: true,
            term: "endpoint.keyword",
            termLabel: "Endpoint",
            filters: [
                {
                    term: "systemServiceId",
                    value: serviceId
                }
            ]
        })
    }

    static getIStudioServiceTransactions(serviceIds = [], endpoints = [], from = null, to = null, desc = true, page = 0, size = 10) {
        return Apis.get('/core/logs/system/transactions', null, {
            serviceIds: serviceIds != null ? serviceIds.join(',') : '',
            endpoints: endpoints != null ? endpoints.map(item => item.trim()).join(',') : '',
            desc: desc,
            from: from,
            to: to,
            page: page,
            limit: size
        })
    }

    static getServiceTransactions(serviceIds = null, endpoints = null, desc = true, from = null, to = null, page = 1, size = 200) {
        return Apis.get('/logs/system/transactions', null, {
            //serviceIds: serviceIds != null ? serviceIds.join(',') : '',
            //endpoints: endpoints != null ? endpoints.map(item => item.trim()).join(',') : '',
            serviceIds: serviceIds,
            endpoints: endpoints,
            desc: desc,
            from: from,
            to: to,
            page: page,
            limit: size
        })
    }

    static getUserActivityModules() {
        return this.getSearchs({
            index: "i_user_activities_myhonda",
            timeCol: "time",
            includeMainTerm: true,
            term: "module.keyword",
            termLabel: "Module"
        })
    }

    static getUserActivityActions(module) {
        let params = {
            index: "i_user_activities_myhonda",
            timeCol: "time",
            includeMainTerm: true,
            term: "action.keyword",
            termLabel: "Action",
        }
        if (module) {
            params.filters = [
                {
                    "term": "module.keyword",
                    "value": module
                }
            ]
        }
        return this.getSearchs(params)
    }

    static getUserActivities(userIds = [], module = null, action = null, from = null, to = null, page = 0, size = 10) {
        return Apis.get('/user-activities', null, {
            userIds: userIds != null ? userIds.join(',') : '',
            module: module,
            action: action,
            from: from,
            to: to,
            page: page,
            limit: size
        })
    }

    static getSimpleUsers() {
        return Apis.get('/users/simple', null, {
            size: 100,
            page: 0
        })
    }

    static getAdminUsers(sudo = false) {
        return Apis.get('/users/admin/simple', null, {
            sudo: sudo,
            size: 1000,
            page: 0
        })
    }

    static getAdminUsersWithRoles(roles = []) {
        let token = this.getToken()
        return Apis.get('/users/simple-with-roles', token, {
            roles: roles.join(',')
        })
    }

    static getSuperiors() {
        let roles = ['ROLE_ADMIN']
        return this.getAdminUsersWithRoles(roles)
    }

    static saveSettings(settings) {
        let token = this.getToken()
        return Apis.post('/core/settings', token, settings)
    }

    static getSettings(type) {
        let token = this.getToken()
        return Apis.get(`/core/settings`, token, {
            type: type
        })
    }

    static uploadTnc(tnc, pdpa, privacy) {
        let token = this.getToken()
        return Apis.postMultipartForm('/core/settings/tnc', token, {
            tnc, pdpa, privacy
        })
    }

    static getMyUserMatrix(userId, token = null) {
        return Apis.get(`/users/matrix`, token, {
            userId
        })
    }

    static updateMyUserMatrix(userId, includes = [], excludes = []) {
        let token = this.getToken()
        return Apis.post(`/users/${userId}/matrix`, token, {
            includes, excludes
        })
    }

    /**
     * User services
     */
    static getMe() {
        let token = this.getToken()
        return Apis.get('/users/me', token)
    }

    static createUser(data) {
        let token = this.getToken()
        return Apis.post(`/auth/admin/create`, token, data)
    }

    static updateUser(userId, data) {
        let token = this.getToken()
        return Apis.put(`/users/${userId}`, token, data)
    }

    static deleteUser(userId) {
        let token = this.getToken()
        return Apis.delete(`/users/${userId}`, token)
    }

    static getUserDetails(userId) {
        let token = this.getToken()
        return Apis.get(`/users/${userId}`, token)
    }

    static getActiveUsers(from, to) {
        let token = this.getToken()
        return Apis.get(`/logs/active-users`, token, {
            from, to
        })
    }
    static getDashBoardData(from, to) {
        let token = this.getToken()
        return Apis.get(`/adminPortal/getDashboardDetails?startDate=${from}&endDate=${to}`)
    }
    static getUnverifiedEmail(from, to) {
        let token = this.getToken()
        return Apis.get(`/adminPortal/getUnverifiedUsers?startDate=${from}&endDate=${to}`)
    }
    static fetchUserName(params) {
        return Apis.post(`/logs/fetchUserName`, null, params)
    }

    /**
     * Report services
     */
    static getTimes(params) {
        return Apis.get('/core/analytics/_search/time', null, params)
    }
    static getSearchs(params) {
        return Apis.post('/core/analytics/_search', null, params)
    }

}
export default IStudioService