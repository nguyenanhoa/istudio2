/**
 * @author Guster
 * @email guster@inglab.com.my
 * @create date 2020-04-15 15:58:46
 * @modify date 2020-04-15 15:58:46
 * @desc [description]
 */

export const ACCESS_HOME = "home"

export const ACCESS_PUSH = "push"
export const ACCESS_PUSH_DRAFT = "push.draft"
export const ACCESS_PUSH_DRAFT_CREATE = "push.draft.create"
export const ACCESS_PUSH_DRAFT_SEND = "push.draft.send"
export const ACCESS_PUSH_SCHEDULED = "push.scheduled"
export const ACCESS_PUSH_SENT = "push.sent"

export const ACCESS_ACTLOG = "actlog"
export const ACCESS_ACTLOG_SYSTEM = "actlog.system"
export const ACCESS_ACTLOG_USER = "actlog.user"
export const ACCESS_ACTLOG_ADMIN = "actlog.admin"

export const ACCESS_BUSINESS = "business"
export const ACCESS_BUSINESS_DEALER = "business.dealer"
export const ACCESS_BUSINESS_WEB_REL = "business.web"
export const ACCESS_BUSINESS_REPORT = "business.report"


export const ACCESS_REPORT = "report"
export const ACCESS_REPORT_USER = "report.user"
export const ACCESS_REPORT_ANDROID = "report.android"
export const ACCESS_REPORT_IOS = "report.ios"
export const ACCESS_REPORT_NOTIFICATION = "report.notification"

export const ACCESS_SETTING = "setting"
export const ACCESS_SETTING_GENERAL = "setting.general"
export const ACCESS_SETTING_GENERAL_SMTP = "setting.general.smtp"
export const ACCESS_SETTING_GENERAL_PUSH = "setting.general.push"
export const ACCESS_SETTING_GENERAL_TNC = "setting.general.tnc"
export const ACCESS_SETTING_BUSINESS = "setting.business"
export const ACCESS_SETTING_USERS = "setting.users"
export const ACCESS_SETTING_SECURITY = "setting.security"
export const ACCESS_SETTING_SECURITY_LOGIN = "setting.security.login"
export const ACCESS_SETTING_SECURITY_PASSWORD = "setting.security.password"
export const ACCESS_SETTING_SECURITY_DEVICE = "setting.security.device"
export const ACCESS_SETTING_SECURITY_COMM = "setting.security.comm"

export class AccessService {
    static matrix;

    static accessible(key) {
        if (!process.browser) return false

        try {
            let matrix = this.matrix || JSON.parse(window.localStorage.getItem('matrix'))
            if (matrix == null) return false
            return matrix.find(e => e.startsWith(key)) != null
        } catch (e) {
            return false
        }

    }
}